/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "evobotsmainthread.h"
#include "evobots.h"
#include "uiupdater.h"
#include "graphics.h"
#include "graphicsglew.h"
#include "inputmanager.h"
#include "chunkmanager.h"
#include "chunkloadmanager.h"
#include "chunkupdatemanager.h"
#include "threadmanager.h"
#include "utils.h"

#include "sevocameranode.h"
#include "sevoscenenode.h"
#include "chunk.h"
#include "chunktable.h"

using namespace irr;
using namespace core;

EvobotsMainThread::EvobotsMainThread(Evobots* mainWindow)
	: Threadable(new ThreadManager()),
	  Engine(),
	  mainWindow(mainWindow),
	  mainLoopPartTime(0)
{
	this->threadManagerModule = this->threadManager;
	this->uiUpdaterModule = mainWindow->GetUiUpdater();
}

EvobotsMainThread::~EvobotsMainThread() {
	//UpdateGui();
}

void EvobotsMainThread::ShutDown() {
	this->shutDown = true;
}

void EvobotsMainThread::DoWork() {
	Threadable::DoWork();
	MainLoop();
}

void EvobotsMainThread::UpdateGui() {
	//this->uiUpdaterModule->SetSevoNodes();
	this->uiUpdaterModule->SetUiValue(IChunkSceneNode::chunksLoaded, Ui_ChunkNodes);
	this->uiUpdaterModule->SetUiValue(this->chunkManagerModule->chunkTable->chunkTableChunkCount, Ui_ChunkTableCount);
	this->uiUpdaterModule->SetRenderTimeCt(this->chunkManagerModule->GetChunkTable()->renderTime);
	this->uiUpdaterModule->SetMainLoopTime(utils::Timer.dTime);
	this->uiUpdaterModule->SetMainLoopPartTime(this->mainLoopPartTime);
	this->uiUpdaterModule->SetUiValue(this->chunkManagerModule->chunkLoadManager->chunksToLoad.size(), Ui_ChunksToLoad );
	this->uiUpdaterModule->SetUiValue(this->chunkManagerModule->chunkLoadManager->chunksLoading.size(), Ui_ChunksLoading);
	//this->uiUpdaterModule->SetUiValue(this->chunkManagerModule->chunkTable->chunksToSetBuffer.size(), Ui_ChunksToSetBuffer);
	this->uiUpdaterModule->SetUiValue(this->chunkManagerModule->chunkUpdateManager->chunksToRebuildCount, Ui_ChunksToRebuild);
	this->uiUpdaterModule->SetUiValue(this->chunkManagerModule->chunkUpdateManager->chunksToInformCount, Ui_ChunksToInform);
	this->uiUpdaterModule->SetUiValue(this->chunkManagerModule->chunkUpdateManager->chunksToRefreshCount, Ui_ChunksToRefresh);
	this->uiUpdaterModule->SetUiValue(this->chunkManagerModule->chunkUpdateManager->chunksToDeleteCount, Ui_ChunksToDelete);

	this->uiUpdaterModule->UpdateSelfSlot();
}

void EvobotsMainThread::MainLoop() {
	IGraphics* grglew = new GraphicsGlew(this);
	this->graphicsModule = grglew;
	this->graphicsModule->Initialize();
	GLFWwindow* window;
	if ((window = this->graphicsModule->OpenScreen()) == nullptr) {
		delete this->graphicsModule;
		this->deleteLater();
		return;
	}
	this->graphicsModule->PlaceStuff();

	utils::Timer.Tick();
	utils::Timer.Tick();

	this->inputManagerModule = new InputManager(this, window);
	this->inputManagerModule->Initialize();

	this->chunkManagerModule = new ChunkManager(this);
	this->chunkManagerModule->Initialize();

	this->threadManagerModule->AddThread(this->chunkManagerModule);

	this->graphicsModule->SetCameraPos(v3f(-5, 10, 10));
	this->graphicsModule->GetEvoCamera()->setTarget(v3f(0));
	this->chunkManagerModule->SetCameraPosition(graphicsModule->GetCameraPos());

	this->graphicsModule->CreateEvoTestNode(this->graphicsModule->GetRootSceneNode(), v3f(5,5,5));
	this->graphicsModule->GrabCursor(true);
	while(!this->shutDown) {
		this->graphicsModule->Render();
		double* timerStart(utils::Timer.StartTiming());
		this->inputManagerModule->CheckKeys(utils::Timer.dTimeMs);
		UpdateGui();
		utils::Timer.Tick();
		this->mainLoopPartTime = utils::Timer.StopTiming(timerStart);
	}
	this->graphicsModule->GrabCursor(false);
	this->graphicsModule->Render();

	this->chunkManagerModule->deleteLater();
	delete this->inputManagerModule;
	delete this->threadManagerModule;
	delete this->graphicsModule;
	//UpdateGui();
	this->deleteLater();
	return;

	this->graphicsModule = new Graphics(this);
	this->graphicsModule->Initialize();

	this->graphicsModule->OpenScreen();
	this->graphicsModule->PlaceStuff();

	this->chunkManagerModule = new ChunkManager(this);
	this->chunkManagerModule->Initialize();
	this->threadManagerModule->AddThread(this->chunkManagerModule);

	this->graphicsModule->SetCameraPos(v3f(1, 5, -15));
	this->chunkManagerModule->SetCameraPosition(graphicsModule->GetCameraPos());

	IrrlichtDevice *device = this->graphicsModule->GetDevice();
	bool windowHadFocus = true;
	while(device->run()) {
		if(!device->isWindowFocused()) {
			if(windowHadFocus) {
				this->graphicsModule->Pause(true);
				windowHadFocus = false;
			}

			device->yield();
		} else {
			if(!windowHadFocus) {
				this->graphicsModule->Pause(false);
				windowHadFocus = true;
			}

			this->chunkManagerModule->SetCameraPosition(this->graphicsModule->GetCameraPos());
			this->graphicsModule->Render();
			//device->yield();
		}

		//QCoreApplication::processEvents();
	}

	//while(this->threadManager->workerCount)	// that's only half of the fix, chunks are still alive when the workers that created them finished
	device->yield();

	this->chunkManagerModule->deleteLater();
	this->chunkManagerModule = nullptr;
	delete this->threadManagerModule;
	delete this->graphicsModule;
	this->deleteLater();
}
