#ifndef EVOBOTSMAINTHREAD_H
#define EVOBOTSMAINTHREAD_H

#include "engine.h"
#include "threadable.h"

class Evobots;

class EvobotsMainThread :public Threadable, public Engine {
public:
					EvobotsMainThread(Evobots *mainWindow);
					~EvobotsMainThread();

	virtual void	ShutDown();

protected slots:
	virtual void	DoWork();

private:
	void			UpdateGui();
	void			MainLoop();

	Evobots*		mainWindow;

	double			mainLoopPartTime;
};

#endif // EVOBOTSMAINTHREAD_H
