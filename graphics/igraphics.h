#ifndef IGRAPHICS_H
#define IGRAPHICS_H

#include "typedefs.h"

using namespace irr;

namespace irr {
namespace scene {
	class SEvoSceneNode;
	class SEvoCameraNode;
	class EvoHudNode;
} // irr::scene
namespace video {
class SEvoMaterial;
} // irr::video
} // irr
class GLFWwindow;

class IGraphics
{
public:
	virtual							~IGraphics(){}
	virtual void					Initialize() = 0;

	virtual GLFWwindow*				OpenScreen() = 0;
	virtual void					prepareNodeForRendering(irr::scene::SEvoSceneNode *node) = 0;
	virtual void					unregisterMeshForRendering(irr::scene::SEvoSceneNode* node) = 0;
	virtual void					PlaceStuff() = 0;
	virtual scene::ISceneNode*		CreateEvoTestNode(scene::ISceneNode *parent, const core::v3f &pos) = 0;
	virtual scene::EvoHudNode*		CreateHudSceneNode(scene::ISceneNode *parent) = 0;
	virtual	video::ITexture*		getTexture(const io::path& file) = 0;
	virtual u32						getShader(const StringPair& files) = 0;
	virtual scene::ISceneNode*		GetRootSceneNode() = 0;
	virtual void					SetRootSceneNode(scene::SEvoSceneNode* sceneNode) = 0;
	virtual void					setMaterial(const irr::video::SEvoMaterial* material) = 0;
	virtual void					Render() = 0;
	virtual void					Pause(const bool &pause) = 0;
	virtual void					GrabCursor(const bool& grab) = 0;

	virtual core::v3f				GetCameraPos() const = 0;
	virtual void					SetCameraPos(const core::v3f &position) = 0;
	virtual scene::ICameraSceneNode *GetCamera() const = 0;
	virtual scene::SEvoCameraNode	*GetEvoCamera() const = 0;

	virtual IrrlichtDevice			*GetDevice() = 0;
	virtual scene::ISceneManager	*GetSceneManager() = 0;
	virtual gui::IGUIEnvironment	*GetGuiEnvironment() = 0;

	virtual video::ITexture			*GetBlockTexture() = 0;
	virtual video::SMaterial		&GetBlockMaterial() = 0;
	virtual video::SEvoMaterial*	GetEvoMaterial() = 0;
	virtual u32						GetTime() = 0;

	virtual void					LockSceneNode() = 0;
	virtual void					UnlockSceneNode() = 0;
	virtual MutexLockerPtr			GetSceneNodeLocker() = 0;
};

#endif // IGRAPHICS_H
