#ifndef SEVOTESTNODE_H
#define SEVOTESTNODE_H

#include "sevoscenenode.h"

namespace irr {
namespace scene {

class SEvoTestNode : public SEvoSceneNode
{
public:
	SEvoTestNode(ISceneNode *parent, ISceneManager *smgr, s32 id, v3f position, IGraphics *graphics);
	~SEvoTestNode();
};


}; // irr::scene
}; // irr

#endif // SEVOTESTNODE_H
