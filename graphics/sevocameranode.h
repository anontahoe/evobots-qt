#ifndef SEVOCAMERANODE_H
#define SEVOCAMERANODE_H

#include "actor.h"

namespace irr {
namespace scene {
class SViewFrustum;

class SEvoCameraNode : public Actor
{
public:
								SEvoCameraNode(ISceneNode *parent, ISceneManager *smgr, s32 id, v3f position, IGraphics *graphics);
								~SEvoCameraNode();

	virtual void				render();
	//virtual void				setRotation(const vector3df &rotation);
	//virtual void				setPosition(const vector3df &newpos);
	virtual void				OnAnimate(u32 timeMs);

	virtual const SViewFrustum* getViewFrustum() const;
	const matrix4&				getViewMatrix() const;
	virtual void				setTarget(const v3f& target);
	void						createProjectionMatrix(const f32& fieldOfViewRadians, const f32& aspectRatio, const f32& zNear, const f32& zFar);

private:
	matrix4						viewMatrix;
	matrix4						projMatrix;
	SViewFrustum*				viewFrustum;
};


} // irr::scene
} // irr

#endif // SEVOCAMERANODE_H
