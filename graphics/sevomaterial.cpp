/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "sevomaterial.h"

namespace irr { namespace video {

SEvoMaterial::SEvoMaterial(const u32 &shaderProgramId, const u32 &modelViewProjId)
	: SMaterial(),
	  shaderProgramId(shaderProgramId),
	  modelViewProjId(modelViewProjId),
	  TextureIdLayer{}
{

}

SEvoMaterial::~SEvoMaterial() {
	this->modelViewProjId = 0;
	this->shaderProgramId = 0;
}

u32 SEvoMaterial::getTextureId(const u32 layer) const {
	if(layer < MATERIAL_MAX_TEXTURES) {
		return this->TextureIdLayer[layer];
	}
	return 0;
}

void SEvoMaterial::setTexture(u32 i, ITexture *tex, const u32& textureId) {
	SMaterial::setTexture(i, tex);
	if(i < MATERIAL_MAX_TEXTURES) {
		this->TextureIdLayer[i] = textureId;
	}
}


} // video
} // irr
