/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "sevocameranode.h"
#include "iengine.h"
#include "graphicsglew.h"
#include <irrlicht/SViewFrustum.h>


namespace irr {
namespace scene {


SEvoCameraNode::SEvoCameraNode(ISceneNode *parent, ISceneManager *smgr, s32 id, v3f position, IGraphics *graphics)
	: Actor(parent, smgr, id, position, graphics),
	  viewMatrix(matrix4()),
	  projMatrix(matrix4()),
	  viewFrustum(new SViewFrustum())
{
}

SEvoCameraNode::~SEvoCameraNode() {
	delete this->viewFrustum;
	this->viewFrustum = nullptr;
}

void SEvoCameraNode::render() {
	this->graphics->setTransform(ETS_VIEW, this->viewMatrix);
}

void SEvoCameraNode::OnAnimate(u32 timeMs) {
	this->AbsoluteTransformation.getInverse(this->viewMatrix);
	this->viewFrustum->setFrom(this->projMatrix * this->viewMatrix);
	Actor::OnAnimate(timeMs);	// must be called after setting forward vector
}

const SViewFrustum* SEvoCameraNode::getViewFrustum() const {
	return this->viewFrustum;
}

const matrix4& SEvoCameraNode::getViewMatrix() const {
	return this->viewMatrix;
}

void SEvoCameraNode::setTarget(const v3f &target) {
	v3f pos = this->RelativeTranslation;
	v3f dir = target - this->RelativeTranslation;
	dir.X *= -1;
	dir.Z *= -1;	// fucking righthandedness
	dir.normalize();
	f64 yRot = atan2(dir.X, dir.Z);
	f64 horizHypo = sqrt(1 - (dir.Y * dir.Y));
	f64 xRot = atan2(dir.Y, horizHypo);
	f64 yRotDeg = yRot * RADTODEG64;
	f64 xRotDeg = xRot * RADTODEG64;
	this->RelativeRotation.X = xRotDeg;
	this->RelativeRotation.Y = yRotDeg;
}

void SEvoCameraNode::createProjectionMatrix(const f32& fieldOfViewRadians, const f32& aspectRatio, const f32& zNear, const f32& zFar) {
	this->projMatrix.buildProjectionMatrixPerspectiveFovRH(fieldOfViewRadians, aspectRatio, zNear, zFar);
	this->graphics->setTransform(ETS_PROJECTION, this->projMatrix);
}


} // irr::scene
} // irr















