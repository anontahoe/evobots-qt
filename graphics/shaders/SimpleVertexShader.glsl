#version 300 core
layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec4 vertexColor;
layout(location = 3) in vec2 vertexUV;
uniform mat4 worldViewProj;

out vec4 fragmentColor;
out vec2 UV;

void main() {
	gl_Position = worldViewProj * vec4(vertexPosition_modelspace, 1);

	fragmentColor = vertexColor;
	UV = vertexUV;
}
