#version 300 core
in mediump vec4 fragmentColor;
in mediump vec2 UV;
uniform sampler2D textureSampler;

out lowp vec4 color;

void main() {
	//color = vec4(fragmentColor.r,fragmentColor.g, fragmentColor.b,1);
	color = texture2D(textureSampler, UV);
}


// GLSL 3.30 is not supported.
// Supported versions are: 1.10, 1.20, 1.30, 1.00 ES, and 3.00 ES
