#ifndef GRAPHICSGLEW_H
#define GRAPHICSGLEW_H

#include "igraphics.h"

class IChunkTableSceneNode;
namespace irr {
namespace scene {
	class SEvoSceneNode;
	class SEvoTestNode;
	class SEvoCameraNode;
	class EvoHudNode;
};
};
class ShaderLoader;
class IEngine;
class QMutex;


class GraphicsGlew : public IGraphics
{
public:
									GraphicsGlew(IEngine* engine);
	virtual							~GraphicsGlew();
	virtual void					Initialize();

	virtual GLFWwindow*				OpenScreen();
	virtual void					PlaceStuff();
	virtual scene::ISceneNode*		CreateEvoTestNode(scene::ISceneNode *parent, const core::v3f &pos);
	virtual IChunkTableSceneNode*	CreateChunkTableNode(u32 worldSize);
	virtual scene::EvoHudNode*		CreateHudSceneNode(scene::ISceneNode *parent);
	virtual void					prepareNodeForRendering(irr::scene::SEvoSceneNode *node);
	virtual void					unregisterMeshForRendering(irr::scene::SEvoSceneNode* node);
	virtual void					prepareRootNodeForRendering(irr::scene::SEvoSceneNode *node);
	virtual void					unregisterRootNodeForRendering(scene::SEvoSceneNode *node);
	virtual void					createVertexArray(u32 &arrayIdOut);
	virtual	video::ITexture*		getTexture(const io::path &file);
	virtual u32						getTextureId(video::ITexture *texture);
	virtual u32						getShader(const StringPair &files);
	virtual	scene::ISceneNode*		GetRootSceneNode();
	virtual void					SetRootSceneNode(scene::SEvoSceneNode *sceneNode);
	virtual void					setMaterial(const irr::video::SEvoMaterial* material);
	virtual void					setTransform(irr::video::E_TRANSFORMATION_STATE state, const core::matrix4& mat);
	virtual void					setFog(irr::video::SColor color=irr::video::SColor(0,255,255,255),
										   irr::video::E_FOG_TYPE fogType=irr::video::EFT_FOG_LINEAR,
										   f32 start=50.0f, f32 end=100.0f, f32 density=0.01f,
										   bool pixelFog=false, bool rangeFog=false);
	virtual void					addOcclusionQuery(scene::SEvoSceneNode *node);
	virtual void					runAllOcclusionQueries(bool visible=false);
	virtual void					updateAllOcclusionQueries(bool block=true);
	virtual u32						getOcclusionQueryResult(scene::ISceneNode* node) const;
	virtual void					removeOcclusionQuery(scene::ISceneNode* node);
	virtual void					removeAllOcclusionQueries();
	virtual void					renderSceneNode(const scene::SEvoSceneNode *node);
	virtual void					renderSceneNodePart(const scene::SEvoSceneNode* node, const u16& count, const u16& offset);
	virtual void					Render();
	virtual void					Pause(const bool &pause);
	virtual void					GrabCursor(const bool& grab);



	virtual core::v3f				GetCameraPos() const;
	virtual void					SetCameraPos(const core::v3f &position);
	virtual scene::ICameraSceneNode *GetCamera() const;
	virtual scene::SEvoCameraNode	*GetEvoCamera() const;

	virtual IrrlichtDevice*			GetDevice();
	virtual scene::ISceneManager*	GetSceneManager();
	virtual gui::IGUIEnvironment*	GetGuiEnvironment();

	virtual video::ITexture*		GetBlockTexture();
	virtual video::SMaterial&		GetBlockMaterial();
	virtual video::SEvoMaterial*	GetEvoMaterial();
	virtual u32						GetTime();

	virtual void					LockSceneNode();
	virtual void					UnlockSceneNode();
	virtual MutexLockerPtr			GetSceneNodeLocker();

private:
	IEngine*						engine;
	GLFWwindow*						window;
	ShaderLoader*					shaderLoader;
	QMutex*							renderLock;
	scene::SEvoSceneNode*			rootSceneNode;
	core::matrix4					matrices[video::ETS_COUNT];
	core::matrix4					viewProjection;
	core::matrix4					worldViewProjection;
	u32								wvpId;
	video::SEvoMaterial*			evoMaterial;
	video::TextureMap				loadedTextures;
	video::TextureIdMap				loadedTextureIds;
	u32								currentTextureId;
	u32								currentProgramId;
	scene::SEvoCameraNode*			evoCamera;

	bool paused;
};

#endif // GRAPHICSGLEW_H
