#ifndef SEVOSCENENODE_H
#define SEVOSCENENODE_H

#include "typedefs.h"

class QMutex;
class IGraphics;
class GraphicsGlew;

namespace irr {
using namespace core;
using namespace video;
namespace video {
	class SEvoMaterial;
} // namespace video
namespace scene {


struct SceneNodeMeta {
	bool valid = false;
	u32 VertexArrayId = 0;
	u32 VertexBufferId;
	u32 IndexBufferId;
	u32 ColorBufferId;
};

class SEvoSceneNode : public ISceneNode
{
public:
									SEvoSceneNode(ISceneNode *parent, ISceneManager *smgr, s32 id, v3f position, IGraphics *graphics);
									SEvoSceneNode(const SEvoSceneNode& other);
									~SEvoSceneNode();

	void							PrepareForRendering();
	void							ClearRenderingData();

	// ISceneNode
	virtual void					OnAnimate(u32 timeMs);
	virtual void					OnRegisterSceneNode();
	virtual void					render();
	virtual const bboxf&			getBoundingBox() const;
	virtual void					addChild(ISceneNode *child);	// replaces original implementation

	virtual const SEvoMaterial*		getEvoMaterial() const;
	virtual u32						getVertexCount() const;
	virtual const					S3DVertex *getVertices() const;
	virtual u32						getIndexCount() const;
	virtual const					u16 *getIndices() const;
	virtual void					Move(v3f relativeDirection);
	virtual void					Rotate(const v3f &relativeRotation);

	SceneNodeMeta					sceneNodeMeta;

protected:
	GraphicsGlew*					graphics;
	SEvoMaterial*					evoMaterial;
	QMutex*							renderMutex;
	VerticesArray					verticesArray;
	IndicesArray					indicesArray;
	core::bboxf						boundingBox;
	bool							toBeDestroyed;

private:
	static s32						nodesLoaded;
	static QMutex*					nodesLoadedLock;
};


} // namespace scene
} // namespace irr

#endif // SEVOSCENENODE_H
