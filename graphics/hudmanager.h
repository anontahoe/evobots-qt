#ifndef HUDMANAGER_H
#define HUDMANAGER_H

#include "typedefs.h"

namespace irr {
namespace scene {
	class EvoHudNode;
} // scene
} // irr

class IGraphics;

using namespace irr;

class HudManager
{
public:
	enum HudElement {
		HUD_CrossHair,
		HUD_Count
	};

							HudManager(IGraphics *graphics);
							~HudManager();
	void					Initialize();

	void					AddHudElement(const u8 &element);

private:
	IGraphics*				graphics;
	scene::EvoHudNode*		hudRootNode;
	core::v3f				hudPositions[HUD_Count];
	core::v3f				hudSize[HUD_Count];
	core::v2f				uvCoords[HUD_Count];
	core::v2f				uvSize[HUD_Count];
};



#endif // HUDMANAGER_H
