#ifndef SEVOTEST3NODE_H
#define SEVOTEST3NODE_H

#include "sevoscenenode.h"

namespace irr {
namespace scene {


class SEvoTest3Node : public SEvoSceneNode
{
public:
	SEvoTest3Node(ISceneNode *parent, ISceneManager *smgr, s32 id, v3f position, IGraphics *graphics);
	~SEvoTest3Node();
};


} // irr::scene
} // irr

#endif // SEVOTEST3NODE_H
