/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "sevotest3node.h"



namespace irr {
namespace scene {



SEvoTest3Node::SEvoTest3Node(ISceneNode *parent, ISceneManager *smgr, s32 id, v3f position, IGraphics *graphics)
	: SEvoSceneNode(parent, smgr, id, position, graphics)
{
	this->verticesArray.push_back(S3DVertex(v3f(0,0,0),v3f(0), SColor(255,0,0,255), v2f(0,0)));
	this->verticesArray.push_back(S3DVertex(v3f(0,1,0),v3f(0), SColor(255,0,0,255), v2f(0,0)));
	this->verticesArray.push_back(S3DVertex(v3f(1,1,0),v3f(0), SColor(255,0,0,255), v2f(0,0)));
	this->verticesArray.push_back(S3DVertex(v3f(1,0,0),v3f(0), SColor(255,0,0,255), v2f(0,0)));

	this->indicesArray.push_back(0);
	this->indicesArray.push_back(1);
	this->indicesArray.push_back(2);
	this->indicesArray.push_back(2);
	this->indicesArray.push_back(3);
	this->indicesArray.push_back(0);
}

SEvoTest3Node::~SEvoTest3Node()
{

}


} // irr::scene
} // irr





