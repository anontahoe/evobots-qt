#ifndef BLOCK_H
#define BLOCK_H

#include <irrlicht/irrlicht.h>

class Block
{
public:
	enum BlockType
	{
		BT_Air,
		BT_Water,
		BT_Dirt,
		BT_Grass,
		BT_Rock,
		BlockTypeCount,
		Bt_Invalid = (irr::u8)-1
	};

	enum BlockMetaType {
		BT_Transparent = (1 << BT_Air),		// not implemented yet
		BT_Opague = (1 << BT_Water) | (1 << BT_Rock) | (1 << BT_Dirt) | (1 << BT_Grass),
		BT_Solid = (1 << BT_Rock) | (1 << BT_Dirt) | (1 << BT_Grass)
	};

	Block(BlockType blockType=BT_Air);
	~Block();

	const irr::u8 &GetBlockType() const;
	const irr::u8 &GetBlockTypeBitMask() const;
	void SetBlockType(const irr::u8 &type);

private:   
	irr::u8 blockType;
};

struct BlockData {
	BlockData();
	static irr::u8 blockTypeBitmasks[Block::BlockTypeCount];
	static irr::u8 blockTypes[Block::BlockTypeCount];
};
typedef BlockData BD;
static BD blockDataInstance;

#endif // BLOCK_H
