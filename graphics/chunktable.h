#ifndef CHUNKTABLE_H
#define CHUNKTABLE_H

#include "typedefs.h"
#include "sevoscenenode.h"

using namespace irr;
using namespace scene;

class IEngine;
class IGraphics;
class GraphicsGlew;
class QReadWriteLock;

namespace irr {
namespace video {
	class SEvoMaterial;
} // video
namespace scene {
	class SEvoCameraNode;
} // scene
} // irr


class IChunkTableSceneNode : public scene::SEvoSceneNode {
public:
									IChunkTableSceneNode(ISceneNode *parent, ISceneManager* smgr, s32 id, v3f position, IGraphics *graphics);
									~IChunkTableSceneNode();

	virtual	void					OnAnimate(u32 timeMs);
	virtual	void					OnRegisterSceneNode();
	virtual	void					render();
	virtual	void					setVisible(bool isVisible);
	virtual	const core::bboxf&		getBoundingBox() const;

	ChunkPtr						GetChunkAt(const core::v3s& chunkPos) const;
	void							SetChunkAt(const core::v3s& chunkPos, ChunkPtr chunk);
	void							DropChunk(const core::v3s& chunkPos);
	void							PrepareChunkForRendering(const ChunkPtr& chunk);
	static core::v3s				PosToChunkCoord(const core::v3f &pos);
	static core::v3s				ChunkPosToHashPos(const core::v3s& chunkPos);

	static s32						chunkTableChunkCount;	// statistics
	static std::map<size_t,size_t>	chunksPerHashPos;
	s32								totalAdded;
	s32								totalReplaced;
	s32								totalRemoved;
	double							renderTimeStart;
	double							renderTime;

	ChunkPosChunkMap				chunksToSetBuffer;
	ChunkPosChunkMap				chunksToSet;
	ChunkPtrSet						chunksToCleanBuffer;
	ChunkPtrSet						chunksToClean;
	ChunkPtrSet						chunksToPrepareBuffer;
	ChunkPtrSet						chunksToPrepare;

private:
	void							RenderChunksXYZ(const v3s &hashPosStart, v3s& hashPos, const u8 &axisToRender, const bboxf& camBox, const bool& doOcclusionQueries);
	inline void						RenderChunksYZ(const v3s& hashPosStart, v3s& hashPos, u8& facesToRender, const u8 &axisToRender, const bboxf& camBox, const bool& doOcclusionQueries);
	inline void						RenderChunksZ(const v3s& hashPosStart, v3s& hashPos, u8& facesToRender, const u8& axisToRender, const bboxf &camBox, const bool &doOcclusionQueries);
	inline void						RenderChunksAt(const v3s &hashPos, const u8& facesToRender, const bboxf& camBox, const bool& doOcclusionQueries);

	GraphicsGlew*					graphics;
	//scene::ICameraSceneNode		*camera;
	//IrrlichtDevice				*device;
	//video::IVideoDriver			*videoDriver;
	scene::SEvoCameraNode*			camera;
	GraphicsGlew*					videoDriver;

	ChunkPtrList*					chunkTable;
	QMutex*							chunksToSetSwitchLock;
	QMutex*							chunksToCleanSwitchLock;
	QMutex*							chunksToPrepareSwitchLock;
	QReadWriteLock*					chunkListLock;

	core::aabbox3df					Box;

	u32								occlusionQueryTime;
	bool							retrieveOcclusionQueries;
};

#endif // CHUNKTABLE_H
