#ifndef SHADERLOADER_H
#define SHADERLOADER_H

#include "typedefs.h"

class ShaderLoader
{
public:
	ShaderLoader();
	~ShaderLoader();

	irr::u32 LoadShaders(std::string vertex_file_path, std::string fragment_file_path);

private:
	ShaderProgramMap shaderProgramMap;
};

#endif // SHADERLOADER_H
