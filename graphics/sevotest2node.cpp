/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "sevotest2node.h"

namespace irr {
namespace scene {


SEvoTest2Node::SEvoTest2Node(ISceneNode *parent, ISceneManager *smgr, s32 id, v3f position, IGraphics *graphics)
	: SEvoSceneNode(parent, smgr, id, position, graphics),
	  g_vertex_buffer_data{
						   0.0f, 0.0f, 0.0f,
						   0.0f, 1.0f, 0.0f,
						   1.0f, 1.0f, 0.0f,
						   1.0f, 0.0f, 0.0f},
	  g_index_buffer_data{ 0, 1, 2, 2, 3, 0 },
	  g_color_buffer_data{
		  0.583f,  0.771f,  0.014f,
		  0.609f,  0.115f,  0.436f,
		  0.327f,  0.483f,  0.844f,
		  0.822f,  0.569f,  0.201f,
		  0.435f,  0.602f,  0.223f,
		  0.310f,  0.747f,  0.185f,
		  0.597f,  0.770f,  0.761f,
		  0.559f,  0.436f,  0.730f,
		  0.359f,  0.583f,  0.152f,
		  0.483f,  0.596f,  0.789f,
		  0.559f,  0.861f,  0.639f,
		  0.195f,  0.548f,  0.859f,
		  0.014f,  0.184f,  0.576f,
		  0.771f,  0.328f,  0.970f,
		  0.406f,  0.615f,  0.116f,
		  0.676f,  0.977f,  0.133f,
		  0.971f,  0.572f,  0.833f,
		  0.140f,  0.616f,  0.489f,
		  0.997f,  0.513f,  0.064f,
		  0.945f,  0.719f,  0.592f,
		  0.543f,  0.021f,  0.978f,
		  0.279f,  0.317f,  0.505f,
		  0.167f,  0.620f,  0.077f,
		  0.347f,  0.857f,  0.137f,
		  0.055f,  0.953f,  0.042f,
		  0.714f,  0.505f,  0.345f,
		  0.783f,  0.290f,  0.734f,
		  0.722f,  0.645f,  0.174f,
		  0.302f,  0.455f,  0.848f,
		  0.225f,  0.587f,  0.040f,
		  0.517f,  0.713f,  0.338f,
		  0.053f,  0.959f,  0.120f,
		  0.393f,  0.621f,  0.362f,
		  0.673f,  0.211f,  0.457f,
		  0.820f,  0.883f,  0.371f,
		  0.982f,  0.099f,  0.879f
	  }
{
	for(u16 i = 0; i < 6; i++) {
		this->indicesArray.push_back(0);
	}
}

SEvoTest2Node::~SEvoTest2Node()
{

}



} // irr::scene
} // irr
