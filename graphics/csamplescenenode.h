#ifndef CSAMPLESCENENODE_H
#define CSAMPLESCENENODE_H

#include <irrlicht/irrlicht.h>
#include "typedefs.h"

using namespace irr;

class CSampleSceneNode : public scene::ISceneNode
{
	core::aabbox3d<f32> Box;
	core::array<video::S3DVertex> Vertices;
	core::array<u16> Indices;
	video::SMaterial Material;

public:
	CSampleSceneNode(scene::ISceneNode* parent, scene::ISceneManager* mgr, s32 id, core::v3s pos);

	virtual void OnRegisterSceneNode();


	virtual void render();


	virtual const core::aabbox3d<f32>& getBoundingBox() const;


	virtual u32 getMaterialCount() const;


	virtual video::SMaterial& getMaterial(u32 i);

};

#endif // CSAMPLESCENENODE_H
