/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "csamplescenenode.h"

using namespace irr;
using namespace core;
using namespace video;

CSampleSceneNode::CSampleSceneNode(scene::ISceneNode* parent, scene::ISceneManager* mgr, s32 id, core::v3s pos)
	: scene::ISceneNode(parent, mgr, id) {
	Material.Wireframe = false;
	Material.Lighting = false;

	/*Vertices.push_back(video::S3DVertex(0,0,0, 0,0,1,
				video::SColor(255,0,255,255), 0, 1));
		Vertices.push_back(video::S3DVertex(0,1,0, 0,0,1,
				video::SColor(255,255,0,255), 1, 1));
		Vertices.push_back(video::S3DVertex(1,1,0, 0,0,1,
				video::SColor(255,255,255,0), 1, 0));
		Vertices.push_back(video::S3DVertex(1,0,0, 0,0,1,
				video::SColor(255,0,255,255), 0, 0));*/

	s32 x,y,z; x=pos.X; y=pos.Y; z=pos.Z;

	//float blockSize = 1.0f;
	vector3df p1(x, y, z);
	vector3df p2(x, y+1, z);
	vector3df p3(x+1, y+1, z);
	vector3df p4(x+1, y, z);
	vector3df p5(x+1, y, z+1);
	vector3df p6(x+1, y+1, z+1);
	vector3df p7(x, y+1, z+1);
	vector3df p8(x, y, z+1);

	vector3df n1;

	SColor vertexColor(255, 255, 255, 255);

	// Front
	n1 = vector3df(-1, -1, 1);
	Vertices.push_back(S3DVertex(p1, n1, vertexColor, vector2df(0.5, 0)));
	n1 = vector3df(-1, 1, 1);
	Vertices.push_back(S3DVertex(p2, n1, vertexColor, vector2df(0.5, 0.1666)));
	n1 = vector3df(1, 1, 1);
	Vertices.push_back(S3DVertex(p3, n1, vertexColor, vector2df(0.6666, 0.1666)));
	n1 = vector3df(1, -1, 1);
	Vertices.push_back(S3DVertex(p4, n1, vertexColor, vector2df(0.6666, 0)));

	// Back
	n1 = vector3df(1, -1, -1);
	Vertices.push_back(S3DVertex(p5, n1, vertexColor, vector2df(0.5, 0)));
	n1 = vector3df(1, 1, -1);
	Vertices.push_back(S3DVertex(p6, n1, vertexColor, vector2df(0.5, 0.1666)));
	n1 = vector3df(-1, 1, -1);
	Vertices.push_back(S3DVertex(p7, n1, vertexColor, vector2df(0.6666, 0.1666)));
	n1 = vector3df(-1, -1, -1);
	Vertices.push_back(S3DVertex(p8, n1, vertexColor, vector2df(0.6666, 0)));

	// Front
	this->Indices.push_back(0 + this->Vertices.size() - 8);
	this->Indices.push_back(1 + this->Vertices.size() - 8);
	this->Indices.push_back(2 + this->Vertices.size() - 8);

	this->Indices.push_back(2 + this->Vertices.size() - 8);
	this->Indices.push_back(3 + this->Vertices.size() - 8);
	this->Indices.push_back(0 + this->Vertices.size() - 8);

	// Back
	this->Indices.push_back(4 + this->Vertices.size() - 8);
	this->Indices.push_back(5 + this->Vertices.size() - 8);
	this->Indices.push_back(6 + this->Vertices.size() - 8);

	this->Indices.push_back(6 + this->Vertices.size() - 8);
	this->Indices.push_back(7 + this->Vertices.size() - 8);
	this->Indices.push_back(4 + this->Vertices.size() - 8);

	// Left
	this->Indices.push_back(0 + this->Vertices.size() - 8);
	this->Indices.push_back(7 + this->Vertices.size() - 8);
	this->Indices.push_back(6 + this->Vertices.size() - 8);

	this->Indices.push_back(6 + this->Vertices.size() - 8);
	this->Indices.push_back(1 + this->Vertices.size() - 8);
	this->Indices.push_back(0 + this->Vertices.size() - 8);

	// Right
	this->Indices.push_back(3 + this->Vertices.size() - 8);
	this->Indices.push_back(2 + this->Vertices.size() - 8);
	this->Indices.push_back(5 + this->Vertices.size() - 8);

	this->Indices.push_back(5 + this->Vertices.size() - 8);
	this->Indices.push_back(4 + this->Vertices.size() - 8);
	this->Indices.push_back(3 + this->Vertices.size() - 8);

	// Top
	this->Indices.push_back(1 + this->Vertices.size() - 8);
	this->Indices.push_back(6 + this->Vertices.size() - 8);
	this->Indices.push_back(5 + this->Vertices.size() - 8);

	this->Indices.push_back(5 + this->Vertices.size() - 8);
	this->Indices.push_back(2 + this->Vertices.size() - 8);
	this->Indices.push_back(1 + this->Vertices.size() - 8);

	// Bottom
	this->Indices.push_back(0 + this->Vertices.size() - 8);
	this->Indices.push_back(3 + this->Vertices.size() - 8);
	this->Indices.push_back(4 + this->Vertices.size() - 8);

	this->Indices.push_back(4 + this->Vertices.size() - 8);
	this->Indices.push_back(7 + this->Vertices.size() - 8);
	this->Indices.push_back(0 + this->Vertices.size() - 8);


	/*for(int i = 4; i > 0; i--)
		{
			this->Indices.push_back(Vertices.size() - i);
		}*/

	Box.reset(Vertices[0].Pos);
	for(u32 i = 0; i < this->Vertices.size(); i++) {
		Box.addInternalPoint(Vertices[i].Pos);
	}
	/*for (s32 i=1; i<4; ++i)
			 Box.addInternalPoint(Vertices[i].Pos);*/
}

void CSampleSceneNode::OnRegisterSceneNode() {
	if (IsVisible)
		SceneManager->registerNodeForRendering(this);

	ISceneNode::OnRegisterSceneNode();
}

void CSampleSceneNode::render() {
	//u16 indices[] = {   0,2,3, 2,1,3, 1,0,3, 2,0,1  };
	video::IVideoDriver* driver = SceneManager->getVideoDriver();

	driver->setMaterial(Material);
	driver->setTransform(video::ETS_WORLD, AbsoluteTransformation);
	driver->drawVertexPrimitiveList(&Vertices[0], this->Vertices.size(), &this->Indices[0], this->Indices.size() / 3, video::EVT_STANDARD, scene::EPT_TRIANGLES, video::EIT_16BIT);
}

const core::aabbox3d<f32>& CSampleSceneNode::getBoundingBox() const {
	return Box;
}

u32 CSampleSceneNode::getMaterialCount() const {
	return 1;
}

video::SMaterial& CSampleSceneNode::getMaterial(u32 i) {
	i = i;
	return Material;
}

