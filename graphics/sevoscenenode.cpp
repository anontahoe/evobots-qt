/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "sevoscenenode.h"
#include "graphicsglew.h"
#include "igraphics.h"
#include "sevomaterial.h"

namespace irr {
using namespace core;
using namespace video;
namespace scene {

s32 SEvoSceneNode::nodesLoaded(0);
QMutex* SEvoSceneNode::nodesLoadedLock(new QMutex());

SEvoSceneNode::SEvoSceneNode(ISceneNode *parent, ISceneManager *smgr, s32 id, v3f position, IGraphics* graphics)
	: ISceneNode(parent, smgr, id, position),
	  sceneNodeMeta(SceneNodeMeta()),
	  graphics(dynamic_cast<GraphicsGlew*>(graphics)),
	  evoMaterial(nullptr),
	  renderMutex(new QMutex()),
	  verticesArray(VerticesArray()),
	  indicesArray(IndicesArray()),
	  boundingBox(bboxf()),
	  toBeDestroyed(false)
{
	SEvoSceneNode::nodesLoadedLock->lock();
	SEvoSceneNode::nodesLoaded++;
	SEvoSceneNode::nodesLoadedLock->unlock();
}

SEvoSceneNode::SEvoSceneNode(const SEvoSceneNode &other)
	: ISceneNode(other),
	  sceneNodeMeta(other.sceneNodeMeta),
	  graphics(other.graphics),
	  evoMaterial(other.evoMaterial),
	  renderMutex(other.renderMutex),
	  verticesArray(other.verticesArray),
	  indicesArray(other.indicesArray),
	  boundingBox(other.boundingBox),
	  toBeDestroyed(other.toBeDestroyed)
{
	throw;
}

SEvoSceneNode::~SEvoSceneNode() {
	ClearRenderingData();

	this->indicesArray.clear();
	this->verticesArray.clear();

	delete this->renderMutex;
	this->renderMutex = nullptr;

	//delete this->sceneNodeMeta;
	//this->sceneNodeMeta = nullptr;

	SEvoSceneNode::nodesLoadedLock->lock();
	SEvoSceneNode::nodesLoaded--;
	if(SEvoSceneNode::nodesLoaded == 5) {
		int a = 0;
	}
	SEvoSceneNode::nodesLoadedLock->unlock();
}

void SEvoSceneNode::PrepareForRendering() {
	//if(this->sceneNodeMeta != nullptr) {
	//	this->engine->unregisterMeshForRendering(this);
	//}

	ClearRenderingData();
	this->renderMutex->lock();
	if(!this->indicesArray.empty()) {
		this->graphics->prepareNodeForRendering(this);
	}
	this->renderMutex->unlock();
}

void SEvoSceneNode::ClearRenderingData() {
	this->renderMutex->lock();
	if(this->sceneNodeMeta.valid) {
		this->graphics->unregisterMeshForRendering(this);
	}
	this->renderMutex->unlock();
}

void SEvoSceneNode::OnAnimate(u32 timeMs) {
	ISceneNode::OnAnimate(timeMs);
}

void SEvoSceneNode::OnRegisterSceneNode() {

	if(!this->toBeDestroyed && IsVisible)
		SceneManager->registerNodeForRendering(this);

	ISceneNode::OnRegisterSceneNode();
}

void SEvoSceneNode::render() {
	if(this->toBeDestroyed)
		return;

	this->renderMutex->lock();

	for(ISceneNode* node : this->Children) {
		node->render();
	}

	if(!this->sceneNodeMeta.valid) {
		this->renderMutex->unlock();
		return;
	}

	if(!this->indicesArray.count()) {
		this->renderMutex->unlock();
		return;
	}

	//video::IVideoDriver *driver = this->SceneManager->getVideoDriver();

	this->graphics->setMaterial(getEvoMaterial());
	this->graphics->setTransform(ETS_WORLD, this->AbsoluteTransformation);
	this->graphics->renderSceneNode(this);

	this->renderMutex->unlock();
}

const bboxf& SEvoSceneNode::getBoundingBox() const {
	return this->boundingBox;
}

/*const bboxf& SEvoSceneNode::getAbsoluteBoundingBox() const {
	return bboxf(this->boundingBox.MinEdge + this->getAbsolutePosition(), this->boundingBox.MaxEdge + this->getAbsolutePosition());
}*/

void SEvoSceneNode::addChild(ISceneNode *child) {
	this->renderMutex->lock();
	ISceneNode::addChild(child);
	this->renderMutex->unlock();
}


const SEvoMaterial* SEvoSceneNode::getEvoMaterial() const {
	return this->graphics->GetEvoMaterial();
}

u32 SEvoSceneNode::getVertexCount() const {
	return this->verticesArray.count();
}

const S3DVertex* SEvoSceneNode::getVertices() const{
	return this->verticesArray.constData();
}

u32 SEvoSceneNode::getIndexCount() const {
	return this->indicesArray.count();
}

const u16* SEvoSceneNode::getIndices() const {
	return this->indicesArray.constData();
}

void SEvoSceneNode::Move(v3f relativeDirection) {
	this->getAbsoluteTransformation().rotateVect(relativeDirection);
	this->setPosition(this->getPosition() + relativeDirection);
}

void SEvoSceneNode::Rotate(const v3f& relativeRotation) {
	matrix4 rota;
	rota.setRotationDegrees(relativeRotation);
	this->AbsoluteTransformation = rota * this->AbsoluteTransformation;
	this->setRotation(this->AbsoluteTransformation.getRotationDegrees());
}


} // namespace scene
} // namespace irr
