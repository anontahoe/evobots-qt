/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "graphicsglew.h"
#include "utils.h"
#include "engine.h"
#include "inputmanager.h"
#include "logger.h"
#include "shaderloader.h"
#include "pngloader.h"
#include "chunk.h"
#include "sevotexture.h"
#include "sevomaterial.h"
#include "sevocameranode.h"
#include "sevoscenenode.h"
#include "sevotestnode.h"
#include "sevotest2node.h"
#include "sevotest3node.h"
#include "evohudnode.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <qmutex.h>

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;

enum ShaderAttrib {
	Pos,
	Normal,
	Color,
	UV
};

GraphicsGlew::GraphicsGlew(IEngine *engine)
	: engine(engine),
	  window(nullptr),
	  shaderLoader(new ShaderLoader()),
	  renderLock(new QMutex(QMutex::Recursive)),
	  rootSceneNode(nullptr),
	  matrices{},
	  viewProjection(matrix4()),
	  worldViewProjection(matrix4()),
	  wvpId(0),
	  evoMaterial(nullptr),
	  loadedTextures(TextureMap()),
	  loadedTextureIds(TextureIdMap()),
	  currentTextureId(0),
	  currentProgramId(0),
	  evoCamera(nullptr),
	  paused(false)
{
}

GraphicsGlew::~GraphicsGlew() {
	this->evoCamera = nullptr;

	for(std::pair<io::path, ITexture*> pathTexture : this->loadedTextures) {
		delete pathTexture.second;
	}
	this->loadedTextureIds.clear();
	this->loadedTextures.clear();

	delete this->evoMaterial;
	this->evoMaterial = nullptr;

	if(this->rootSceneNode != nullptr) {
		unregisterRootNodeForRendering(this->rootSceneNode);
		delete this->rootSceneNode;
		this->rootSceneNode = nullptr;
	}

	delete this->renderLock;
	this->renderLock = nullptr;

	delete this->shaderLoader;
	this->shaderLoader = nullptr;

	//delete this->window;
	this->window = nullptr;
	this->engine = nullptr;

	glfwTerminate();
}

void GraphicsGlew::Initialize() {
	if(!glfwInit()) {
		Logger::Log("Error: could not start GLFW3");
	}
}
#include <GL/glcorearb.h>
void GraphicsGlew::prepareNodeForRendering(SEvoSceneNode* node) {
	this->renderLock->lock();
	glfwMakeContextCurrent(this->window);
	if(node->sceneNodeMeta.valid) {
		unregisterMeshForRendering(node);
	}

	createVertexArray(node->sceneNodeMeta.VertexArrayId);

	// vertex buffer (pos, normal, color, uv)
	glGenBuffers(1, &node->sceneNodeMeta.VertexBufferId);
	glBindBuffer(GL_ARRAY_BUFFER, node->sceneNodeMeta.VertexBufferId);
	glBufferData(GL_ARRAY_BUFFER, node->getVertexCount() * sizeof(S3DVertex), node->getVertices(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(ShaderAttrib::Pos);
	glVertexAttribPointer(
				ShaderAttrib::Pos,
				3,
				GL_FLOAT,
				GL_FALSE,
				sizeof(S3DVertex),
				(void*)0
				);
	glEnableVertexAttribArray(ShaderAttrib::Normal);
	glVertexAttribPointer(
				ShaderAttrib::Normal,
				3,
				GL_FLOAT,
				GL_FALSE,
				sizeof(S3DVertex),
				(void*)(3*4)
				);
	glEnableVertexAttribArray(ShaderAttrib::Color);
	glVertexAttribPointer(
				ShaderAttrib::Color,
				4,
				GL_UNSIGNED_BYTE,
				GL_TRUE,
				sizeof(S3DVertex),
				(void*)(6*4)
				);
	glEnableVertexAttribArray(ShaderAttrib::UV);
	glVertexAttribPointer(
				ShaderAttrib::UV,
				2,
				GL_FLOAT,
				GL_FALSE,
				sizeof(S3DVertex),
				(void*)(7*4)
				);

	// index buffer
	glGenBuffers(1, &node->sceneNodeMeta.IndexBufferId);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, node->sceneNodeMeta.IndexBufferId);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, node->getIndexCount() * sizeof(u16), node->getIndices(), GL_STATIC_DRAW);

	node->sceneNodeMeta.valid = true;

	this->renderLock->unlock();
}

void GraphicsGlew::unregisterMeshForRendering(SEvoSceneNode *node) {

	this->renderLock->lock();

	glBindVertexArray(0);
	glDeleteBuffers(1, &node->sceneNodeMeta.VertexBufferId);
	glDeleteBuffers(1, &node->sceneNodeMeta.IndexBufferId);
	glDeleteVertexArrays(1, &node->sceneNodeMeta.VertexArrayId);
	node->sceneNodeMeta.valid = false;

	this->renderLock->unlock();
}

void GraphicsGlew::prepareRootNodeForRendering(SEvoSceneNode *node) {
	this->renderLock->lock();
	createVertexArray(node->sceneNodeMeta.VertexArrayId);
	prepareNodeForRendering(node);
	this->renderLock->unlock();
}

void GraphicsGlew::unregisterRootNodeForRendering(SEvoSceneNode *node) {
	this->renderLock->lock();
	unregisterMeshForRendering(node);
	this->renderLock->unlock();
}

void GraphicsGlew::createVertexArray(u32& arrayIdOut) {
	glGenVertexArrays(1, &arrayIdOut);
	glBindVertexArray(arrayIdOut);
}

void ErrorCallback(int errorCode, const char* errorDesc) {
	std::string desc(errorDesc);
	int error = errorCode;
}

GLFWwindow *GraphicsGlew::OpenScreen() {
	glfwSetErrorCallback(ErrorCallback);

	//glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	//glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2); // We want OpenGL 3.3
	//glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	//glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL

	// Open a window and create its OpenGL context
	this->window = glfwCreateWindow( 1280, 1024, "Tutorial 01", glfwGetPrimaryMonitor(), NULL);
	if( this->window == NULL ){
		Logger::Log(std::string("Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials."));
		glfwTerminate();
		return nullptr;
	}
	glfwMakeContextCurrent(this->window); // Initialize GLEW
	glewExperimental=true; // Needed in core profile
	if (glewInit() != GLEW_OK) {
		Logger::Log(std::string("Failed to initialize GLEW"));
		return nullptr;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(this->window, GLFW_STICKY_KEYS, GL_TRUE);
	//glfwSetInputMode(this->window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	glClearColor(0.0, 0.0, 0.0, 0.0);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	//glfwSwapInterval(1);

	std::ustring version(glGetString(GL_VERSION));

	return this->window;
	return nullptr;
}

void GraphicsGlew::PlaceStuff() {
	u32 shader = getShader(std::make_pair("../media/shaders/SimpleVertexShader.glsl",
										  "../media/shaders/SimpleFragmentShader.glsl"));
	u32 wvpId = glGetUniformLocation(shader, "worldViewProj");
	ITexture* texture = getTexture("../media/blocktexturesflipped-pow2.png");
	this->evoMaterial = new SEvoMaterial(shader, wvpId);
	this->evoMaterial->setTexture(0, texture, getTextureId(texture));

	SEvoCameraNode* cam(new SEvoCameraNode(nullptr, nullptr, 0, v3f(5,5, 5), this));
	this->evoCamera = cam;
	this->evoCamera->setTarget(v3f(0));
	this->evoCamera->createProjectionMatrix(glm::radians(90.0f), 5.0f/4.0f, 0.1f, 600.0f);

	SEvoSceneNode* node(new SEvoTestNode(nullptr, nullptr, 0, v3f(0), this));
	SetRootSceneNode(node);
	node->setVisible(true);

	SEvoSceneNode* node2(new SEvoTestNode(node, nullptr, 0, v3f(0, 1, 0), this));
	node2->setVisible(true);
	node2->PrepareForRendering();
	SEvoSceneNode* node3(new SEvoTestNode(node, nullptr, 0, v3f(0, 2, 0), this));
	node3->setVisible(true);
	node3->PrepareForRendering();
	SEvoSceneNode* node4(new SEvoTestNode(node, nullptr, 0, v3f(0, 3, 0), this));
	node4->setVisible(true);
	node4->PrepareForRendering();
	SEvoSceneNode* node5(new SEvoTestNode(node, nullptr, 0, v3f(0, 3, 1), this));
	node5->setVisible(true);
	node5->PrepareForRendering();
	SEvoSceneNode* node6(new SEvoTestNode(node, nullptr, 0, v3f(1, 3, 0), this));
	node6->setVisible(true);
	node6->PrepareForRendering();
}

ISceneNode* GraphicsGlew::CreateEvoTestNode(ISceneNode* parent, const v3f &pos) {
	SEvoSceneNode* node(new SEvoTestNode(parent, nullptr, 0, pos, this));
	node->setVisible(true);
	node->PrepareForRendering();
}

IChunkTableSceneNode* GraphicsGlew::CreateChunkTableNode(u32 worldSize) {
	worldSize = worldSize;
	engine = engine;
	return nullptr;
}

EvoHudNode* GraphicsGlew::CreateHudSceneNode(ISceneNode *parent) {
	EvoHudNode* hudNode(new EvoHudNode(parent, nullptr, -1, v3f(0), v3f(0), v2f(0), v2f(0), this));
	hudNode->setVisible(true);
	return hudNode;
}

ITexture* GraphicsGlew::getTexture(const io::path& file) {
	TextureMap::iterator textureIter = this->loadedTextures.find(file);
	if(textureIter != this->loadedTextures.end()) {
		return (*textureIter).second;
	}

	SEvoTexture* texture = new SEvoTexture(file);
	this->loadedTextures[file] = texture;
	return texture;
}

u32 GraphicsGlew::getTextureId(ITexture* texture) {
	//glfwMakeContextCurrent(this->window);
	TextureIdMap::iterator textureIdIter = this->loadedTextureIds.find(texture);
	if(textureIdIter != this->loadedTextureIds.end()) {
		return (*textureIdIter).second;
	}

	// create texture buffer
	u32 textureId;
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_2D, textureId);
	bool hasAlpha = true;
	void* textureData = texture->lock(ETLM_READ_ONLY);
	glTexImage2D(GL_TEXTURE_2D,
				 0,
				 hasAlpha ? GL_RGBA : GL_RGB,
				 texture->getSize().Width,
				 texture->getSize().Height,
				 0,
				 hasAlpha ? GL_RGBA : GL_RGB,
				 GL_UNSIGNED_BYTE,
				 textureData);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	//glGenerateMipmap(GL_TEXTURE_2D);
	//glShadeModel(GL_FLAT);
	glDeleteBuffers(1, &textureId);

	this->loadedTextureIds[texture] = textureId;
	this->currentTextureId = textureId;
	return textureId;
}

u32 GraphicsGlew::getShader(const StringPair &files) {
	return this->shaderLoader->LoadShaders(files.first, files.second);
}

ISceneNode* GraphicsGlew::GetRootSceneNode() {
	return this->rootSceneNode;
}

void GraphicsGlew::SetRootSceneNode(SEvoSceneNode *sceneNode) {
	prepareRootNodeForRendering(sceneNode);
	this->rootSceneNode = sceneNode;
}

void GraphicsGlew::setMaterial(const SEvoMaterial *material){
	//glfwMakeContextCurrent(this->window);
	if(this->currentProgramId != material->shaderProgramId) {
		glUseProgram(material->shaderProgramId);
		this->wvpId = glGetUniformLocation(material->shaderProgramId, "worldViewProj");
		this->currentProgramId = material->shaderProgramId;
	}
	const u32 materialTextureId = material->getTextureId(0);
	if(this->currentTextureId != materialTextureId) {
		this->currentTextureId = materialTextureId;
		glBindTexture(GL_TEXTURE_2D, materialTextureId);
	}
}

void GraphicsGlew::setTransform(irr::video::E_TRANSFORMATION_STATE state, const core::matrix4& mat) {
	this->matrices[state] = mat;

	if((state == ETS_VIEW) || (state == ETS_PROJECTION)) {
		this->viewProjection =
				this->matrices[ETS_PROJECTION] *
				this->matrices[ETS_VIEW];
	} else if(state & ETS_WORLD) {
		this->worldViewProjection = viewProjection * this->matrices[ETS_WORLD];
		glUniformMatrix4fv(this->wvpId, 1, GL_FALSE, &this->worldViewProjection[0]);
	}
}

void GraphicsGlew::setFog(SColor color, E_FOG_TYPE fogType, f32 start, f32 end,
						  f32 density, bool pixelFog, bool rangeFog) {
	color=color;fogType=fogType;start=start;end=end;density=density;
	pixelFog=pixelFog;rangeFog=rangeFog;
}

void GraphicsGlew::addOcclusionQuery(scene::SEvoSceneNode* node) {
	node = node;
}

void GraphicsGlew::runAllOcclusionQueries(bool visible) {
	visible = visible;
}

void GraphicsGlew::updateAllOcclusionQueries(bool block) {
	block = block;
}

u32 GraphicsGlew::getOcclusionQueryResult(ISceneNode* node) const {
	node = node;
	return 1;
}

void GraphicsGlew::removeOcclusionQuery(ISceneNode* node) {
	node = node;
}

void GraphicsGlew::removeAllOcclusionQueries() {

}

void GraphicsGlew::renderSceneNode(const SEvoSceneNode* node) {
	glBindVertexArray(node->sceneNodeMeta.VertexArrayId);
	//glUniformMatrix4fv(this->wvpId, 1, GL_FALSE, &this->worldViewProjection[0]);
	glDrawElements(GL_TRIANGLES, node->getIndexCount(), GL_UNSIGNED_SHORT, (void*)0);
}

void GraphicsGlew::renderSceneNodePart(const SEvoSceneNode* node, const u16& count, const u16& offset) {
	glBindVertexArray(node->sceneNodeMeta.VertexArrayId);
	//glUniformMatrix4fv(this->wvpId, 1, GL_FALSE, &this->worldViewProjection[0]);
	glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_SHORT, (void*)(sizeof(u16) * offset));
}

void GraphicsGlew::Render() {
	this->renderLock->lock();

	//glfwMakeContextCurrent(this->window);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	this->evoCamera->OnAnimate(utils::Timer.dTimeMs);
	this->evoCamera->render();

	if (this->rootSceneNode != nullptr) {
		this->rootSceneNode->OnAnimate(utils::Timer.dTimeMs);
		this->rootSceneNode->render();
	}

	//glFlush();
	//glFinish();
	glfwSwapBuffers(this->window);
	//std::this_thread::sleep_for(std::chrono::duration<double,std::milli>(150));

	this->renderLock->unlock();
}

void GraphicsGlew::Pause(const bool &pause) {
	this->paused = pause;
}

void GraphicsGlew::GrabCursor(const bool &grab) {
	glfwSetInputMode(this->window, GLFW_CURSOR, grab ? GLFW_CURSOR_DISABLED : GLFW_CURSOR_NORMAL);
}

core::v3f GraphicsGlew::GetCameraPos() const {
	return this->evoCamera->getPosition();
}

void GraphicsGlew::SetCameraPos(const core::v3f &position) {
	this->evoCamera->setPosition(position);
}

scene::ICameraSceneNode *GraphicsGlew::GetCamera() const {
	return nullptr;
}

scene::SEvoCameraNode* GraphicsGlew::GetEvoCamera() const {
	return this->evoCamera;
}

IrrlichtDevice* GraphicsGlew::GetDevice() {
	return nullptr;
}

scene::ISceneManager* GraphicsGlew::GetSceneManager() {
	return nullptr;
}

gui::IGUIEnvironment* GraphicsGlew::GetGuiEnvironment() {
	return nullptr;
}

video::ITexture *GraphicsGlew::GetBlockTexture() {
	return nullptr;
}

video::SMaterial &GraphicsGlew::GetBlockMaterial() {
	return *this->evoMaterial;
}

video::SEvoMaterial* GraphicsGlew::GetEvoMaterial() {
	return this->evoMaterial;
}

u32 GraphicsGlew::GetTime() {
	return glfwGetTime() * 1000;
}

void GraphicsGlew::LockSceneNode() {

}
void GraphicsGlew::UnlockSceneNode() {

}
MutexLockerPtr GraphicsGlew::GetSceneNodeLocker() {
	return MutexLockerPtr();
}
