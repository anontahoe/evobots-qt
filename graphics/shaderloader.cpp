/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "shaderloader.h"
#include "logger.h"
#include <GL/glew.h>
//#include <GLFW/glfw3.h>
//#include <glm/glm.hpp>
//#include <stdio.h>
#include <string>
#include <fstream>
#include <vector>
#include <utility>

using namespace irr;

ShaderLoader::ShaderLoader()
	: shaderProgramMap(ShaderProgramMap())
{
}

ShaderLoader::~ShaderLoader() {
	this->shaderProgramMap.clear();
}

u32 ShaderLoader::LoadShaders(std::string vertex_file_path, std::string fragment_file_path){

	StringPair shaderPaths = std::make_pair(vertex_file_path, fragment_file_path);
	ShaderProgramMap::const_iterator shaderProgramId = this->shaderProgramMap.find(shaderPaths);
	if(shaderProgramId != this->shaderProgramMap.end()) {
		return (*shaderProgramId).second;
	}

	// Create the shaders
	u32 VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	u32 FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	// Read the Vertex Shader code from the file
	std::string VertexShaderCode;
	std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
	if(VertexShaderStream.is_open())
	{
		std::string Line = "";
		while(getline(VertexShaderStream, Line))
			VertexShaderCode += "\n" + Line;
		VertexShaderStream.close();
	}

	// Read the Fragment Shader code from the file
	std::string FragmentShaderCode;
	std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
	if(FragmentShaderStream.is_open()){
		std::string Line = "";
		while(getline(FragmentShaderStream, Line))
			FragmentShaderCode += "\n" + Line;
		FragmentShaderStream.close();
	}

	s32 Result = GL_FALSE;
	int InfoLogLength;

	// Compile Vertex Shader
	Logger::Log("Compiling shader : " + std::string(vertex_file_path));
	char const * VertexSourcePointer = VertexShaderCode.c_str();
	glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
	glCompileShader(VertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> VertexShaderErrorMessage(InfoLogLength);
	glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
	Logger::Log(std::string(&VertexShaderErrorMessage[0]));

	// Compile Fragment Shader
	Logger::Log("Compiling shader : " + std::string(fragment_file_path));
	char const * FragmentSourcePointer = FragmentShaderCode.c_str();
	glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> FragmentShaderErrorMessage(InfoLogLength);
	glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
	Logger::Log(std::string(&FragmentShaderErrorMessage[0]));

	// Link the program
	Logger::Log("Linking program");
	u32 ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> ProgramErrorMessage( std::max(InfoLogLength, int(1)) );
	glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
	Logger::Log(std::string(&ProgramErrorMessage[0]));

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	this->shaderProgramMap[shaderPaths] = ProgramID;

	return ProgramID;
}
