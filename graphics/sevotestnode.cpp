/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "sevotestnode.h"


namespace irr {
namespace scene {


SEvoTestNode::SEvoTestNode(ISceneNode *parent, ISceneManager *smgr, s32 id, v3f position, IGraphics *graphics)
	: SEvoSceneNode(parent, smgr, id, position, graphics)
{
	//v3f pos = position;
	s32 x(0),y(0),z(0); //x=pos.X; y=pos.Y; z=pos.Z;

	//float blockSize = 1.0f;
	vector3df p1(x, y, z);
	vector3df p2(x, y+1, z);
	vector3df p3(x+1, y+1, z);
	vector3df p4(x+1, y, z);
	vector3df p5(x+1, y, z+1);
	vector3df p6(x+1, y+1, z+1);
	vector3df p7(x, y+1, z+1);
	vector3df p8(x, y, z+1);

	vector3df n1;

	SColor vertexColor(255, 255, 255, 255);

	// Front
	n1 = vector3df(-1, -1, 1);
	this->verticesArray.push_back(S3DVertex(p1, n1, vertexColor, vector2df(0.5, 1-0)));
	n1 = vector3df(-1, 1, 1);
	this->verticesArray.push_back(S3DVertex(p2, n1, vertexColor, vector2df(0.5, 1-0.1666)));
	n1 = vector3df(1, 1, 1);
	this->verticesArray.push_back(S3DVertex(p3, n1, vertexColor, vector2df(0.6666, 1-0.1666)));
	n1 = vector3df(1, -1, 1);
	this->verticesArray.push_back(S3DVertex(p4, n1, vertexColor, vector2df(0.6666, 1-0)));

	// Back
	n1 = vector3df(1, -1, -1);
	this->verticesArray.push_back(S3DVertex(p5, n1, vertexColor, vector2df(0.5, 1-0)));
	n1 = vector3df(1, 1, -1);
	this->verticesArray.push_back(S3DVertex(p6, n1, vertexColor, vector2df(0.5, 1-0.1666)));
	n1 = vector3df(-1, 1, -1);
	this->verticesArray.push_back(S3DVertex(p7, n1, vertexColor, vector2df(0.6666, 1-0.1666)));
	n1 = vector3df(-1, -1, -1);
	this->verticesArray.push_back(S3DVertex(p8, n1, vertexColor, vector2df(0.6666, 1-0)));

	// Front
	this->indicesArray.push_back(0);
	this->indicesArray.push_back(1);
	this->indicesArray.push_back(2);

	this->indicesArray.push_back(2);
	this->indicesArray.push_back(3);
	this->indicesArray.push_back(0);

	// Back
	this->indicesArray.push_back(4);
	this->indicesArray.push_back(5);
	this->indicesArray.push_back(6);

	this->indicesArray.push_back(6);
	this->indicesArray.push_back(7);
	this->indicesArray.push_back(4);

	// Left
	this->indicesArray.push_back(0);
	this->indicesArray.push_back(7);
	this->indicesArray.push_back(6);

	this->indicesArray.push_back(6);
	this->indicesArray.push_back(1);
	this->indicesArray.push_back(0);

	// Right
	this->indicesArray.push_back(3);
	this->indicesArray.push_back(2);
	this->indicesArray.push_back(5);

	this->indicesArray.push_back(5);
	this->indicesArray.push_back(4);
	this->indicesArray.push_back(3);

	// Top
	this->indicesArray.push_back(1);
	this->indicesArray.push_back(6);
	this->indicesArray.push_back(5);

	this->indicesArray.push_back(5);
	this->indicesArray.push_back(2);
	this->indicesArray.push_back(1);

	// Bottom
	this->indicesArray.push_back(0);
	this->indicesArray.push_back(3);
	this->indicesArray.push_back(4);

	this->indicesArray.push_back(4);
	this->indicesArray.push_back(7);
	this->indicesArray.push_back(0);


	/*for(int i = 4; i > 0; i--)
		{
			this->indicesArray.push_back(this->verticesArray.size() - i);
		}*/

	this->boundingBox.reset(this->verticesArray[0].Pos);
	for(int i = 0; i < this->verticesArray.size(); i++) {
		this->boundingBox.addInternalPoint(verticesArray[i].Pos);
	}
	/*for (s32 i=1; i<4; ++i)
			 Box.addInternalPoint(verticesArray[i].Pos);*/
}

SEvoTestNode::~SEvoTestNode() {

}


} // irr::scene
} // irr

