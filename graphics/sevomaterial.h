#ifndef ICHUNKMATERIALLAYER_H
#define ICHUNKMATERIALLAYER_H

#include "typedefs.h"

namespace irr { namespace video {

class SEvoMaterial : public SMaterial
{
public:
					SEvoMaterial(const u32& shaderProgramId, const u32& modelViewProjId);
	virtual			~SEvoMaterial();

	virtual u32		getTextureId(const u32 layer) const;
	virtual void	setTexture(u32 i, ITexture *tex, const u32 &textureId);

	u32				shaderProgramId;
	u32				modelViewProjId;

private:
	u32				TextureIdLayer[MATERIAL_MAX_TEXTURES];
};

} // video
} // irr

#endif // ICHUNKMATERIALLAYER_H
