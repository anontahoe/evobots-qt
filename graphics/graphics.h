#ifndef GRAPHICS_H
#define GRAPHICS_H

#include "igraphics.h"

using namespace irr;

class IEngine;

class Graphics : public IGraphics {
public:
									Graphics(IEngine *engine);
	virtual							~Graphics();
	virtual void					Initialize();

	virtual GLFWwindow*				OpenScreen();
	virtual void					PlaceStuff();
	virtual void					prepareNodeForRendering(irr::scene::SEvoSceneNode *node);
	virtual void					unregisterMeshForRendering(irr::scene::SEvoSceneNode* node);
	virtual scene::ISceneNode*		CreateEvoTestNode(scene::ISceneNode *parent, const core::v3f &pos);
	virtual scene::EvoHudNode*		CreateHudSceneNode(scene::ISceneNode *parent);
	virtual	video::ITexture*		getTexture(const io::path& file);
	virtual u32						getShader(const StringPair &files);
	virtual scene::ISceneNode		*GetRootSceneNode();
	virtual void					SetRootSceneNode(scene::SEvoSceneNode* sceneNode);
	virtual void					setMaterial(const video::SEvoMaterial *material);
	virtual void					Render();
	virtual void					Pause(const bool &pause);
	virtual void					GrabCursor(const bool& grab);

	virtual core::v3f				GetCameraPos() const;
	virtual void					SetCameraPos(const core::v3f &position);
	virtual scene::ICameraSceneNode *GetCamera() const;
	virtual scene::SEvoCameraNode	*GetEvoCamera() const;

	virtual IrrlichtDevice*			GetDevice();
	virtual scene::ISceneManager*	GetSceneManager();
	virtual gui::IGUIEnvironment*	GetGuiEnvironment();

	virtual video::ITexture*		GetBlockTexture();
	virtual video::SMaterial		&GetBlockMaterial();
	virtual video::SEvoMaterial*	GetEvoMaterial();
	virtual u32						GetTime();

	virtual void					LockSceneNode();
	virtual void					UnlockSceneNode();
	virtual MutexLockerPtr			GetSceneNodeLocker();

private:
	IEngine*						engine;

	IrrlichtDevice*					device;
	video::IVideoDriver*			driver;
	scene::ISceneManager*			smgr;
	gui::IGUIEnvironment*			guienv;
	gui::IGUIStaticText*			guiText;

	video::ITexture*				blockTexture;
	video::SMaterial				blockMaterial;

	scene::ICameraSceneNode*		camera;

	QMutex*							sceneNodeLock;
};

#endif // GRAPHICS_H
