#ifndef CHUNK_H
#define CHUNK_H

#include "typedefs.h"
#include "sevoscenenode.h"
#include <irrlicht/irrlicht.h>
#include <QVarLengthArray>


#define CHUNK_SIZE 32

class ChunkManager;
class QMutex;
class Block;

namespace irr {
namespace scene {

//class IChunkSceneNode::BlockMetaData;	// defined at the bottom of this file
class IChunkSceneNode : public SEvoSceneNode {
public:
								IChunkSceneNode(ISceneNode *parent, irr::scene::ISceneManager *smgr, s32 id, core::vector3di chunkPosition, ChunkManager* chunkMgr, IGraphics *graphics);
								~IChunkSceneNode();
	static void					myDeleter(IChunkSceneNode* chunk);
	void						Initialize();

	// ISceneNode
	virtual const bboxf			getTransformedBoundingBox() const;
	virtual void				render();
	void						renderFaces(const u8 &facesToRender);

	// IReferenceCounted
	virtual void				grab() const;
	virtual bool				drop() const;

	void						SetBlockType(const core::vector3di &pos, const u8 &type);
	Block*						GetBlock(const core::v3s &pos);
	const u8&					GetBlockTypeBitMask(const core::v3s &pos) const;
	const core::v3s				&GetChunkPosition() const;
	static void					StartRebuildJob(const ChunkPtr &chunk);
	static void					Rebuild(const ChunkPtr &chunk);
	static void					StartDestroyJob(ChunkPtr &chunk);
	void						Destroy();
	void						DestroyInstantly();
	void						ForgetNeighbour(const u8 &adjacentFace);
	static void					InformNeighbourSignal(const ChunkPtr &chunk, const u8 &adjacentFace);
	static void					InformNeighbourSlot(const ChunkPtr &chunk, const u8 &adjacentFaces);
	static void					RefreshNeighbourSignal(const ChunkPtr &chunk, const u8 &adjacentFace);
	void						RefreshNeighbourSlot(const u8 &adjacentFaces);

	static s32					chunksLoaded;
	static QMutex*				chunksLoadedLock;

private:
	struct						BlockMetaData;
	void						InitializeBlockArray();
	void						InitializeBlockMetaArray();
	void						DeleteBlockArray();
	void						DeleteBlockMetaArray();
	void						CreateMesh();
	void						CreateCube(const core::v3s &blockPos, const u8 &facesRequired, const u8 &blockType);	// blockType is the enum value, not the bitmask
	void						CreateIndices(const u32 verticesFinal[], const u8 &requiredFace, u16 &verticesUsedByBlock, const core::v3s &blockPos, const u8 &blockType);
	u16							GetVertexIndex(const u32 &requiredVertex, const u16 &verticesUsedByBlock, const core::v3s &blockPos, const u8 &blockType);
	u16							FindVertexIndex(const core::vector3df &vPos, const BlockMetaData *meta) const;
	const BlockMetaData*		GetBlockMeta(const core::v3s &pos) const;

	ChunkManager*				chunkMgr;
	core::v3s					chunkPosition;

	QMutex*						updateMutex;
	bool						blocksChanged;
	u8							neighboursInformed;		// true if neighbour knows about this chunk
	ChunkPtr					neighbourChunks[6];		// our neighbours

	QMutex*						rebuildLock;
	Block*						Blocks;
	BlockMetaData*				blockMeta;
	u8							singleBlockType;
	u16*						blockTypeCount;
	//IndicesArray				indicesCreationArray;
	VerticesArray				verticesCreationArray;
	IndicesArray				indicesCreationArrays[6];
	//IndicesArray				indicesArrays[6];
	u16							indicesCounts[6];
	static core::bboxf			nullBox;
	core::bboxf					transformedBoundingBox;
};


} // irr::scene
} // irr

Q_DECLARE_METATYPE(irr::scene::ChunkPtr)

#endif // CHUNK_H
