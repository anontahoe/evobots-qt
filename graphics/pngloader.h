#ifndef PNGLOADER_H
#define PNGLOADER_H
#include <irrlicht/irrlicht.h>

class PngLoader
{
public:
	PngLoader();
	~PngLoader();

	static bool LoadPng(const char* name, irr::u32 &outWidth, irr::u32 &outHeight, bool &outHasAlpha, irr::u8 **outData);
};

#endif // PNGLOADER_H
