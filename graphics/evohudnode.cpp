/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "evohudnode.h"
#include "engine.h"

namespace irr {

namespace scene {

EvoHudNode::EvoHudNode(ISceneNode *parent, ISceneManager *smgr, s32 id, v3f position, const v3f &size, const v2f &uvCoords, const v2f &uvSize, IGraphics *graphics)
	: SEvoSceneNode(parent, smgr, id, position, graphics)
{
	if(size.X == 0 && size.Y == 0) {
		return;
	}

	vector3df p1(position.X,		position.Y,			position.Z);
	vector3df p2(position.X,		position.Y+size.Y,	position.Z);
	vector3df p3(position.X+size.X,	position.Y+size.Y,	position.Z);
	vector3df p4(position.X+size.X,	position.Y,			position.Z);
	vector3df p5(position.X+size.X,	position.Y,			position.Z+size.Z);
	vector3df p6(position.X+size.X,	position.Y+size.Y,	position.Z+size.Z);
	vector3df p7(position.X,		position.Y+size.Y,	position.Z+size.Z);
	vector3df p8(position.X,		position.Y,			position.Z+size.Z);

	v2f uv[4] { v2f(uvCoords),
				v2f(uvCoords.X, uvCoords.Y + uvSize.Y),
				v2f(uvCoords.X + uvSize.X, uvCoords.Y + uvSize.Y),
				v2f(uvCoords.X + uvSize.X, uvCoords.Y)
	};

	vector3df n1;
	SColor vertexColor(255, 0, 0, 255);

	// Front
	this->verticesArray.push_back(S3DVertex(p1, n1, vertexColor, uv[0]));
	this->verticesArray.push_back(S3DVertex(p2, n1, vertexColor, uv[1]));
	this->verticesArray.push_back(S3DVertex(p3, n1, vertexColor, uv[2]));
	this->verticesArray.push_back(S3DVertex(p4, n1, vertexColor, uv[3]));

	// Front
	this->indicesArray.push_back(0);
	this->indicesArray.push_back(1);
	this->indicesArray.push_back(2);

	this->indicesArray.push_back(2);
	this->indicesArray.push_back(3);
	this->indicesArray.push_back(0);
}

EvoHudNode::~EvoHudNode()
{

}

} // scene
} // irr

