/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "sevotexture.h"
#include "pngloader.h"
#include "logger.h"
#include <QReadWriteLock>
#include <QWriteLocker>

namespace irr {
using namespace core;
namespace video {


SEvoTexture::SEvoTexture(const irr::io::path &path)
	: ITexture(path),
	  hasAlphaChannel(false),
	  size(dim2u()),
	  originalSize(dim2u()),
	  textureData(nullptr),
	  textureDataLock(new QReadWriteLock())
{
	QWriteLocker locker(this->textureDataLock);

	bool success = PngLoader::LoadPng(
				path.c_str(),
				this->originalSize.Width,
				this->originalSize.Height,
				this->hasAlphaChannel,
				&this->textureData);
	this->size = originalSize;
	if(!success) {
		this->textureData = nullptr;
		Logger::Log("Failed to load blocktexture " + std::string(path.c_str()));
		return;
	}
	Logger::Log("Loaded blocktexture width ");
	Logger::Log(this->size.Width); Logger::Log(this->size.Height); Logger::Log(this->hasAlphaChannel);
}

SEvoTexture::~SEvoTexture() {
	delete this->textureDataLock;
	this->textureDataLock = nullptr;

	delete this->textureData;
	this->textureData = nullptr;
}

void* SEvoTexture::lock(E_TEXTURE_LOCK_MODE mode, u32 mipmapLevel) {
	mipmapLevel = mipmapLevel;

	if(mode == ETLM_READ_ONLY)
		this->textureDataLock->lockForRead();
	else
		this->textureDataLock->lockForWrite();

	return this->textureData;
}

void SEvoTexture::unlock() {
	this->textureDataLock->unlock();
}

const core::dimension2d<u32>& SEvoTexture::getOriginalSize() const {
	return this->originalSize;
}

const core::dimension2d<u32>& SEvoTexture::getSize() const {
	return this->size;
}

E_DRIVER_TYPE SEvoTexture::getDriverType() const {
	return EDT_OPENGL;
}

ECOLOR_FORMAT SEvoTexture::getColorFormat() const {
	return ECF_A8R8G8B8;
}

u32 SEvoTexture::getPitch() const {
	return 0;
}

void SEvoTexture::regenerateMipMapLevels(void* mipmapData) {
	mipmapData = mipmapData;
}

bool SEvoTexture::hasAlpha() {
	return true;
	return this->hasAlphaChannel;
}

}	// video
}	// irr
