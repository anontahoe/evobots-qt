/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "actor.h"
#include "chunktable.h"
#include "chunkhelper.h"

namespace irr {
namespace scene {

Actor::Actor(ISceneNode *parent, ISceneManager *smgr, s32 id, v3f position, IGraphics *graphics)
	: SEvoSceneNode(parent, smgr, id, position, graphics),
	  forward(v3f(0,0,-1))
{

}

Actor::~Actor()
{

}

void Actor::OnAnimate(u32 timeMs) {
	SEvoSceneNode::OnAnimate(timeMs);
	this->forward = v3f(0,0,-1);
	this->AbsoluteTransformation.rotateVect(this->forward);
}

const v3f& Actor::getForward() const {
	return this->forward;
}

void Actor::SelectBlock(const IChunkTableSceneNode* chunkTable) {
	ChunkHelper::raycast(this->RelativeTranslation, this->forward, 8, chunkTable);
}


} // scene
} // irr



