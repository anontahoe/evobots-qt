#ifndef HUDSCENENODE_H
#define HUDSCENENODE_H

#include "sevoscenenode.h"
#include "typedefs.h"

class IEngine;

namespace irr {
using namespace core;
namespace scene {

class EvoHudNode : public SEvoSceneNode
{
public:
	EvoHudNode(ISceneNode *parent, ISceneManager *smgr, s32 id, v3f position, const v3f &size, const v2f &uvCoords, const v2f &uvSize, IGraphics *graphics);
	~EvoHudNode();

private:

};

} // scene
} // irr

#endif // HUDSCENENODE_H
