/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "block.h"

using namespace irr;



BlockData::BlockData() {
	for(u8 i = 0; i < Block::BlockTypeCount; i++) {
		BlockData::blockTypeBitmasks[i] = 1 << i;
		BlockData::blockTypes[i] = i;
	}
}

u8 BD::blockTypeBitmasks[Block::BlockTypeCount];
u8 BD::blockTypes[Block::BlockTypeCount];


Block::Block(BlockType blockType)
	: blockType(blockType){

}

Block::~Block() {
}

const irr::u8 &Block::GetBlockType() const {
	return this->blockType;
}

const irr::u8 &Block::GetBlockTypeBitMask() const {
	return BD::blockTypeBitmasks[this->blockType];
}

void Block::SetBlockType(const irr::u8 &type) {
	this->blockType = type;
}
