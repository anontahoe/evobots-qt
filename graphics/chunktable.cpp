/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "chunktable.h"
#include "typedefs.h"
#include "iengine.h"
#include "igraphics.h"
#include "graphicsglew.h"
#include "sevomaterial.h"
#include "sevocameranode.h"
#include "chunkmanager.h"
#include "chunk.h"
#include "chunkconstructiondata.h"
#include "utils.h"
#include "logger.h"
#include <iterator>
#include <QReadWriteLock>

using namespace core;
using namespace video;
using namespace scene;

s32 IChunkTableSceneNode::chunkTableChunkCount = 0;
std::map<size_t,size_t>	IChunkTableSceneNode::chunksPerHashPos;


IChunkTableSceneNode::IChunkTableSceneNode(ISceneNode *parent, ISceneManager* smgr, s32 id, v3f position, IGraphics* graphics)
	: SEvoSceneNode(parent, smgr, id, position, graphics),
	  totalAdded(0),
	  totalReplaced(0),
	  totalRemoved(0),
	  renderTimeStart(0),
	  chunksToSetBuffer(ChunkPosChunkMap()),
	  chunksToSet(ChunkPosChunkMap()),
	  chunksToCleanBuffer(ChunkPtrSet()),
	  chunksToClean(ChunkPtrSet()),
	  chunksToPrepareBuffer(ChunkPtrSet()),
	  chunksToPrepare(ChunkPtrSet()),
	  graphics(dynamic_cast<GraphicsGlew*>(graphics)),
	  camera(this->graphics->GetEvoCamera()),
	  videoDriver(this->graphics),
	  chunksToSetSwitchLock(new QMutex()),
	  chunksToCleanSwitchLock(new QMutex()),
	  chunksToPrepareSwitchLock(new QMutex()),
	  chunkListLock(new QReadWriteLock()),
	  Box(bboxf()),
	  occlusionQueryTime(0),
	  retrieveOcclusionQueries(false)
{
	for (size_t i = 0; i < 255; i++) {
		IChunkTableSceneNode::chunksPerHashPos[i] = 0;
	}
	IChunkTableSceneNode::chunksPerHashPos[0] = WORLD_SIZE*WORLD_SIZE*WORLD_SIZE;

	this->chunkTable = new ChunkPtrList[WORLD_SIZE * WORLD_SIZE * WORLD_SIZE];
	for(u32 i = 0; i < WORLD_SIZE; i++) {
		this->chunkTable[i] = ChunkPtrList();
	}
}

IChunkTableSceneNode::~IChunkTableSceneNode() {
	for(const ChunkPosChunkPair& chunkPosChunk : this->chunksToSetBuffer) {
		chunkPosChunk.second->DestroyInstantly();
	}
	this->chunksToSetBuffer.clear();

	for(const ChunkPtr& chunk : this->chunksToCleanBuffer) {
		chunk->ClearRenderingData();
	}
	this->chunksToCleanBuffer.clear();

	this->chunksToPrepareBuffer.clear();


	delete this->chunkListLock;
	this->chunkListLock = nullptr;

	delete this->chunksToPrepareSwitchLock;
	this->chunksToPrepareSwitchLock = nullptr;

	delete this->chunksToCleanSwitchLock;
	this->chunksToCleanSwitchLock = nullptr;

	delete this->chunksToSetSwitchLock;
	this->chunksToSetSwitchLock = nullptr;

	for(s32 i = 0; i < WORLD_SIZE * WORLD_SIZE * WORLD_SIZE; i++) {
		for(ChunkPtr &itty : this->chunkTable[i]) {
			itty->DestroyInstantly();
			totalRemoved++;
			this->chunkTableChunkCount--;
		}
		//chunkTable[x][y][z].clear();
	}
	delete[] chunkTable;
	this->chunksPerHashPos.clear();
}

void IChunkTableSceneNode::OnAnimate(u32 timeMs) {
	this->chunksToSetSwitchLock->lock();
	this->chunksToSet.swap(this->chunksToSetBuffer);
	this->chunksToSetSwitchLock->unlock();

	this->chunkListLock->lockForWrite();
	for(const ChunkPosChunkPair& chunkPosChunk : this->chunksToSet) {
		// TODO: clean up this messy loop
		const v3s hashPos(ChunkPosToHashPos(chunkPosChunk.first));
		ChunkPtrList &chunkList(this->chunkTable[hashPos.X + WORLD_SIZE * hashPos.Y + WORLD_SIZE * WORLD_SIZE * hashPos.Z]);
		size_t oldLen(chunkList.size());
		bool foundChunk(false);
		for(ChunkPtrList::iterator chunkIter = chunkList.begin(); chunkIter != chunkList.end(); chunkIter++) {
			if((*chunkIter)->GetChunkPosition() != chunkPosChunk.first) {
				continue;
			}
			foundChunk = true;
			totalRemoved++;
			this->chunkTableChunkCount--;

			(*chunkIter)->ClearRenderingData();
			IChunkSceneNode::StartDestroyJob(*chunkIter);

			if(chunkPosChunk.second != nullptr) {
				totalReplaced++;
				this->chunkTableChunkCount++;
				(*chunkIter) = chunkPosChunk.second;
				IChunkSceneNode::StartRebuildJob(chunkPosChunk.second);
			} else {
				chunkIter = chunkList.erase(chunkIter);
				this->chunksPerHashPos[oldLen]--;	// statistics
				this->chunksPerHashPos[chunkList.size()]++;
			}
		}

		if(!foundChunk && chunkPosChunk.second != nullptr) {
			this->totalAdded++;
			this->chunkTableChunkCount++;
			chunkList.push_back(chunkPosChunk.second);
			IChunkSceneNode::StartRebuildJob(chunkPosChunk.second);
			this->chunksPerHashPos[oldLen]--;
			this->chunksPerHashPos[chunkList.size()]++;
		}
	}
	this->chunkListLock->unlock();
	this->chunksToSet.clear();

	this->chunksToCleanSwitchLock->lock();
	this->chunksToClean.swap(this->chunksToCleanBuffer);
	this->chunksToCleanSwitchLock->unlock();
	for(const ChunkPtr& chunk : this->chunksToClean) {
		chunk->ClearRenderingData();
	}
	this->chunksToClean.clear();

	this->chunksToPrepareSwitchLock->lock();
	this->chunksToPrepare.swap(this->chunksToPrepareBuffer);
	this->chunksToPrepareSwitchLock->unlock();
	for(const ChunkPtr& chunk : this->chunksToPrepare) {
		chunk->PrepareForRendering();
	}
	this->chunksToPrepare.clear();

	ISceneNode::OnAnimate(timeMs);
}

void IChunkTableSceneNode::OnRegisterSceneNode() {
	if(IsVisible)
		this->SceneManager->registerNodeForRendering(this);

	ISceneNode::OnRegisterSceneNode();
}

void IChunkTableSceneNode::render() {
	this->renderTimeStart = utils::Timer.GetNow();

	//this->videoDriver = this->SceneManager->getVideoDriver();
	videoDriver->setMaterial(getEvoMaterial());
	videoDriver->setFog(SColor(0,88,128,190), EFT_FOG_LINEAR, WORLD_RENDER_SIZE * CHUNK_SIZE / 2 - 2 * CHUNK_SIZE, WORLD_RENDER_SIZE * CHUNK_SIZE / 2 - CHUNK_SIZE , 0.01f, true, false);

	bool doOcclusionQueries = false;
	u32	timeNow;
	if(this->retrieveOcclusionQueries) {
			videoDriver->updateAllOcclusionQueries(true);
	} else if((timeNow = utils::Timer.lastTime) - this->occlusionQueryTime > 500) {
		this->occlusionQueryTime = timeNow;
		doOcclusionQueries = true;
	}

	const bboxf camBox(this->camera->getViewFrustum()->getBoundingBox().MinEdge	+ this->camera->getPosition(),
					   this->camera->getViewFrustum()->getBoundingBox().MaxEdge + this->camera->getPosition());
	const v3s	camHashPos(ChunkPosToHashPos(PosToChunkCoord(this->camera->getPosition())));
	v3s			hashPos(camHashPos);

	// calculate the cos(cameraAxisAngle) for x/y/z axes (dotProduct)
	// by default the camera faces in -z direction because ogl devs are retarded or maybe another reason
	v3f camDirection(this->camera->getForward());
	const f32 cameraAngleCos = cos(DEGTORAD64 * (90+45));
	u8 camFacing {};
	// check which axes lie inside the view frustum
	if(camDirection.Z < -cameraAngleCos) {	// -z
		camFacing |= 1 << CD::FACE_BACK;
	}
	if(camDirection.Z > cameraAngleCos) {	// +z
		camFacing |= 1 << CD::FACE_FRONT;
	}
	if(camDirection.Y > cameraAngleCos) {	// +y
		camFacing |= 1 <<CD::FACE_TOP_SPECIAL;
	}
	if(camDirection.Y < -cameraAngleCos) {	// -y
		camFacing |= 1 << CD::FACE_BOTTOM_SPECIAL;
	}
	if(camDirection.X < -cameraAngleCos) {	// -x
		camFacing |= 1 << CD::FACE_LEFT;
	}
	if(camDirection.X > cameraAngleCos) {	// +x
		camFacing |= 1 << CD::FACE_RIGHT;
	}

	this->camera->SelectBlock(this);

	// render the chunk at camera position
	//RenderChunksAt(hashPos, camBox, doOcclusionQueries);
	// render the world from camera position outwards
	v3s toRender(v3s(WORLD_RENDER_SIZE));
	RenderChunksXYZ(camHashPos, hashPos, camFacing, camBox, doOcclusionQueries);

	if(this->retrieveOcclusionQueries) {
		videoDriver->removeAllOcclusionQueries();
		this->retrieveOcclusionQueries = false;
	}
	else if(doOcclusionQueries) {
		videoDriver->runAllOcclusionQueries(true);
		videoDriver->updateAllOcclusionQueries(false);
		this->retrieveOcclusionQueries = true;
	}

	double renderTimeStop = utils::Timer.GetNow();
	this->renderTime = renderTimeStop - this->renderTimeStart;
	this->renderTimeStart = renderTimeStop;
}

void IChunkTableSceneNode::RenderChunksXYZ(const v3s &hashPosStart, v3s &hashPos, const u8& axisToRender, const bboxf& camBox, const bool& doOcclusionQueries) {
	u8 facesToRender((u8)-1);

	// always render the row where the camera is located
	RenderChunksYZ(hashPosStart, hashPos, facesToRender, axisToRender, camBox, doOcclusionQueries);

	if(axisToRender & (1 << CD::FACE_RIGHT)) {	// +x
		facesToRender ^= (1 << CD::FACE_RIGHT);
		for(s32 x = 1; x < WORLD_RENDER_SIZE / 2; x++) {
			hashPos.X++;
			if(hashPos.X == WORLD_RENDER_SIZE) {
				hashPos.X -= WORLD_RENDER_SIZE;
			}
			RenderChunksYZ(hashPosStart, hashPos, facesToRender, axisToRender, camBox, doOcclusionQueries);
		}
		hashPos.X = hashPosStart.X;
		facesToRender |= (1 << CD::FACE_RIGHT);
	}
	if(axisToRender & (1 << CD::FACE_LEFT)) {	// -x
		facesToRender ^= (1 << CD::FACE_LEFT);
		for(s32 x = 0; x < WORLD_RENDER_SIZE / 2; x++) {
			hashPos.X--;
			if(hashPos.X < 0) {
				hashPos.X += WORLD_RENDER_SIZE;
			}
			RenderChunksYZ(hashPosStart, hashPos, facesToRender, axisToRender, camBox, doOcclusionQueries);
		}
	}
}

void IChunkTableSceneNode::RenderChunksYZ(const v3s &hashPosStart, v3s& hashPos, u8 &facesToRender, const u8& axisToRender, const bboxf& camBox, const bool& doOcclusionQueries) {
	// always render the row where the camera is located
	RenderChunksZ(hashPosStart, hashPos, facesToRender, axisToRender, camBox, doOcclusionQueries);

	if(axisToRender & (1 <<CD::FACE_TOP_SPECIAL)) {	// +y
		facesToRender ^= (1 << CD::FACE_TOP_SPECIAL);
		for(s32 y = 1; y < WORLD_RENDER_SIZE / 2; y++) {
			hashPos.Y++;
			if(hashPos.Y == WORLD_RENDER_SIZE) {
				hashPos.Y -= WORLD_RENDER_SIZE;
			}
			RenderChunksZ(hashPosStart, hashPos, facesToRender, axisToRender, camBox, doOcclusionQueries);
		}
		hashPos.Y = hashPosStart.Y;
		facesToRender |= (1 << CD::FACE_TOP_SPECIAL);
	}
	if(axisToRender & (1 << CD::FACE_BOTTOM_SPECIAL)) {	// -y
		facesToRender ^= (1 << CD::FACE_BOTTOM_SPECIAL);
		for(s32 y = 0; y < WORLD_RENDER_SIZE / 2; y++) {
			hashPos.Y--;
			if(hashPos.Y < 0) {
				hashPos.Y += WORLD_RENDER_SIZE;
			}
			RenderChunksZ(hashPosStart, hashPos, facesToRender, axisToRender, camBox, doOcclusionQueries);
		}
		hashPos.Y = hashPosStart.Y;
		facesToRender |= (1 << CD::FACE_BOTTOM_SPECIAL);
	}
}

void IChunkTableSceneNode::RenderChunksZ(const v3s &hashPosStart, v3s& hashPos, u8 &facesToRender, const u8 &axisToRender, const bboxf& camBox, const bool& doOcclusionQueries) {
	// always render the row where the camera is located
	RenderChunksAt(hashPos, facesToRender, camBox, doOcclusionQueries);

	if(axisToRender & (1 << CD::FACE_FRONT)) {	// +z
		facesToRender ^= (1 << CD::FACE_FRONT);
		for(s32 z = 1; z < WORLD_RENDER_SIZE / 2; z++) {
			hashPos.Z++;
			if(hashPos.Z == WORLD_RENDER_SIZE) {
				hashPos.Z -= WORLD_RENDER_SIZE;
			}
			RenderChunksAt(hashPos, facesToRender, camBox, doOcclusionQueries);
		}
		hashPos.Z = hashPosStart.Z;
		facesToRender |= (1 << CD::FACE_FRONT);
	}
	if(axisToRender & (1 << CD::FACE_BACK)) {	// -z
		facesToRender ^= (1 << CD::FACE_BACK);
		for(s32 z = 0; z < WORLD_RENDER_SIZE / 2; z++) {
			hashPos.Z--;
			if(hashPos.Z < 0) {
				hashPos.Z += WORLD_RENDER_SIZE;
			}
			RenderChunksAt(hashPos, facesToRender, camBox, doOcclusionQueries);
		}
		hashPos.Z = hashPosStart.Z;
		facesToRender |= (1 << CD::FACE_BACK);
	}
}

void IChunkTableSceneNode::RenderChunksAt(const v3s& hashPos, const u8& facesToRender, const bboxf &camBox, const bool &doOcclusionQueries) {
	for(ChunkPtr itty : this->chunkTable[hashPos.X + WORLD_SIZE * hashPos.Y + WORLD_SIZE * WORLD_SIZE * hashPos.Z]) {
		if(!itty->sceneNodeMeta.valid)
			continue;

		// not quite accurate frustum culling
		// TODO: getAbsoluteBoundingBox (axis aligned world space)
		if(!camBox.intersectsWithBox(itty->getTransformedBoundingBox()))
			continue;

		if(false && doOcclusionQueries) {
			this->videoDriver->addOcclusionQuery(itty.get());
			continue;
		} else if(this->retrieveOcclusionQueries) {
			u32 visible = this->videoDriver->getOcclusionQueryResult(itty.get());
			if(visible == (u32)-1) {
				continue;
			} else if(visible > 0) {
				itty->setVisible(true);
			} else {
				itty->setVisible(false);
			}
			this->videoDriver->removeOcclusionQuery(itty.get());
		}

		if(itty->isVisible()) {
		//if(!doOcclusionQueries && itty->isVisible()) {
			//itty->render();
			itty->renderFaces(facesToRender);
		}
	}
}

void IChunkTableSceneNode::setVisible(bool isVisible) {
	ISceneNode::setVisible(isVisible);
}

const bboxf& IChunkTableSceneNode::getBoundingBox() const {
	return this->Box;
}

ChunkPtr IChunkTableSceneNode::GetChunkAt(const core::v3s &chunkPos) const {
	// convert to hash params
	vector3di hashPos = ChunkPosToHashPos(chunkPos);

	//QReadLocker locker(this->chunkListLocks[hashPos.X][hashPos.Y][hashPos.Z]);
	const QReadLocker locker(this->chunkListLock);

	ChunkPtrList &chunkList = this->chunkTable[hashPos.X + WORLD_SIZE * hashPos.Y + WORLD_SIZE * WORLD_SIZE * hashPos.Z];
	for(const ChunkPtr &chunkPtr : chunkList) {
		if(chunkPtr->GetChunkPosition() == chunkPos)
			return chunkPtr;
	}

	return ChunkPtr();
}

void IChunkTableSceneNode::SetChunkAt(const core::v3s &chunkPos, ChunkPtr chunkToSet) {
	const QMutexLocker locker(this->chunksToSetSwitchLock);

	const ChunkPosChunkMap::iterator oldChunkToSet(this->chunksToSetBuffer.find(chunkPos));
	if(oldChunkToSet == this->chunksToSetBuffer.end()) {
		this->chunksToSetBuffer[chunkPos] = chunkToSet;
		return;
	}

	if((*oldChunkToSet).second != nullptr) {
		//(*oldChunkToSet).second->StartDestroyJob((*oldChunkToSet).second);
		(*oldChunkToSet).second->DestroyInstantly();
	}
	(*oldChunkToSet).second = chunkToSet;
}

void IChunkTableSceneNode::DropChunk(const v3s &chunkPos) {
	SetChunkAt(chunkPos, nullptr);
}

void IChunkTableSceneNode::PrepareChunkForRendering(const ChunkPtr &chunk) {
	this->chunksToPrepareSwitchLock->lock();
	this->chunksToPrepareBuffer.insert(chunk);
	this->chunksToPrepareSwitchLock->unlock();
}

v3s IChunkTableSceneNode::PosToChunkCoord(const v3f &pos) {
	return v3s(pos.X < 0 ? float((pos.X / CHUNK_SIZE) - 0.999f) : float(pos.X / CHUNK_SIZE),
			   pos.Y < 0 ? float((pos.Y / CHUNK_SIZE) - 0.999f) : float(pos.Y / CHUNK_SIZE),
			   pos.Z < 0 ? float((pos.Z / CHUNK_SIZE) - 0.999f) : float(pos.Z / CHUNK_SIZE));
}

v3s IChunkTableSceneNode::ChunkPosToHashPos(const v3s &chunkPos) {
	//vector3du hashPos = Utils::MurmurHash(chunkPos);

	// -x gets mapped to WORLD_SIZE - x
	return v3s(chunkPos.X >= 0 ? chunkPos.X % WORLD_SIZE : (WORLD_SIZE + (chunkPos.X % WORLD_SIZE)) % WORLD_SIZE,
			   chunkPos.Y >= 0 ? chunkPos.Y % WORLD_SIZE : (WORLD_SIZE + (chunkPos.Y % WORLD_SIZE)) % WORLD_SIZE,
			   chunkPos.Z >= 0 ? chunkPos.Z % WORLD_SIZE : (WORLD_SIZE + (chunkPos.Z % WORLD_SIZE)) % WORLD_SIZE);
}

















