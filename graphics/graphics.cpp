/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "graphics.h"
#include "iengine.h"
#include "chunk.h"
#include "threadable.h"
#include "threadmanager.h"
#include "chunktable.h"
#include "sevomaterial.h"
#include "utils.h"

using namespace irr;
using namespace core;
using namespace video;
using namespace scene;
using namespace io;
using namespace gui;

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif

Graphics::Graphics(IEngine *engine) {
	this->engine = engine;

	this->device = nullptr;
	this->driver = nullptr;
	this->smgr = nullptr;
	this->guienv = nullptr;

	this->camera = nullptr;

	this->sceneNodeLock = new QMutex();	
}

Graphics::~Graphics()
{

	this->smgr->clear();
	this->device->closeDevice();
	this->device->run();
	this->device->drop();

	delete this->sceneNodeLock;
}

void Graphics::Initialize() {
}

GLFWwindow *Graphics::OpenScreen() {
	SIrrlichtCreationParameters params = SIrrlichtCreationParameters();
	params.Bits = 32;
	params.DeviceType = EIDT_X11;
	params.Doublebuffer = true;
	//params.DriverMultithreaded = false;
	params.DriverType = EDT_OPENGL;
	params.Fullscreen = true;
	params.Stencilbuffer = true;
	//params.Stereobuffer = false;
	params.Vsync = false;
	params.WithAlphaChannel = true;
	params.ZBufferBits = 24;
	params.WindowSize = dimension2du(1920, 1080);
	//IrrlichtDevice *device = createDevice(video::EDT_OPENGL, dimension2du(1920, 1080), 32, true, true, false);
	this->device = createDeviceEx(params);

	if(!this->device)
		return nullptr;

	this->device->setWindowCaption(L"Evobots");

	this->driver = device->getVideoDriver();
	this->smgr = device->getSceneManager();
	this->guienv = device->getGUIEnvironment();

	// load texture. this should be done elsewhere but we need this->driver and thus this->device
	this->driver->setTextureCreationFlag(video::ETCF_CREATE_MIP_MAPS, true);
	this->blockTexture = this->driver->getTexture("../media/blocktextures.png");

	this->blockMaterial = SMaterial();
	this->blockMaterial.Wireframe = false;
	this->blockMaterial.Lighting = false;
	this->blockMaterial.MaterialType = EMT_SOLID;
	this->blockMaterial.setTexture(0, this->blockTexture);
	this->blockMaterial.setFlag(EMF_LIGHTING, false);
	this->blockMaterial.setFlag(EMF_FOG_ENABLE, true);

	return nullptr;
}

void Graphics::PlaceStuff() {
	QMutexLocker(this->sceneNodeLock);

	this->camera = smgr->addCameraSceneNodeFPS(0, 100.0f, 0.01f);
	this->camera->setNearValue(0.001f);
	this->camera->setPosition(vector3df(0,5,-5));
	this->camera->setTarget(vector3df(0,0,0));
	this->camera->setFarValue(600);

	stringw statusText = L"Hello world!";
	this->guiText = guienv->addStaticText(statusText.c_str(), recti(10, 10, 260, 252), true);

	//ISceneNode *skyDome = smgr->addSkyDomeSceneNode(this->driver->getTexture("/tmp/fromgenuser/skydome.bmp"), 40, 10, 1.0, 1.2, 500);
	//skyDome->getMaterial(0).setFlag(EMF_LIGHTING, false);
	ISceneNode *skyBox = smgr->addSkyBoxSceneNode(driver->getTexture("../media/1-up-blue.png"),
												  driver->getTexture("../media/2-down.png"),
												  driver->getTexture("../media/3-left-blue.png"),
												  driver->getTexture("../media/4-right-blue.png"),
												  driver->getTexture("../media/5-front-blue.png"),
												  driver->getTexture("../media/6-back-blue.png"));

	if(skyBox == nullptr) {
		// lol, ignore
	}

	driver->setTextureCreationFlag(video::ETCF_CREATE_MIP_MAPS, true);
/*
	IAnimatedMesh *sydneyMesh = smgr->getMesh("/usr/share/doc/irrlicht-1.8-r2/media/sydney.md2");
	if(!sydneyMesh)
		return;

	IAnimatedMeshSceneNode *sydneyNode = smgr->addAnimatedMeshSceneNode(sydneyMesh);
	if(!sydneyNode)
		return;

	sydneyNode->setPosition(vector3df(0,0,20));
	sydneyNode->setMaterialFlag(EMF_LIGHTING, false);
	sydneyNode->setMD2Animation(scene::EMAT_STAND);
	sydneyNode->setMaterialTexture(0, driver->getTexture("/usr/share/doc/irrlicht-1.8-r2/media/sydney.bmp"));

	CSampleSceneNode *myNode = new CSampleSceneNode(smgr->getRootSceneNode(), smgr, 666, vector3di(0,10,0));
	myNode->setMaterialTexture(0, this->blockTexture);
	ISceneNodeAnimator *anim = smgr->createRotationAnimator(vector3df(0.8f, 0, 0.8f));
	if(!anim)
		return;

	myNode->addAnimator(anim);
	anim->drop();
	myNode->drop();
	anim = 0;

	CSampleSceneNode *myNode2 = new CSampleSceneNode(smgr->getRootSceneNode(), smgr, -1, vector3di(10,10,0));
	myNode2->setMaterialTexture(0, this->blockTexture);
	myNode2->drop();

	(new CSampleSceneNode(smgr->getRootSceneNode(), smgr, -1, vector3di(12,11,0)))->drop();*/
}

scene::ISceneNode* Graphics::CreateEvoTestNode(ISceneNode *parent, const v3f &pos) {
	return nullptr;
}

scene::EvoHudNode* Graphics::CreateHudSceneNode(ISceneNode *parent) {
	return nullptr;
}

video::ITexture* Graphics::getTexture(const io::path& file) {
	return this->driver->getTexture(file);
}

void Graphics::prepareNodeForRendering(SEvoSceneNode *node) {
	node = node; // placeholder
}

void Graphics::unregisterMeshForRendering(SEvoSceneNode *node) {
	node = node; // placeholder
}

u32 Graphics::getShader(const StringPair &files) {
	return 0;
}

ISceneNode* Graphics::GetRootSceneNode() {
	return nullptr;
}

void Graphics::SetRootSceneNode(SEvoSceneNode *sceneNode) {
	sceneNode = sceneNode;
}

void Graphics::setMaterial(const SEvoMaterial *material) {
	this->driver->setMaterial(*material);
}

void Graphics::Render() {
	this->sceneNodeLock->lock();	

	driver->beginScene(true, true, SColor(255, 100, 101, 240));
	smgr->drawAll();
	guienv->drawAll();
	driver->endScene();

	this->sceneNodeLock->unlock();

	s32 fps = driver->getFPS();
	v3f cameraTarget = this->camera->getTarget();
	stringw statusText = L"Hello world!\n";
	statusText += stringw(L"FPS: ") + stringw(fps) + L"\n";
	statusText += stringw(L"Chunks: ") + stringw(IChunkSceneNode::chunksLoaded) + L"\n";
	statusText += stringw(L"Threadables: ") + stringw(Threadable::threadableCount) + L"\n";
	statusText += stringw(L"Threaded jobs: ") + stringw(this->engine->GetThreadManager()->workerCount) + L"\n";
	statusText += stringw(L"Chunktable entries: ") + stringw(IChunkTableSceneNode::chunkTableChunkCount) + L"\n";
	statusText += stringw(L"camtarget: (") + stringw(cameraTarget.X) + L", " + stringw(cameraTarget.Y) + L", " + stringw(cameraTarget.Z) + L"\n";
	statusText += stringw(L"Position: ") + stringw(this->camera->getPosition().X) + ", " + stringw(this->camera->getPosition().Y) + ", " + stringw(this->camera->getPosition().Z) + L"\n";
	for(size_t i = 0; i < 5; i++) {
		statusText += stringw(L"chunktable entries: ") + "\n " + stringw(i) + L":    " + stringw(IChunkTableSceneNode::chunksPerHashPos[i]) + L"\n";
	}
	this->guiText->setText(statusText.c_str());
}

void Graphics::Pause(const bool &pause) {
	this->camera->setInputReceiverEnabled(!pause);
	if(pause) {
		this->device->getTimer()->stop();
	} else {
		this->device->getTimer()->setTime(this->device->getTimer()->getTime() + 1);
		this->device->getTimer()->start();
	}
}

void Graphics::GrabCursor(const bool &grab) {
}

ICameraSceneNode *Graphics::GetCamera() const {
	return this->camera;
}

SEvoCameraNode* Graphics::GetEvoCamera() const {
	return nullptr;
}

v3f Graphics::GetCameraPos() const {
	return this->camera->getAbsolutePosition();
}

void Graphics::SetCameraPos(const v3f &position) {
	this->camera->setPosition(position);
}

IrrlichtDevice* Graphics::GetDevice() {
	return this->device;
}

ISceneManager* Graphics::GetSceneManager() {
	return this->smgr;
}

IGUIEnvironment* Graphics::GetGuiEnvironment() {
	return this->guienv;
}

ITexture* Graphics::GetBlockTexture() {
	return this->blockTexture;
}

SMaterial &Graphics::GetBlockMaterial() {
	return this->blockMaterial;
}

SEvoMaterial* Graphics::GetEvoMaterial() {
	return nullptr;
}

u32 Graphics::GetTime() {
	return this->device->getTimer()->getTime();
}

void Graphics::LockSceneNode() {
	this->sceneNodeLock->lock();
}

void Graphics::UnlockSceneNode() {
	this->sceneNodeLock->unlock();
}

MutexLockerPtr Graphics::GetSceneNodeLocker() {
	return MutexLockerPtr(new QMutexLocker(this->sceneNodeLock));
}










