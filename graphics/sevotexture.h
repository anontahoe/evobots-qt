#ifndef ICHUNKTEXTURE_H
#define ICHUNKTEXTURE_H

#include "typedefs.h"

class QReadWriteLock;

namespace irr {
namespace video {


class SEvoTexture : public irr::video::ITexture
{
public:
	SEvoTexture(const irr::io::path& path);
	~SEvoTexture();

	virtual void*				lock(E_TEXTURE_LOCK_MODE mode=ETLM_READ_WRITE, u32 mipmapLevel=0);
	virtual void				unlock();
	virtual const core::dim2u&	getOriginalSize() const;
	virtual const core::dim2u&	getSize() const;
	virtual E_DRIVER_TYPE		getDriverType() const;
	virtual ECOLOR_FORMAT		getColorFormat() const;
	virtual u32					getPitch() const;
	virtual void				regenerateMipMapLevels(void* mipmapData=0);

	bool						hasAlpha();

private:
	bool						hasAlphaChannel;
	core::dim2u					size;
	core::dim2u					originalSize;

	u8*							textureData;
	QReadWriteLock*				textureDataLock;
};


}	// video
}	// irr

#endif // ICHUNKTEXTURE_H
