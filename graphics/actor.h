#ifndef ENTITY_H
#define ENTITY_H

#include "sevoscenenode.h"

class IChunkTableSceneNode;

namespace irr {
namespace scene {

class Actor : public SEvoSceneNode
{
public:
	Actor(ISceneNode *parent, ISceneManager *smgr, s32 id, v3f position, IGraphics *graphics);
	~Actor();

	// ISceneNode
	virtual void		OnAnimate(u32 timeMs);

	const core::v3f&	getForward() const;
	void				SelectBlock(const IChunkTableSceneNode* chunkTable);


private:
	core::v3f			forward;
};

} // scene
} // irr

#endif // ENTITY_H
