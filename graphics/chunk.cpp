/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "chunk.h"
#include "block.h"
#include "iengine.h"
#include "graphicsglew.h"
#include "chunkmanager.h"
#include "chunktable.h"
#include "chunkupdatemanager.h"
#include "chunkconstructiondata.h"
#include <QMutex>


namespace irr {
using namespace video;
using namespace core;
namespace scene {

struct IChunkSceneNode::BlockMetaData {
	BlockMetaData()
		: globalFaceCount(0),
		  localFaceCount(0),
		  globalVertexCount((u16)-1),
		  localVertexCount(0)
	{
	}

	// how many faces have been created before this block
	u32 globalFaceCount;
	// how many faces this block created (used to keep track of the Indices array)
	u32 localFaceCount;

	u16 globalVertexCount;
	u8 localVertexCount;
};

aabbox3df IChunkSceneNode::nullBox = aabbox3df(0,0,0,0,0,0);

s32 IChunkSceneNode::chunksLoaded(0);
QMutex* IChunkSceneNode::chunksLoadedLock(new QMutex);

IChunkSceneNode::IChunkSceneNode(ISceneNode *parent, ISceneManager *smgr, s32 id, vector3di position, ChunkManager *chunkMgr, IGraphics *graphics)
	: SEvoSceneNode(parent, smgr, id, v3f(position.X,position.Y,position.Z) * CHUNK_SIZE, graphics),
	  chunkMgr(chunkMgr),
	  chunkPosition(position),
	  updateMutex(new QMutex()),
	  blocksChanged(false),
	  neighboursInformed(0),
	  neighbourChunks{},
	  rebuildLock(new QMutex()),
	  blockMeta(nullptr),
	  singleBlockType(Block::BT_Air),
	  blockTypeCount(new u16[Block::BlockTypeCount]),
	  //indicesCreationArray(IndicesArray()),
	  verticesCreationArray(VerticesArray()),
	  indicesCreationArrays {},
	  //verticesCreationArrays {},
	  //indicesArrays {},
	  indicesCounts {},
	  transformedBoundingBox(bboxf())
{
	/*memset(this->neighboursInformed, 0, sizeof(bool) * CD::FACE_COUNT);
	for(u8 i = 0; i < CD::FACE_COUNT; i++) {
		this->neighbourChunks[i].reset();
		this->neighboursInformed[i] = false;
	}*/

	InitializeBlockArray();

	this->IsVisible = true;

	IChunkSceneNode::chunksLoadedLock->lock();
	IChunkSceneNode::chunksLoaded++;		// roughly keep track of chunk count for debugging purposes
	IChunkSceneNode::chunksLoadedLock->unlock();
}

IChunkSceneNode::~IChunkSceneNode() {
	this->verticesCreationArray.clear();
	//this->indicesCreationArray.clear();
	for(u8 i = 0; i < CD::FACE_COUNT; i++) {
		this->indicesCreationArrays[i].clear();
		//this->indicesArrays[i].clear();
	}

	for(u8 i = 0; i < CD::FACE_COUNT; i++) {
		this->neighbourChunks[i].reset();
	}

	DeleteBlockMetaArray();
	DeleteBlockArray();

	delete[] this->blockTypeCount;

	delete this->rebuildLock;
	this->rebuildLock = nullptr;

	delete this->updateMutex;
	this->updateMutex = nullptr;

	IChunkSceneNode::chunksLoadedLock->lock();
	IChunkSceneNode::chunksLoaded--;
	if(IChunkSceneNode::chunksLoaded == 5) {
		int a = 0;
	}
	IChunkSceneNode::chunksLoadedLock->unlock();
}

void IChunkSceneNode::myDeleter(IChunkSceneNode *chunk) {
	chunk->drop();
}

void IChunkSceneNode::Initialize() {
	OnAnimate(0);	// calculate absolute world transformation
	this->transformedBoundingBox = bboxf(this->RelativeTranslation, this->RelativeTranslation);
}

void IChunkSceneNode::InitializeBlockArray() {
	this->Blocks = new Block[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];
	memset(this->Blocks, 0, sizeof(Block) * CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE);

}

void IChunkSceneNode::InitializeBlockMetaArray() {
	if(this->blockMeta != nullptr)
		DeleteBlockMetaArray();

	this->blockMeta = new BlockMetaData[CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE];
	memset(this->blockMeta, 0, sizeof(BlockMetaData) * CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE);
}

void IChunkSceneNode::DeleteBlockArray() {
	if(this->Blocks == nullptr)
		return;
	delete[] this->Blocks;
	this->Blocks = nullptr;
}

void IChunkSceneNode::DeleteBlockMetaArray() {
	// Delete the blocks
	if(this->blockMeta == nullptr)
		return;
	delete[] this->blockMeta;
	this->blockMeta = nullptr;

}

const bboxf IChunkSceneNode::getTransformedBoundingBox() const {
	return this->transformedBoundingBox;
}

void IChunkSceneNode::render() {
	if(this->toBeDestroyed)
		return;

	this->renderMutex->lock();

	if(!this->sceneNodeMeta.valid) {
		this->renderMutex->unlock();
		return;
	}

	if(!this->verticesArray.count()) {
		this->renderMutex->unlock();
		return;
	}

	//video::IVideoDriver *driver = this->SceneManager->getVideoDriver();

	this->graphics->setMaterial(getEvoMaterial());
	this->graphics->setTransform(ETS_WORLD, this->AbsoluteTransformation);
	u16 offset = 0;
	for(u8 i = 0; i < CD::FACE_COUNT; i++) {
		if(this->indicesCounts[i] != 0) {
			this->graphics->renderSceneNodePart(this, this->indicesCounts[i], offset);
		}
		offset += this->indicesCounts[i];
	}

	this->renderMutex->unlock();
}

void IChunkSceneNode::renderFaces(const u8& facesToRender) {
	if(this->toBeDestroyed)
		return;

	this->renderMutex->lock();

	if(!this->sceneNodeMeta.valid) {
		this->renderMutex->unlock();
		return;
	}

	if(!this->verticesArray.count()) {
		this->renderMutex->unlock();
		return;
	}

	//video::IVideoDriver *driver = this->SceneManager->getVideoDriver();

	this->graphics->setMaterial(getEvoMaterial());
	this->graphics->setTransform(ETS_WORLD, this->AbsoluteTransformation);
	u16 offset = 0;
	for(u8 i = 0; i < CD::FACE_COUNT; i++) {
		if((facesToRender & (1 << i)) && this->indicesCounts[i]) {
			this->graphics->renderSceneNodePart(this, this->indicesCounts[i], offset);
		}
		offset += this->indicesCounts[i];
	}

	this->renderMutex->unlock();
}

void IChunkSceneNode::grab() const {
	IReferenceCounted::grab();
}

bool IChunkSceneNode::drop() const {
	return IReferenceCounted::drop();
}

const core::v3s &IChunkSceneNode::GetChunkPosition() const {
	return this->chunkPosition;
}

void IChunkSceneNode::StartDestroyJob(ChunkPtr& chunk) {
	if(chunk->toBeDestroyed) {
		return;
	}
	chunk->toBeDestroyed = true;
	chunk->blocksChanged = false;

	chunk->chunkMgr->chunkUpdateManager->DeleteChunk(chunk);
}

void IChunkSceneNode::Destroy() {
	this->updateMutex->lock();

	for(u8 i = 0; i < CD::FACE_COUNT; i++) {
		if(this->neighbourChunks[i] != nullptr) {
			IChunkSceneNode::RefreshNeighbourSignal(this->neighbourChunks[i], CD::oppositeFaces[i]);
			this->neighbourChunks[i].reset();
		}
	}

	this->updateMutex->unlock();
}

void IChunkSceneNode::DestroyInstantly() {
	this->updateMutex->lock();

	for(u8 i = 0; i < CD::FACE_COUNT; i++) {
		if(this->neighbourChunks[i] != nullptr) {
			this->neighbourChunks[i].reset();
		}
	}

	//ClearRenderingData();

	this->updateMutex->unlock();
}

void IChunkSceneNode::ForgetNeighbour(const u8 &adjacentFace) {
	this->updateMutex->lock();
	this->neighbourChunks[adjacentFace].reset();
	this->neighboursInformed &= u8(-1) ^ adjacentFace;
	this->updateMutex->unlock();
}

void IChunkSceneNode::StartRebuildJob(const ChunkPtr &chunk) {
	chunk->chunkMgr->chunkUpdateManager->RebuildChunk(chunk);
}

void IChunkSceneNode::InformNeighbourSignal(const ChunkPtr &chunk, const u8 &adjacentFace) {
	//chunk->neighboursInformed |= adjacentFaces;		// neighbour already knows about this chunk
	chunk->chunkMgr->chunkUpdateManager->InformChunkNeighbourCreated(chunk, 1 << adjacentFace);
}

void IChunkSceneNode::InformNeighbourSlot(const ChunkPtr &chunk, const u8 &adjacentFaces) {
	chunk->updateMutex->lock();
	chunk->neighboursInformed |= adjacentFaces;
	chunk->updateMutex->unlock();
	IChunkSceneNode::Rebuild(chunk);
	//chunk->chunkMgr->chunkUpdateManager->RebuildChunk(chunk);	// rebuild in a different thread
}

void IChunkSceneNode::RefreshNeighbourSignal(const ChunkPtr& chunk, const u8 &adjacentFace) {
	chunk->chunkMgr->chunkUpdateManager->RefreshChunkNeighbour(chunk, 1<<adjacentFace);
}

void IChunkSceneNode::RefreshNeighbourSlot(const u8 &adjacentFaces) {
	this->updateMutex->lock();
	for(u8 i = 0; i < CD::FACE_COUNT; i++) {
		if(!(adjacentFaces & 1<<i)) {
			continue;
		}
		this->neighbourChunks[i].reset();
	}
	this->neighboursInformed &= this->neighboursInformed ^ adjacentFaces;
	this->updateMutex->unlock();
}

void IChunkSceneNode::Rebuild(const ChunkPtr &chunk) {
	bool neighboursChanged(false);
	u8 neighboursAvailable(0);

	chunk->updateMutex->lock();
	if(chunk->toBeDestroyed) {
		chunk->updateMutex->unlock();
		return;
	}

	for(u8 adjacentFace = 0; adjacentFace < CD::FACE_COUNT; adjacentFace++) {
		if(chunk->neighbourChunks[adjacentFace] == nullptr) {
			ChunkPtr neighbour = chunk->chunkMgr->GetChunkAt(chunk->chunkPosition + CD::adjacentFaceDir[adjacentFace]);
			if(neighbour == nullptr) {
				continue;
			}
			chunk->neighbourChunks[adjacentFace] = neighbour;
			neighboursChanged = true;
		}
		if(!(chunk->neighboursInformed & (1 << adjacentFace))) {
			InformNeighbourSignal(chunk->neighbourChunks[adjacentFace], CD::oppositeFaces[adjacentFace]);
			chunk->neighboursInformed |= 1 << adjacentFace;
		}
		neighboursAvailable++;
	}

	if(neighboursChanged && neighboursAvailable >= 3) {
		chunk->updateMutex->unlock();
		chunk->CreateMesh();
		chunk->updateMutex->lock();
		chunk->chunkMgr->chunkTable->PrepareChunkForRendering(chunk);
	}
	chunk->updateMutex->unlock();
}

void IChunkSceneNode::CreateMesh() {
	QMutexLocker locker(this->rebuildLock);

	//this->renderMutex->lock();
	//this->indicesCreationArray.clear();
	//this->verticesCreationArray.clear();
	for(u8 i = 0; i < CD::FACE_COUNT; i++) {
		this->indicesCreationArrays[i].clear();
	}
	//this->renderMutex->unlock();

	InitializeBlockMetaArray();

	v3f bboxMin(CHUNK_SIZE);
	v3f bboxMax;
	//u32 previousGlobalFaceCount = 0;
	bool chunkEmpty(true);
	u8 facesRequired;			// those variables are used when creating the chunk's 3d mesh

	v3s blockPos;
	for(blockPos.Z = 0; blockPos.Z < CHUNK_SIZE; blockPos.Z++) {
		for(blockPos.Y = 0; blockPos.Y < CHUNK_SIZE; blockPos.Y++) {
			for(blockPos.X = 0; blockPos.X < CHUNK_SIZE; blockPos.X++) {
				if(!(this->Blocks[blockPos.X + CHUNK_SIZE * blockPos.Y + CHUNK_SIZE * CHUNK_SIZE * blockPos.Z].GetBlockTypeBitMask() & Block::BT_Opague))
					continue;

				facesRequired = 0;

				for(u8 adjacentFace = 0; adjacentFace < CD::FACE_COUNT; adjacentFace++) {
					if(!(GetBlockTypeBitMask(blockPos + CD::adjacentFaceDir[adjacentFace]) & Block::BT_Opague)) {		// && adjacentFace != CD::FACE_TOP && adjacentFace != CD::FACE_BOTTOM
						facesRequired |= 1 << adjacentFace;
						chunkEmpty = false;
					}
				}

				if(facesRequired == 0)
					continue;				

				// keeping track of the chunk's bounding box
				if(bboxMin.X > blockPos.X)
					bboxMin.X = blockPos.X;
				else if(bboxMax.X < blockPos.X)
					bboxMax.X = blockPos.X;
				if(bboxMin.Y > blockPos.Y)
					bboxMin.Y = blockPos.Y;
				else if(bboxMax.Y < blockPos.Y)
					bboxMax.Y = blockPos.Y;
				if(bboxMin.Z > blockPos.Z)
					bboxMin.Z = blockPos.Z;
				else if(bboxMax.Z < blockPos.Z)
					bboxMax.Z = blockPos.Z;

				// keeping track of number of the number of faces for each block. only needed to merge vertices between blocks so we might ditch it
				//this->blockMeta[blockPos.X][blockPos.Y][blockPos.Z].globalFaceCount = previousGlobalFaceCount;
				// create the visible faces for the current block
				CreateCube(blockPos, facesRequired, this->Blocks[blockPos.X  + CHUNK_SIZE * blockPos.Y +  CHUNK_SIZE * CHUNK_SIZE * blockPos.Z].GetBlockType());
				//previousGlobalFaceCount += this->blockMeta[blockPos.X][blockPos.Y][blockPos.Z].localFaceCount;
			}
		}
	}

	// let's not delete the metadata for now. I think we'll need it to modify the chunk.
	// nvm.
	DeleteBlockMetaArray();

	if(!chunkEmpty) {
		this->boundingBox = bboxf(bboxMin, bboxMax);
		this->transformedBoundingBox = bboxf(this->RelativeTranslation, this->RelativeTranslation + this->boundingBox.MaxEdge);
	} else {
		this->boundingBox =  aabbox3df();
		return;
	}

	this->renderMutex->lock();
	u16 totalIndices(0);
	for(u8 i = 0; i < CD::FACE_COUNT; i++) {
		this->indicesCounts[i] = this->indicesCreationArrays[i].size();
		totalIndices += this->indicesCounts[i];
	}
	this->verticesArray.resize(this->verticesCreationArray.size());
	this->indicesArray.resize(totalIndices);
	u16 offset(0);
	for(u8 i = 0; i < CD::FACE_COUNT; i++) {
		if(!this->indicesCounts[i] == 0) {
			memcpy(&this->indicesArray[offset], &this->indicesCreationArrays[i][0], sizeof(u16) * this->indicesCreationArrays[i].size());
			this->indicesCreationArrays[i].clear();
		}
		offset += this->indicesCounts[i];
	}
	memcpy(&this->verticesArray[0], &this->verticesCreationArray[0], sizeof(S3DVertex) * this->verticesCreationArray.size());
	this->verticesCreationArray.clear();
	this->renderMutex->unlock();
}

void IChunkSceneNode::CreateCube(const v3s &blockPos, const u8 &facesRequired, const u8 &blockType) {

	this->blockMeta[blockPos.X + CHUNK_SIZE * blockPos.Y + CHUNK_SIZE * CHUNK_SIZE * blockPos.Z].localFaceCount = 0;
	u16 verticesUsedByBlock = 0;

	// front face
	if(facesRequired & (1 << CD::FACE_BACK)) {
		// get/create indices to all four vertices
		u32 requiredVertices[] { CD::VERT_BDL, CD::VERT_BUL, CD::VERT_BUR, CD::VERT_BDR };	// placing vertices clockwise makes normals face the right way
		CreateIndices(requiredVertices, CD::FACE_BACK, verticesUsedByBlock, blockPos, blockType);

		this->blockMeta[blockPos.X + CHUNK_SIZE * blockPos.Y + CHUNK_SIZE * CHUNK_SIZE * blockPos.Z].localFaceCount++;
	}

	// back face
	if(facesRequired & (1 << CD::FACE_FRONT)) {
		// get/create indices to all four vertices
		u32 requiredVertices[] { CD::VERT_FDL, CD::VERT_FDR, CD::VERT_FUR, CD::VERT_FUL };
		CreateIndices(requiredVertices, CD::FACE_FRONT, verticesUsedByBlock, blockPos, blockType);

		this->blockMeta[blockPos.X + CHUNK_SIZE * blockPos.Y + CHUNK_SIZE * CHUNK_SIZE * blockPos.Z].localFaceCount++;
	}

	// up face
	/*if(facesRequired & (1 << CD::FACE_TOP)) {
		// get/create indices to all four vertices
		u32 requiredVertices[] { CD::VERT_FUL, CD::VERT_BUL, CD::VERT_BUR, CD::VERT_FUR };
		CreateIndices(requiredVertices, verticesUsedByBlock, blockPos, blockType);

		this->blockMeta[blockPos.X][blockPos.Y][blockPos.Z].localFaceCount++;
	}*/

	// up special face (no shared vertices)
	if(facesRequired & (1 << CD::FACE_TOP_SPECIAL)) {
		u32 requiredVertices[] { CD::VERT_BUL_SPECIAL, CD::VERT_FUL_SPECIAL, CD::VERT_FUR_SPECIAL, CD::VERT_BUR_SPECIAL };
		CreateIndices(requiredVertices, CD::FACE_TOP_SPECIAL, verticesUsedByBlock, blockPos, blockType);

		this->blockMeta[blockPos.X + CHUNK_SIZE * blockPos.Y + CHUNK_SIZE * CHUNK_SIZE * blockPos.Z].localFaceCount++;
	}

	// down face
	/*if(facesRequired & (1 << CD::FACE_BOTTOM)) {
		// get/create indices to all four vertices
		u32 requiredVertices[] { CD::VERT_FDL, CD::VERT_FDR, CD::VERT_BDR, CD::VERT_BDL };
		CreateIndices(requiredVertices, verticesUsedByBlock, blockPos, blockType);

		this->blockMeta[blockPos.X][blockPos.Y][blockPos.Z].localFaceCount++;
	}*/

	// down special face (no shared vertices)
	if(facesRequired & (1 << CD::FACE_BOTTOM_SPECIAL)) {
		u32 requiredVertices[] { CD::VERT_BDL_SPECIAL, CD::VERT_BDR_SPECIAL, CD::VERT_FDR_SPECIAL, CD::VERT_FDL_SPECIAL };
		CreateIndices(requiredVertices, CD::FACE_BOTTOM_SPECIAL, verticesUsedByBlock, blockPos, blockType);

		this->blockMeta[blockPos.X + CHUNK_SIZE * blockPos.Y + CHUNK_SIZE * CHUNK_SIZE * blockPos.Z].localFaceCount++;
	}

	// left face
	if(facesRequired & (1 << CD::FACE_LEFT)) {
		u32 requiredVertices[] { CD::VERT_BDL, CD::VERT_FDL, CD::VERT_FUL, CD::VERT_BUL };
		CreateIndices(requiredVertices, CD::FACE_LEFT, verticesUsedByBlock, blockPos, blockType);

		this->blockMeta[blockPos.X + CHUNK_SIZE * blockPos.Y + CHUNK_SIZE * CHUNK_SIZE * blockPos.Z].localFaceCount++;
	}

	// right face
	if(facesRequired & (1 << CD::FACE_RIGHT)) {
		// get/create indices to all four vertices
		u32 requiredVertices[] { CD::VERT_BDR, CD::VERT_BUR, CD::VERT_FUR, CD::VERT_FDR };
		CreateIndices(requiredVertices, CD::FACE_RIGHT, verticesUsedByBlock, blockPos, blockType);

		this->blockMeta[blockPos.X + CHUNK_SIZE * blockPos.Y + CHUNK_SIZE * CHUNK_SIZE * blockPos.Z].localFaceCount++;
	}
}

void IChunkSceneNode::CreateIndices(const u32 requiredVertices[], const u8& requiredFace, u16 &verticesUsedByBlock, const v3s &blockPos, const u8 &blockType)
{
	u16 indices[4];
	for(u8 i = 0; i < 4; i++) {
		// returns the position in vertices[] array of a vertex located at blockPos
		// and creates a new vertex if none exists yet
		indices[i] = GetVertexIndex(requiredVertices[i], verticesUsedByBlock, blockPos, blockType);
	}

	// first triangle
	this->indicesCreationArrays[requiredFace].append(indices[0]);	// placing vertices clockwise makes normals face the right way
	this->indicesCreationArrays[requiredFace].append(indices[1]);
	this->indicesCreationArrays[requiredFace].append(indices[2]);

	// second triangle
	this->indicesCreationArrays[requiredFace].append(indices[2]);	// reusing vertices from first triangle in second triangle
	this->indicesCreationArrays[requiredFace].append(indices[3]);
	this->indicesCreationArrays[requiredFace].append(indices[0]);

	/*this->indicesCreationArray.append(indices[0]);
	this->indicesCreationArray.append(indices[1]);
	this->indicesCreationArray.append(indices[2]);
	this->indicesCreationArray.append(indices[2]);
	this->indicesCreationArray.append(indices[3]);
	this->indicesCreationArray.append(indices[0]);*/

	for(u8 i = 0; i < 4; i++) {
		verticesUsedByBlock |= 1 << requiredVertices[i];
	}

}

u16 IChunkSceneNode::GetVertexIndex(const u32 &requiredVertex, const u16 &verticesUsedByBlock, const v3s &blockPos, const u8 &blockType) {
	vector3df vPos(blockPos.X + CD::vertexLocalPos[requiredVertex].X, blockPos.Y + CD::vertexLocalPos[requiredVertex].Y, blockPos.Z + CD::vertexLocalPos[requiredVertex].Z);

	// see if we already used this vertex so we don't have to iterate through vertexProviders
	// TODO: top/bottom faces can probably share two vertices with other faces, even with planned mechanism
	BlockMetaData* meta = &blockMeta[blockPos.X + CHUNK_SIZE * blockPos.Y + CHUNK_SIZE * CHUNK_SIZE * blockPos.Z];
	if(verticesUsedByBlock & (1 << requiredVertex)) {
		const u16 idx = FindVertexIndex(vPos, meta);				// vertexNormals[vertex]
		if(idx != (u16)-1)
			return idx;
	}

	// TODO: give every second chunk inverted UV coordinates, merge vertices between blocks

	// old code, probably won't work well with textures (although it might if we also check for UV equalness, gonna check later):
	/*
	// if the vertex wasn't used by the current block search in relevant blocks around the current block
	u32 vProvs = vertexProviders[vertex];
	for(s32 i = 0; vProvs != 0 && i < CD::DIR_MAXPROVIDERS; i++) {
		if(vProvs & 1) {
			BlockMetaData* meta = GetBlockMeta(blockPos + CD::adjacentDir[i]);
			if(meta != nullptr) {
				u16 idx = FindVertexIndex(vPos, meta);		// vertexNormals[vertex]
				if(idx != (u16)-1)
					return idx;
			}
		}
		vProvs >>= 1;
	}*/

	if(meta->globalVertexCount == (u16)-1) {
		// set first vertex created for this block to current index
		meta->globalVertexCount = this->verticesCreationArray.size();
	}
	meta->localVertexCount++;
	const f64 yPos((blockType / CD::tilesPerTextureRow) / CD::tilesPerTextureRow);
	const f64 xPos((blockType - yPos * CD::tilesPerTextureRow) / CD::tilesPerTextureRow);
	this->verticesCreationArray.append(S3DVertex(vPos, CD::vertexNormals[requiredVertex], SColor(255, 0, 0, 255), CD::vertexUV[requiredVertex] + v2f(xPos, yPos)));
	return this->verticesCreationArray.size() - 1;
}

u16 IChunkSceneNode::FindVertexIndex(const vector3df &vPos, const BlockMetaData* meta) const {
	for(u16 index = meta->globalVertexCount; index < meta->globalVertexCount + meta->localVertexCount; index++) {
		if(this->verticesCreationArray[index].Pos == vPos) {
			return index;
		}
	}

	/*for(u32 index = 6 * meta->globalFaceCount; index < 6 * meta->globalFaceCount + 6 * meta->localFaceCount; index++)	{
		// if count++ == 3 count = 1 continue
		if(this->verticesCreationArray[this->indicesCreationArray[index]].Pos == vPos ) {		//&& this->Vertices[this->Indices[index]].Normal == vNormal)	// irrlicht doesn't need normals for regular rendering
			return this->indicesCreationArray[index];
		}
	}*/

	return (u16)-1;
}

const IChunkSceneNode::BlockMetaData* IChunkSceneNode::GetBlockMeta(const v3s &pos) const {
	if(!(pos.X < 0 || pos.X >= CHUNK_SIZE || pos.Y < 0 || pos.Y >= CHUNK_SIZE || pos.Z < 0 || pos.Z >= CHUNK_SIZE))
		return &this->blockMeta[pos.X + CHUNK_SIZE * pos.Y + CHUNK_SIZE * CHUNK_SIZE * pos.Z];

	return nullptr;
}

Block* IChunkSceneNode::GetBlock(const v3s &pos) {
	//if(!(pos.X < 0 || pos.X >= CHUNK_SIZE || pos.Y < 0 || pos.Y >= CHUNK_SIZE || pos.Z < 0 || pos.Z >= CHUNK_SIZE))
		return &this->Blocks[pos.X + CHUNK_SIZE * pos.Y + CHUNK_SIZE * CHUNK_SIZE * pos.Z];	// regular case, return block that is part of this chunk

	//return nullptr;
}

const u8 &IChunkSceneNode::GetBlockTypeBitMask(const v3s &pos) const {
	if(!(pos.X < 0 || pos.X >= CHUNK_SIZE || pos.Y < 0 || pos.Y >= CHUNK_SIZE || pos.Z < 0 || pos.Z >= CHUNK_SIZE)) {
		if(this->singleBlockType != Block::Bt_Invalid) {
			return BlockData::blockTypeBitmasks[this->singleBlockType];
		} else {
			return this->Blocks[pos.X + CHUNK_SIZE * pos.Y + CHUNK_SIZE * CHUNK_SIZE * pos.Z].GetBlockTypeBitMask();	// regular case, return block that is part of this chunk
		}
	}

	vector3di chunkToGet(IChunkTableSceneNode::PosToChunkCoord(this->getPosition() + vector3df(pos.X, pos.Y, pos.Z)));

	ChunkPtr chunk;
	this->updateMutex->lock();
	for(u8 adjacentFaces = 0; adjacentFaces < CD::FACE_COUNT; adjacentFaces++) {
		if(this->neighbourChunks[adjacentFaces] != nullptr && this->neighbourChunks[adjacentFaces]->GetChunkPosition() == chunkToGet) {
			chunk = this->neighbourChunks[adjacentFaces];
			break;
		}
	}
	this->updateMutex->unlock();

	if(chunk == nullptr) {
		return BD::blockTypeBitmasks[Block::BT_Dirt];	// if the chunk isn't in memory, assume it's dirt so we don't render a face on the outside of the visible world
	}

	return chunk->GetBlockTypeBitMask(v3s(pos.X - (pos.X / CHUNK_SIZE - (pos.X < 0 ? 1 : 0)) * CHUNK_SIZE,		// modulo
										  pos.Y - (pos.Y / CHUNK_SIZE - (pos.Y < 0 ? 1 : 0)) * CHUNK_SIZE,		// and in case of negative chunk positions
										  pos.Z - (pos.Z / CHUNK_SIZE - (pos.Z < 0 ? 1 : 0)) * CHUNK_SIZE		// subtract one
										  ));
}

void IChunkSceneNode::SetBlockType(const vector3di& pos, const u8 &type) {
	Block* block = GetBlock(pos);
	if(block == nullptr)
		return;

	if(block->GetBlockType() == type)
		return;

	this->blockTypeCount[type]++;
	this->blockTypeCount[block->GetBlockType()]--;
	if(this->blockTypeCount[type] == CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE) {
		this->singleBlockType = type;
		//DeleteBlockArray();
	} else if(this->singleBlockType != Block::Bt_Invalid) {
		//InitializeBlockArray();
		this->singleBlockType = Block::Bt_Invalid;
	}
	block->SetBlockType(type);

	// switch off the informed bit for affected neighbours
	if(pos.X == 0)
		this->neighboursInformed &= u8(-1) ^ 1<<CD::FACE_LEFT;
	else if(pos.X == CHUNK_SIZE - 1)
		this->neighboursInformed &= u8(-1) ^ 1<<CD::FACE_RIGHT;
	if(pos.Z == 0)
		this->neighboursInformed &= u8(-1) ^ 1<<CD::FACE_BACK;
	else if(pos.Z == CHUNK_SIZE - 1)
		this->neighboursInformed &= u8(-1) ^ 1<<CD::FACE_FRONT;
	if(pos.Y == 0)
		this->neighboursInformed &= u8(-1) ^ 1<<CD::FACE_BOTTOM_SPECIAL;
	else if(pos.Y == CHUNK_SIZE - 1)
		this->neighboursInformed &= u8(-1) ^ 1<<CD::FACE_TOP_SPECIAL;
	//memset(this->neighboursKnown, false, sizeof(bool) * CD::FACE_COUNT);	// notify neighbours some time in the future
	this->blocksChanged = true;
}

} // irr::scene
} // irr









