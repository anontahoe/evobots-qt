/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "hudmanager.h"
#include "engine.h"
#include "evohudnode.h"

using namespace irr;
using namespace core;
using namespace scene;

HudManager::HudManager(IGraphics *graphics)
	: graphics(graphics),
	  hudRootNode(nullptr),
	  hudPositions { },
	  hudSize { },
	  uvCoords { },
	  uvSize { }
{
	this->hudPositions[HUD_CrossHair] = v3f(-0.1f,-0.1,0);
	this->hudSize[HUD_CrossHair] = v3f(0.2f, 0.2f, 0);

	for(u8 i = 0; i < HUD_Count; i++) {
		u8 tilesPerRow(16);
		f64 tileWidth = 1/tilesPerRow;
		v2f startPos(0, tileWidth * 8);
		u8 y = i / tilesPerRow;
		u8 x = i - (y*tilesPerRow);
		v2f uv(startPos + v2f(x, y));
		this->uvCoords[i] = uv;
		this->uvSize[i] = v2f(1/tilesPerRow);
	}
}

HudManager::~HudManager() {

}

void HudManager::Initialize() {
	this->hudRootNode = this->graphics->CreateHudSceneNode(this->graphics->GetRootSceneNode());
}

void HudManager::AddHudElement(const u8 &element) {
	EvoHudNode* hudNode(new EvoHudNode(this->hudRootNode, nullptr, -1, this->hudPositions[element], this->hudSize[element], this->uvCoords[element], this->uvSize[element], this->graphics));
}
