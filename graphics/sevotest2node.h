#ifndef SEVOTEST2NODE_H
#define SEVOTEST2NODE_H

#include "sevoscenenode.h"

namespace irr {
namespace scene {


class SEvoTest2Node : public SEvoSceneNode
{
public:
	SEvoTest2Node(ISceneNode *parent, ISceneManager *smgr, s32 id, v3f position, IGraphics* graphics);
	~SEvoTest2Node();

	f32 g_vertex_buffer_data[4*3];
	u16 g_index_buffer_data[6];
	f32 g_color_buffer_data[36*3];
};


} // irr::scene
} // irr



#endif // SEVOTEST2NODE_H
