/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "utils.h"


using namespace core;

namespace utils {

void Sleeper::sleep(u32 ms) {
	QThread::sleep(ms);
}

s32 MurmurHash(s32 key)
{
	static const s32 c1 = 0xcc9e2d51;
	static const s32 c2 = 0x1b873593;
	static const s32 r1 = 15;
	static const s32 r2 = 13;
	static const s32 m = 5;
	static const s32 n = 0xe6546b64;

	s32 hash = 0xfafafafa;

	key *= c1;
	key = (key << r1) | (key >> (32 - r1));
	key *= c2;

	hash ^= key;
	hash = ((hash << r2) | (hash >> (32 - r2))) * m + n;

	hash ^= 4;
	hash ^= (hash >> 16);
	hash *= 0x85ebca6b;
	hash ^= (hash >> 13);
	hash *= 0xc2b2ae35;
	hash ^= (hash >> 16);

	return hash;
}

vector3du MurmurHash(const vector3di &key)
{
	return vector3du(MurmurHash(key.X), MurmurHash(key.Y), MurmurHash(key.Z));
}

s32 MurmurHash(const char *key, s32 len, s32 seed)
{
	static const s32 c1 = 0xcc9e2d51;
	static const s32 c2 = 0x1b873593;
	static const s32 r1 = 15;
	static const s32 r2 = 13;
	static const s32 m = 5;
	static const s32 n = 0xe6546b64;

	s32 hash = seed;

	const int nblocks = len / 4;
	const s32 *blocks = (const s32 *) key;
	int i;
	for (i = 0; i < nblocks; i++) {
		s32 k = blocks[i];
		k *= c1;
		k = (k << r1) | (k >> (32 - r1));
		k *= c2;

		hash ^= k;
		hash = ((hash << r2) | (hash >> (32 - r2))) * m + n;
	}

	const s8 *tail = (const s8 *) (key + nblocks * 4);
	s32 k1 = 0;

	switch (len & 3) {
	case 3:
		k1 ^= tail[2] << 16;
	case 2:
		k1 ^= tail[1] << 8;
	case 1:
		k1 ^= tail[0];

		k1 *= c1;
		k1 = (k1 << r1) | (k1 >> (32 - r1));
		k1 *= c2;
		hash ^= k1;
	}

	hash ^= len;
	hash ^= (hash >> 16);
	hash *= 0x85ebca6b;
	hash ^= (hash >> 13);
	hash *= 0xc2b2ae35;
	hash ^= (hash >> 16);

	return hash;
}

}	// namespace utils
