#ifndef UTILS_H
#define UTILS_H

#include "typedefs.h"
#include "utils/timerglew.h"
#include <QThread>

using namespace irr;

namespace utils {

	static TimerGlew Timer;

	template <typename T> inline constexpr
	int signum(T x, std::false_type is_signed) {
		return T(0) < x;
	}

	template <typename T> inline constexpr
	int signum(T x, std::true_type is_signed) {
		return (T(0) < x) - (x < T(0));
	}

	template <typename T> inline constexpr
	int signum(T x) {
		return signum(x, std::is_signed<T>());
	}

	s32 MurmurHash(s32 key);

	core::vector3du MurmurHash(const core::vector3di& key);

	s32 MurmurHash(const char* key, s32 len, s32 seed);

	class Sleeper : public QThread {
	public:
		static void sleep(u32 ms);
	};

}	// namespace utils

#endif // UTILS_H
