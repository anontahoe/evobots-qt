/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "timerglew.h"
#include <GLFW/glfw3.h>

TimerGlew::TimerGlew()
	: lastTime(0),
	  dTime(0),
	  dTimeMs(0)
{

}

TimerGlew::~TimerGlew() {

}

void TimerGlew::Tick() {
	double now = glfwGetTime();
	this->dTime = now - this->lastTime;
	this->dTimeMs = dTime * 1000;
	this->lastTime = now;
}

double TimerGlew::GetNow() const {
	return glfwGetTime();
}

double TimerGlew::GetNowMs() const {
	return glfwGetTime() * 1000;
}

double* TimerGlew::StartTiming() const {
	return new double(GetNow());
}

double TimerGlew::StopTiming(double* timerStart) const {
	double dTime = GetNow() - *timerStart;
	delete timerStart;
	return dTime;
}
