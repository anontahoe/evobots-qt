#ifndef TIMERGLEW_H
#define TIMERGLEW_H

#include "typedefs.h"

class TimerGlew
{
public:
				TimerGlew();
				~TimerGlew();

	void		Tick();
	double		GetNow() const;
	double		GetNowMs() const;
	double*		StartTiming() const;
	double		StopTiming(double* timerStart) const;

	double		lastTime;
	double		dTime;
	irr::u32	dTimeMs;
};

#endif // TIMERGLEW_H
