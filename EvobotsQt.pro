#-------------------------------------------------
#
# Project created by QtCreator 2014-12-04T01:53:45
#
#-------------------------------------------------

QT	   += core gui

CONFIG += c++11 static staticlib
CONFIG -= debug_and_release

TARGET = EvobotsQt
TEMPLATE = app

INCLUDEPATH += graphics core utils ui

LIBS	+= -lIrrlicht -lGLEW -lglfw -lpng

#QMAKE_CXXFLAGS += "-std=c++11 -g -march=native -maes -mavx -mmmx -mpopcnt -msse -msse2 -msse3 -msse4.1 -msse4.2 -mssse3 -fstack-protector -fomit-frame-pointer"
QMAKE_CXXFLAGS += "-std=c++11 -march=native -mtune=native -fomit-frame-pointer"

CONFIG(debug, debug|release) {
	message("debug")
}
CONFIG(release) {
	QMAKE_CXXFLAGS_RELEASE += -march=native -mtune=native -fomit-frame-pointer
	message("release")
}

exists("disable-pax.pri") {
   include("disable-pax.pri")     # so i can debug on gentoo
}

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

DISTFILES += \
	graphics/shaders/SimpleVertexShader.glsl \
	graphics/shaders/SimpleFragmentShader.glsl


SOURCES += main.cpp\
	ui/evobots.cpp \
	ui/uiupdater.cpp \
	graphics/graphics.cpp \
	graphics/block.cpp \
	graphics/chunk.cpp \
	graphics/chunktable.cpp \
	core/terraingenerator.cpp \
	core/chunkmanager.cpp \
	core/engine.cpp \
	core/logger.cpp \
	core/threadmanager.cpp \
	core/threadable.cpp \
	core/chunkloadmanager.cpp \
	graphics/graphicsglew.cpp \
	graphics/csamplescenenode.cpp \
	core/chunkupdatemanager.cpp \
	evobotsmainthread.cpp \
	core/chunkloadworker.cpp \
	core/chunkupdateworker.cpp \
	graphics/shaderloader.cpp \
	graphics/pngloader.cpp \
	core/inputmanager.cpp \
	graphics/sevoscenenode.cpp \
	graphics/sevotestnode.cpp \
	graphics/sevocameranode.cpp \
	graphics/sevotexture.cpp \
	graphics/sevomaterial.cpp \
	graphics/sevotest2node.cpp \
	graphics/sevotest3node.cpp \
	graphics/actor.cpp \
	utils/utils.cpp \
	utils/timerglew.cpp \
	core/chunkconstructiondata.cpp \
	core/chunkhelper.cpp \
	graphics/evohudnode.cpp \
	graphics/hudmanager.cpp \
	graphics/scenemanager.cpp

HEADERS  += ui/evobots.h \
	ui/uiupdater.h \
	graphics/graphics.h \
	graphics/igraphics.h \
	graphics/block.h \
	graphics/chunk.h \
	graphics/chunktable.h \
	core/terraingenerator.h \
	core/chunkmanager.h \
	core/typedefs.h \
	core/engine.h \
	core/iengine.h \
	core/logger.h \
	core/threadmanager.h \
	core/threadable.h \
	core/chunkloadmanager.h \
	graphics/graphicsglew.h \
	graphics/csamplescenenode.h \
	core/chunkupdatemanager.h \
	core/chunkconstructiondata.h \
	evobotsmainthread.h \
	core/chunkupdateworker.h \
	core/chunkloadworker.h \
	graphics/shaderloader.h \
	graphics/pngloader.h \
	core/inputmanager.h \
	graphics/sevoscenenode.h \
	graphics/sevomaterial.h \
	graphics/sevotexture.h \
	graphics/sevotestnode.h \
	graphics/sevocameranode.h \
	graphics/sevotest2node.h \
	graphics/sevotest3node.h \
	graphics/actor.h \
	utils/utils.h \
	utils/timerglew.h \
	core/chunkhelper.h \
	graphics/evohudnode.h \
	graphics/hudmanager.h \
	graphics/scenemanager.h

FORMS	+= ui/evobots.ui





