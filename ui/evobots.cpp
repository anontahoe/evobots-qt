/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "evobots.h"
#include "ui_evobots.h"
#include "logger.h"
#include "threadmanager.h"
#include "evobotsmainthread.h"
#include "uiupdater.h"


using namespace irr;
using namespace core;


Evobots::Evobots(QWidget *parent)
	: QMainWindow(parent),
	  ui(new Ui::Evobots),
	  uiUpdater(new UiUpdater(this->ui))
{
	this->ui->setupUi(this);

	Logger::Log("Starting up...");
	Logger::Log("--------------");

	connect(this, SIGNAL(QuitApplicationSignal()), SLOT(QuitApplicationSlot()));
}

Evobots::~Evobots() {
	delete this->uiUpdater;
	this->uiUpdater = nullptr;

	delete this->ui;
	this->ui = nullptr;
}

void Evobots::on_pushButton_clicked() {
	EvobotsMainThread* mainThread(new EvobotsMainThread(this));
	mainThread->GetThreadManager()->AddDetatchedThread(mainThread);
	this->uiUpdater->StartLoop();
}

void Evobots::QuitApplication() {
	emit QuitApplicationSignal();
}

void Evobots::QuitApplicationSlot() {
	QCoreApplication::quit();
}

Ui::Evobots* Evobots::GetUi() {
	return this->ui;
}

UiUpdater* Evobots::GetUiUpdater() const {
	return this->uiUpdater;
}
