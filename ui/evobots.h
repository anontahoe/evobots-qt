#ifndef EVOBOTS_H
#define EVOBOTS_H

#include <QMainWindow>
#include "threadable.h"

namespace Ui {
class Evobots;
//class EvobotsMainThread;
}

class UiUpdater;

class Evobots : public QMainWindow {
	Q_OBJECT

public:
	explicit		Evobots(QWidget *parent = 0);
					~Evobots();

	void			QuitApplication();
	Ui::Evobots*	GetUi();
	UiUpdater*		GetUiUpdater() const;

signals:
	void			QuitApplicationSignal();

private slots:
	void			on_pushButton_clicked();
	void			QuitApplicationSlot();

private:
	Ui::Evobots*	ui;
	UiUpdater*		uiUpdater;
};

#endif // EVOBOTS_H
