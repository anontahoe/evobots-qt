/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "uiupdater.h"
#include "evobots.h"
#include "ui_evobots.h"

UiUpdater::UiUpdater(Ui::Evobots *evoBotsMainWindow, QObject *parent)
	: QObject(parent),
	  evoBots(evoBotsMainWindow),
	  currentHistoryTime(0),
	  maxHistoryTimes(20),
	  renderTimeCt(0.0),
	  renderTimeCts(std::vector<double>(maxHistoryTimes)),
	  mainLoopTime(0.0),
	  mainLoopTimes(std::vector<double>(maxHistoryTimes)),
	  mainLoopPartTime(0.0),
	  mainLoopPartTimes(std::vector<double>(maxHistoryTimes)),
	  lcdWidgets {},
	  lcdValues {}
{
	connect(this, SIGNAL(StartLoopSignal()), this, SLOT(StartLoopSlot()), Qt::QueuedConnection);
	connect(this, SIGNAL(UpdateSelfSignal()), this, SLOT(UpdateSelfSlot()), Qt::QueuedConnection);
}

UiUpdater::~UiUpdater() {
}

void UiUpdater::StartLoop() {
	this->lcdWidgets[Ui_SevoNodes]				= this->evoBots->lcdSEvoNodes;
	this->lcdWidgets[Ui_ChunkNodes]				= this->evoBots->lcdChunkNodes;
	this->lcdWidgets[Ui_ChunkTableCount]		= this->evoBots->lcdChunkTable;
	this->lcdWidgets[Ui_RenderTimeCt]			= this->evoBots->lcdRenderTime;
	this->lcdWidgets[Ui_MainLoopTime]			= this->evoBots->lcdMainLoopTime;
	this->lcdWidgets[Ui_MainLoopPartTime]		= this->evoBots->lcdMainLoopPartTime;
	this->lcdWidgets[Ui_ChunksToLoad]			= this->evoBots->lcdToLoad;
	this->lcdWidgets[Ui_ChunksLoading]			= this->evoBots->lcdLoading;
	this->lcdWidgets[Ui_ChunksToSetBuffer]		= this->evoBots->lcdToSetBuffer;
	this->lcdWidgets[Ui_ChunksToRebuild]		= this->evoBots->lcdToRebuild;
	this->lcdWidgets[Ui_ChunksToRefresh]		= this->evoBots->lcdToRefresh;
	this->lcdWidgets[Ui_ChunksToInform]			= this->evoBots->lcdToInform;
	this->lcdWidgets[Ui_ChunksToDelete]			= this->evoBots->lcdToDelete;

	this->StartLoopSignal();
}

void UiUpdater::StartLoopSlot() {
	//this->UpdateSelfSignal();
}

void UiUpdater::UpdateSelfSlot() {
	SetUiValue(s32(this->renderTimeCt * 1000), Ui_RenderTimeCt);
	SetUiValue(s32(this->mainLoopTime * 1000), Ui_MainLoopTime);
	SetUiValue(s32(this->mainLoopPartTime * 1000), Ui_MainLoopPartTime);

	for(u16 i = 0; i < Ui_UiValueCount; i++) {
		this->lcdWidgets[i]->display(this->lcdValues[i]);
	}
}

void UiUpdater::UpdateAverageTime(const double& dTime, double& dTimeOut, std::vector<double>& timeHistory) {
	timeHistory[this->currentHistoryTime] = dTime;
	for(u8 i = 0; i < this->maxHistoryTimes; i++) {
		dTimeOut += timeHistory[i];
	}
	dTimeOut /= this->maxHistoryTimes;
}

void UiUpdater::SetRenderTimeCt(const double &dTime) {
	this->currentHistoryTime++;
	if(this->currentHistoryTime >= this->maxHistoryTimes) {
		this->currentHistoryTime = 0;
	}
	UpdateAverageTime(dTime, this->renderTimeCt, this->renderTimeCts);
}

void UiUpdater::SetMainLoopTime(const double &dTime) {
	UpdateAverageTime(dTime, this->mainLoopTime, this->mainLoopTimes);
}

void UiUpdater::SetMainLoopPartTime(const double &dTime) {
	UpdateAverageTime(dTime, this->mainLoopPartTime, this->mainLoopPartTimes);
}

void UiUpdater::SetUiValue(const u32 &value, const UiValue &uiElement) {
	this->lcdValues[uiElement] = value;
}







