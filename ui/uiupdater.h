#ifndef UIUPDATER_H
#define UIUPDATER_H

#include "typedefs.h"
#include <QObject>

// TODO: create abstract interface that can be used without Qt

namespace Ui {
	class Evobots;
}
class QLCDNumber;

using namespace irr;

enum UiValue {
	Ui_SevoNodes,
	Ui_ChunkNodes,
	Ui_ChunkTableCount,
	Ui_RenderTimeCt,
	Ui_MainLoopTime,
	Ui_MainLoopPartTime,
	Ui_ChunksToLoad,
	Ui_ChunksLoading,
	Ui_ChunksToSetBuffer,
	Ui_ChunksToRebuild,
	Ui_ChunksToInform,
	Ui_ChunksToRefresh,
	Ui_ChunksToDelete,
	Ui_UiValueCount
};

class UiUpdater : public QObject
{
	Q_OBJECT
public:
	explicit UiUpdater(Ui::Evobots *evoBotsMainWindow, QObject *parent = 0);
	~UiUpdater();

	void	StartLoop();
	void	SetSevoNodes(const u32& count);
	void	SetChunkNodes(const u32& count);
	void	SetChunkTableCount(const u32& count);
	void	SetRenderTimeCt(const double& dTime);
	void	SetMainLoopTime(const double& dTime);
	void	SetMainLoopPartTime(const double &dTime);
	void	SetChunksToLoad(const u32& count);
	void	SetChunksLoading(const u32& count);
	void	SetChunksToSetBuffer(const u32& count);
	void	SetChunksToRebuild(const u32& count);
	void	SetChunkNeighboursUpdated(const u32& count);
	void	SetChunksToDelete(const u32& count);
	void	SetUiValue(const u32& value, const UiValue& uiElement);

signals:
	void		StartLoopSignal();
	void		UpdateSelfSignal();

public slots:
	void		StartLoopSlot();
	void		UpdateSelfSlot();

private:
	void		UpdateAverageTime(const double &dTime, double &dTimeOut, std::vector<double>& timeHistory);

	Ui::Evobots* evoBots;
	u8			currentHistoryTime;
	const u8	maxHistoryTimes;
	double		renderTimeCt;
	std::vector<double> renderTimeCts;		// TODO: replace with fixed-size array
	double		mainLoopTime;
	std::vector<double> mainLoopTimes;
	double		mainLoopPartTime;
	std::vector<double> mainLoopPartTimes;

	QLCDNumber*	lcdWidgets[Ui_UiValueCount];
	s32			lcdValues[Ui_UiValueCount];
};

#endif // UIUPDATER_H
