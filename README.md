EvobotsQt
=========

This is a hobby project of mine, right now there is just an unfinished voxel engine but i hope to someday turn this into a Darwinbots clone.

#### Dependencies
*	Qt (for the GUI, nearly removed from inner engine)
*	C++11 capable compiler
*	Irrlicht engine
*	GLEW
*	GLFW
*	libpng

#### How To use  
*	Compile
*	Start EvobotsQt
*	Click the button
*	Press ESC to exit, use arrow keys and mouse for movement

#### Notes  
*	Silly as it sounds, the world size is currently hardcoded in chunkmanager.h so be careful you don't run out of RAM when blindly starting the software.  
	A  12x12 grid uses about 220MB of RAM according to my taskmanager so that should be safe.  
*	Asset paths are hardcoded too (relative to the working directory), if you want to see textures you need to extract [this archive] [media.tar.gz] into the parent folder of your build target directory.  
*	The two .glsl shaders in graphics/shaders/ aren't automatically copied to the media folder yet. The build system definitely needs improvements.
*	All my commits are GPG signed but git just signs the sha1 hashes so this doesn't add security. View with git log --show-siganture

[media.tar.gz]: https://dump.bitcheese.net/files/ifyvuve/media.tar.gz
