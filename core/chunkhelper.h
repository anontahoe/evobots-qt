#ifndef CHUNKHELPER_H
#define CHUNKHELPER_H

#include "typedefs.h"

using namespace irr;

class Block;
class IChunkTableSceneNode;

struct BlockSelection {
	Block* block = nullptr;
	core::v3f face = core::v3f(0);
	core::v3f pos = core::v3f(0);
};
typedef BlockSelection* BlockSelectionPtr;

class ChunkHelper
{
public:
	ChunkHelper();
	~ChunkHelper();

	static BlockSelectionPtr raycast(const core::v3f &origin, const core::v3f &direction, s32 radius, const IChunkTableSceneNode *chunkTable);
	static core::v3f	intBound(const core::v3f &s, const core::v3f &ds);
};

#endif // CHUNKHELPER_H
