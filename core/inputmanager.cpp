/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "inputmanager.h"
#include "engine.h"
#include "chunkmanager.h"
#include "sevocameranode.h"
#include <GLFW/glfw3.h>

using namespace irr;
using namespace core;

#define MOVE_FACTOR 0.005f

InputManager::InputManager(IEngine* engine, GLFWwindow *window)
	: engine(engine),
	  window(window),
	  windowSize(dim2i())
{

}

void InputManager::Initialize() {
	glfwGetWindowSize(this->window, &this->windowSize.Width, &this->windowSize.Height);
	glfwSetCursorPos(this->window, this->windowSize.Width / 2, this->windowSize.Height / 2);
}

InputManager::~InputManager() {
	this->engine = nullptr;
	this->window = nullptr;
}

void InputManager::CheckKeys(u32 timerMs) {
	glfwPollEvents();

	// close window
	if((glfwGetKey(this->window, GLFW_KEY_ESCAPE) == GLFW_PRESS) || (glfwWindowShouldClose(this->window) != 0)) {
		this->engine->ShutDown();
	}

	// cursor
	double mouseX, mouseY;
	glfwGetCursorPos(this->window, &mouseX, &mouseY);
	if((mouseX != 0) && (mouseY != 0)) {
		v3f rota = GetRelativeRota(mouseX, mouseY);
		this->engine->GetGraphics()->GetEvoCamera()->setRotation(this->engine->GetGraphics()->GetEvoCamera()->getRotation() + rota);
		glfwSetCursorPos(this->window, this->windowSize.Width / 2, this->windowSize.Height / 2);
	}

	// keys
	if(glfwGetKey(this->window, GLFW_KEY_SPACE) == GLFW_PRESS) {
		this->engine->GetGraphics()->CreateEvoTestNode(this->engine->GetGraphics()->GetRootSceneNode(),
													   this->engine->GetGraphics()->GetEvoCamera()->getPosition() +
													   this->engine->GetGraphics()->GetEvoCamera()->getRotation().rotationToDirection(v3f(0,0,-1)) * 3);
	}

	v3f moveDirection;
	if(glfwGetKey(this->window, GLFW_KEY_LEFT) == GLFW_PRESS) {
		moveDirection += v3f(-1.0f,0,0) * MOVE_FACTOR;
	}

	if(glfwGetKey(this->window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
		moveDirection += v3f(1.0f,0,0) * MOVE_FACTOR;
	}

	if(glfwGetKey(this->window, GLFW_KEY_UP) == GLFW_PRESS) {
		moveDirection += v3f(0,0,-1.0f) * MOVE_FACTOR;
	}

	if(glfwGetKey(this->window, GLFW_KEY_DOWN) == GLFW_PRESS) {
		moveDirection += v3f(0,0,1.0f) * MOVE_FACTOR;
	}

	moveDirection *= timerMs;
	this->engine->GetGraphics()->GetEvoCamera()->Move(moveDirection);
	this->engine->GetChunkManager()->SetCameraPosition(this->engine->GetGraphics()->GetEvoCamera()->getPosition());
}

v3f InputManager::GetRelativeRota(double mouseX, double mouseY) {
	mouseX /= (double)this->windowSize.Width / 2.0;
	mouseY /= (double)this->windowSize.Height / 2.0;
	mouseX -= 1.0;
	mouseY -= 1.0;

	v3f rota;
	rota.X = 45.0 * -mouseY;	/*irr::DEGTORAD64 **/
	rota.Y = 45.0 * -mouseX;	/*irr::DEGTORAD64 **/
	return rota;
}






























