#ifndef CHUNKREBUILDMANAGER_H
#define CHUNKREBUILDMANAGER_H

#include "typedefs.h"

class QMutex;
class IEngine;
class ChunkManager;
class TerrainGenerator;
class ChunkUpdateWorker;
namespace std { class thread; }

using namespace irr;
using namespace scene;

#define CHUNKUPDATETHREADS 4

class ChunkUpdateManager {
public:
	ChunkUpdateManager(IEngine *engine, ChunkManager *chunkManager);
	~ChunkUpdateManager();

	void				StopThreads();

	bool				RebuildJobsAvailable();
	ChunkPtr			GetFirstRebuildJob();
	void				RebuildChunk(const ChunkPtr &chunkPtr);
	void				RebuildChunks(const ChunkPtrSet &chunks);

	bool				DeleteJobsAvailable();
	ChunkPtr			GetFirstDeleteJob();
	void				DeleteChunk(ChunkPtr &chunkPtr);
	void				DeleteChunks(const ChunkPtrSet &chunks);

	bool				InformJobsAvailable();
	ChunkNeighbourUpdatedPair *GetFirstInformJob();
	void				InformChunkNeighbourCreated(const ChunkPtr &chunkPtr, const u8 &adjacentFaces);

	ChunkNeighbourUpdatedPair *GetFirstRefreshJob();
	void				RefreshChunkNeighbour(const ChunkPtr &chunkPtr, const u8 &adjacentFaces);

	s32					chunksToRebuildCount;
	s32					chunksToDeleteCount;
	s32					chunksToInformCount;
	s32					chunksToRefreshCount;
private:
	IEngine				*engine;

	Threadable*			chunkUpdateWorkers[CHUNKUPDATETHREADS];
	ChunkPtrSet			chunksToRebuild;
	QMutex*				chunksToRebuildLock;
	ChunkPtrSet			chunksToDelete;
	QMutex*				chunksToDeleteLock;
	ChunkNeighbourUpdatedMap	chunksToInform;
	QMutex*				chunksToInformLock;
	ChunkNeighbourUpdatedMap	chunksToRefresh;
	QMutex*				chunksToRefreshLock;
};

#endif // CHUNKREBUILDMANAGER_H
