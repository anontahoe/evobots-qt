#ifndef CHUNKLOADMANAGER_H
#define CHUNKLOADMANAGER_H

#include "typedefs.h"

class QMutex;
class IEngine;
class ChunkManager;
class TerrainGenerator;
class ChunkLoadWorker;

using namespace irr;

#define CHUNKLOADTHREADS 4

class ChunkLoadManager {
public:
						ChunkLoadManager(IEngine *engine, ChunkManager *chunkManager, TerrainGenerator *terra);
						~ChunkLoadManager();

	void				StopThreads();
	void				LoadChunk(const core::v3s &chunkPos);
	void				LoadChunkSafe(const core::v3s &chunkPos);
	void				LoadChunks(core::v3sSet chunks);
	bool				WorkAvailable();
	core::v3sPtr		GetFirstChunkPos();
	void				IgnoreChunk(const core::v3s& chunkPos);
	void				UnIgnoreChunk(const core::v3s& chunkPos);
	bool				TryStopLoadChunk(const core::v3s& chunkPos);
	bool				TryStopLoadChunkSafe(const core::v3s& chunkPos);
	void				DeliverChunk(const core::v3s& chunkPos, scene::ChunkPtr &chunkToSet);

	core::v3sSet		chunksToLoad;
	core::v3sSet		chunksLoading;
	QMutex*				chunksLoadingLock;
private:
	IEngine*			engine;
	ChunkManager*		chunkManager;

	ChunkLoadWorker*	chunkLoadThreads[CHUNKLOADTHREADS];

	core::v3sSet		chunksToIgnore;
};

#endif // CHUNKLOADMANAGER_H
