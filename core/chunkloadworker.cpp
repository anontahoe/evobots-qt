/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "chunkloadworker.h"
#include "chunkloadmanager.h"
#include "terraingenerator.h"
#include "chunkmanager.h"
#include "engine.h"
#include <thread>
#include "utils.h"
#include "typedefs.h"

using namespace irr;
using namespace irr::core;

ChunkLoadWorker::ChunkLoadWorker(ChunkLoadManager *chunkLoadManager, ChunkManager* chunkManager, const TerrainGenerator *terra) :
	Threadable(chunkManager->engine->GetThreadManager()),
	chunkLoadManager(chunkLoadManager),
	chunkManager(chunkManager),
	terra(terra)
{
}

ChunkLoadWorker::~ChunkLoadWorker() {
}

void ChunkLoadWorker::deleteLater() {
	Threadable::deleteLater();
}

void ChunkLoadWorker::DoWork() {
	Threadable::DoWork();
	LoadChunkLoop();
}

void ChunkLoadWorker::LoadChunkLoop() {
	while(!this->shutDown) {
		v3sPtr chunkToLoadPos(this->chunkLoadManager->GetFirstChunkPos());
		if(chunkToLoadPos != nullptr) {
			LoadChunk(*chunkToLoadPos.get());
		} else {
			std::this_thread::sleep_for(std::chrono::milliseconds(500));
		}
	}
}

void ChunkLoadWorker::LoadChunk(const v3s &posToLoad) {
	ChunkPtr chunk(this->terra->CreateChunk(posToLoad));
	this->chunkLoadManager->DeliverChunk(posToLoad, chunk);
	//this->chunkManager->SetChunkAt(chunk);
}
