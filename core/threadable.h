#ifndef THREADABLE_H
#define THREADABLE_H

#include "typedefs.h"

using namespace irr;

class ThreadManager;

class Threadable {
public:
						Threadable(ThreadManager* threadManager);
	virtual				~Threadable();

	virtual void		deleteLater();
	virtual void		StartWork();
	virtual void		StopWork();
	static void			StartWorkStatic(Threadable *threadable);
	static void			StartWorkDetached(Threadable* threadable);

	static s32			threadableCount;

protected:
	virtual void		DoWork();

	ThreadManager*		threadManager;
	bool				shutDown;
	bool				deleteAfterShutdown;
};

#endif // THREADABLE_H
