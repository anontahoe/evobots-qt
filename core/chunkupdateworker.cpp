/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "chunkupdateworker.h"
#include "chunkupdatemanager.h"
#include "chunkmanager.h"
#include "chunk.h"
#include <thread>
#include <chrono>

using namespace irr;
using namespace irr::core;

ChunkUpdateWorker::ChunkUpdateWorker(ChunkUpdateManager *chunkRebuildManager, ThreadManager* threadManager) :
	Threadable(threadManager),
	chunkUpdateManager(chunkRebuildManager)
{
}

ChunkUpdateWorker::~ChunkUpdateWorker() {
}

void ChunkUpdateWorker::deleteLater() {
	Threadable::deleteLater();
}

void ChunkUpdateWorker::DoWork() {
	Threadable::DoWork();
	ChunkUpdateLoop();
}

void ChunkUpdateWorker::ChunkUpdateLoop() {
	while(!this->shutDown) {
		bool workAvailable = false;
		ChunkPtr chunk = this->chunkUpdateManager->GetFirstRebuildJob();
		if(chunk != nullptr) {
			workAvailable = true;
			RebuildChunk(chunk);

		}

		chunk = this->chunkUpdateManager->GetFirstDeleteJob();
		if(chunk != nullptr) {
			workAvailable = true;
			chunk->Destroy();
		}

		ChunkNeighbourUpdatedPair* chunkToRefresh = this->chunkUpdateManager->GetFirstRefreshJob();
		if(chunkToRefresh != nullptr) {
			workAvailable = true;
			chunkToRefresh->first->RefreshNeighbourSlot(chunkToRefresh->second);
			delete chunkToRefresh;
		}

		ChunkNeighbourUpdatedPair* chunkToUpdate = this->chunkUpdateManager->GetFirstInformJob();
		if(chunkToUpdate != nullptr) {
			workAvailable = true;
			chunkToUpdate->first->InformNeighbourSlot(chunkToUpdate->first, chunkToUpdate->second);
			delete chunkToUpdate;
		}

		if(!workAvailable) {
			std::this_thread::sleep_for(std::chrono::milliseconds(500));
		}
	}
}

void ChunkUpdateWorker::RebuildChunk(const ChunkPtr &chunk) {
	IChunkSceneNode::Rebuild(chunk);
}
