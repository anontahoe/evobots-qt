#ifndef CHUNKLOADER_H
#define CHUNKLOADER_H

#include <irrlicht/irrlicht.h>
#include "typedefs.h"
#include "threadable.h"

using namespace irr;

class ChunkManager;
class ChunkLoadManager;
class TerrainGenerator;

class ChunkLoadWorker : public Threadable {
public:
	explicit ChunkLoadWorker(ChunkLoadManager *chunkLoadManager, ChunkManager *chunkManager, const TerrainGenerator *terra);
	virtual ~ChunkLoadWorker();

public slots:
	virtual void deleteLater();

protected slots:
	virtual void DoWork();

private:
	void LoadChunk(const core::v3s &posToLoad);
	void LoadChunkLoop();

	ChunkLoadManager *chunkLoadManager;
	ChunkManager* chunkManager;
	const TerrainGenerator* terra;	
};

#endif // CHUNKLOADER_H
