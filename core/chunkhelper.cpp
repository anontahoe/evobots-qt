/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "chunkhelper.h"
#include "utils.h"
#include "chunktable.h"
#include "chunk.h"
#include "block.h"
#include <cmath>

using namespace irr;
using namespace core;


ChunkHelper::ChunkHelper()
{

}

ChunkHelper::~ChunkHelper()
{

}

/**
	 * Call the callback with (x,y,z,value,face) of all blocks along the line
	 * segment from point 'origin' in vector direction 'direction' of length
	 * 'radius'. 'radius' may be infinite.
	 *
	 * 'face' is the normal vector of the face of that block that was entered.
	 * It should not be used after the callback returns.
	 *
	 * If the callback returns a true value, the traversal will be stopped.
	 */
BlockSelectionPtr ChunkHelper::raycast(const v3f &origin, const v3f &direction, s32 radius, const IChunkTableSceneNode* chunkTable) {
	// From "A Fast Voxel Traversal Algorithm for Ray Tracing"
	// by John Amanatides and Andrew Woo, 1987
	// <http://www.cse.yorku.ca/~amana/research/grid.pdf>
	// <http://citeseer.ist.psu.edu/viewdoc/summary?doi=10.1.1.42.3443>
	// Extensions to the described algorithm:
	//   • Imposed a distance limit.
	//   • The face passed through to reach the current cube is provided to
	//     the callback.

	// The foundation of this algorithm is a parameterized representation of
	// the provided ray,
	//                    origin + t * direction,
	// except that t is not actually stored; rather, at any given point in the
	// traversal, we keep track of the *greater* t values which we would have
	// if we took a step sufficient to cross a cube boundary along that axis
	// (i.e. change the integer part of the coordinate) in the variables
	// tMaxX, tMaxY, and tMaxZ.

	// Cube containing origin point.
	/*var x = Math.floor(origin[0]);
	var y = Math.floor(origin[1]);
	var z = Math.floor(origin[2]);
	// Break out direction vector.
	var dx = direction[0];
	var dy = direction[1];
	var dz = direction[2];*/
	// Direction to increment x,y,z when stepping.
	v3s step(utils::signum(direction.X),
			 utils::signum(direction.Y),
			 utils::signum(direction.Z));
	// See description above. The initial values depend on the fractional
	// part of the origin.
	v3f tMax(intBound(origin, direction));
	// The change in t when taking a step (always positive).
	v3f tDelta(v3f(step.X, step.Y, step.Z) / direction);

	// Avoids an infinite loop.
	if(direction.getLengthSQ() == 0) {
		return nullptr;
	}

	// Rescale from units of 1 cube-edge to units of 'direction' so we can
	// compare with 't'.
	radius /= direction.getLength();

	BlockSelectionPtr blockSelection(new BlockSelection());
	v3s blockWorldPos(std::floor(origin.X),
					  std::floor(origin.Y),
					  std::floor(origin.Z));

	v3s blockLocalPos(blockWorldPos.X - (blockWorldPos.X / CHUNK_SIZE - (blockWorldPos.X < 0 ? 1 : 0)) * CHUNK_SIZE,		// modulo
					  blockWorldPos.Y - (blockWorldPos.Y / CHUNK_SIZE - (blockWorldPos.Y < 0 ? 1 : 0)) * CHUNK_SIZE,		// and in case of negative chunk positions
					  blockWorldPos.Z - (blockWorldPos.Z / CHUNK_SIZE - (blockWorldPos.Z < 0 ? 1 : 0)) * CHUNK_SIZE		// subtract one
					  );

	ChunkPtr chunk(chunkTable->GetChunkAt(IChunkTableSceneNode::PosToChunkCoord(origin))); //(getBlock(pos));
	if(chunk == nullptr) {
		return nullptr;
	}
	blockSelection->block = chunk->GetBlock(blockLocalPos);
	if(blockSelection->block != nullptr) {
		if(blockSelection->block->GetBlockType() & Block::BT_Solid) {
			return blockSelection;
		}
	}

	while(true) {
		// Invoke the callback, unless we are not *yet* within the bounds of the
		// world.
		//if (!(blockSelection->pos.X < 0 || blockSelection->pos.Y < 0 || blockSelection->pos.Z < 0 || blockSelection->pos.X >= wx || blockSelection->pos.Y >= wy || blockSelection->pos.Z >= wz))
		//	if (callback(blockSelection->pos.X, blockSelection->pos.Y, blockSelection->pos.Z, blocks[blockSelection->pos.X*wy*wz + blockSelection->pos.Y*wz + blockSelection->pos.Z], face))
		//		break;

		// tMaxX stores the t-value at which we cross a cube boundary along the
		// X axis, and similarly for Y and Z. Therefore, choosing the least tMax
		// chooses the closest cube boundary. Only the first case of the four
		// has been commented in detail.
		if (tMax.X < tMax.Y) {
			if (tMax.X < tMax.Z) {
				if (tMax.X > radius) break;
				// Update which cube we are now in.
				blockLocalPos.X += step.X;
				// Adjust tMax.X to the next X-oriented boundary crossing.
				tMax.X += tDelta.X;
				// Record the normal vector of the cube face we entered.
				blockSelection->face.X = -step.X;
				blockSelection->face.Y = 0;
				blockSelection->face.Z = 0;
			} else {
				if (tMax.Z > radius) break;
				blockLocalPos.Z += step.Z;
				tMax.Z += tDelta.Z;
				blockSelection->face.X = 0;
				blockSelection->face.Y = 0;
				blockSelection->face.Z = -step.Z;
			}
		} else {
			if (tMax.Y < tMax.Z) {
				if (tMax.Y > radius) break;
				blockLocalPos.Y += step.Y;
				tMax.Y += tDelta.Y;
				blockSelection->face.X = 0;
				blockSelection->face.Y = -step.Y;
				blockSelection->face.Z = 0;
			} else {
				// Identical to the second case, repeated for simplicity in
				// the conditionals.
				if (tMax.Z > radius) break;
				blockLocalPos.Z += step.Z;
				tMax.Z += tDelta.Z;
				blockSelection->face.X = 0;
				blockSelection->face.Y = 0;
				blockSelection->face.Z = -step.Z;
			}
		}

		if(blockLocalPos.X < 0 || blockLocalPos.X >= CHUNK_SIZE || blockLocalPos.Y < 0 || blockLocalPos.Y >= CHUNK_SIZE || blockLocalPos.Z < 0 || blockLocalPos.Z >= CHUNK_SIZE) {
			v3f tempPos(chunk->getPosition() + v3f(blockLocalPos.X, blockLocalPos.Y, blockLocalPos.Z));
			blockWorldPos = v3s(tempPos.X, tempPos.Y, tempPos.Z);
			chunk = chunkTable->GetChunkAt(IChunkTableSceneNode::PosToChunkCoord(tempPos));
			blockLocalPos = v3s(blockWorldPos.X - (blockWorldPos.X / CHUNK_SIZE - (blockWorldPos.X < 0 ? 1 : 0)) * CHUNK_SIZE,		// modulo
								blockWorldPos.Y - (blockWorldPos.Y / CHUNK_SIZE - (blockWorldPos.Y < 0 ? 1 : 0)) * CHUNK_SIZE,		// and in case of negative chunk positions
								blockWorldPos.Z - (blockWorldPos.Z / CHUNK_SIZE - (blockWorldPos.Z < 0 ? 1 : 0)) * CHUNK_SIZE		// subtract one
								);
			if(chunk == nullptr)
				return nullptr;
		}
		blockSelection->block = chunk->GetBlock(blockLocalPos); //(getBlock(pos));
		if(blockSelection->block != nullptr) {
			if(blockSelection->block->GetBlockType() & Block::BT_Solid) {
				v3f tempPos(chunk->getPosition() + v3f(blockLocalPos.X, blockLocalPos.Y, blockLocalPos.Z));
				blockSelection->pos = tempPos;
				return blockSelection;
			}
		}
	}

	return nullptr;
}

v3f ChunkHelper::intBound(const v3f &s, const v3f &ds) {
	return v3f(ds.X > 0 ?
				   (std::ceil(s.X) - s.X) / ds.X :
				   (s.X - std::floor(s.X)) / -ds.X,
			   ds.Y > 0 ?
				   (std::ceil(s.Y) - s.Y) / ds.Y :
				   (s.Y - std::floor(s.Y)) / -ds.Y,
			   ds.Z > 0 ?
				   (std::ceil(s.Z) - s.Z) / ds.Z :
				   (s.Z - std::floor(s.Z)) / -ds.Z);

}


/*v3f intbound(const v3f &s, const v3f &ds) {
	// Find the smallest positive t such that s+t*ds is an integer.
	if (ds.X < 0) {
		return intbound(-s, -ds);
	} else {
		s.X = mod(s.X, 1);
		// problem is now s+t*ds = 1
		return (1-s)/ds;
	}
}

void signum(x) {
	return x > 0 ? 1 : x < 0 ? -1 : 0;
}

void mod(value, modulus) {
	return (value % modulus + modulus) % modulus;
}*/

