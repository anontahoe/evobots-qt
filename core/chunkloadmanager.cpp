/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "chunkloadmanager.h"
#include "chunkloadworker.h"
#include "threadmanager.h"
#include "chunkmanager.h"
#include "engine.h"
#include "chunk.h"

using namespace core;

ChunkLoadManager::ChunkLoadManager(IEngine *engine, ChunkManager *chunkManager, TerrainGenerator *terra)
	: chunksToLoad(v3sSet()),
	  chunksLoading(v3sSet()),
	  chunksLoadingLock(new QMutex()),
	  engine(engine),
	  chunkManager(chunkManager),
	  chunksToIgnore(v3sSet())
{
	for(u32 i = 0; i < CHUNKLOADTHREADS; i++) {
		this->chunkLoadThreads[i] = new ChunkLoadWorker(this, chunkManager, terra);
		this->engine->GetThreadManager()->AddThread(this->chunkLoadThreads[i]);
	}
}

ChunkLoadManager::~ChunkLoadManager() {
	for(u32 i = 0; i < CHUNKLOADTHREADS; i++) {
		this->engine->GetThreadManager()->JoinThread(this->chunkLoadThreads[i]);
		this->chunkLoadThreads[i] = nullptr;
	}

	this->engine = nullptr;
	this->chunkManager = nullptr;

	delete this->chunksLoadingLock;
	this->chunksLoadingLock = nullptr;
}

void ChunkLoadManager::StopThreads() {
	for(u32 i = 0; i < CHUNKLOADTHREADS; i++) {
		this->chunkLoadThreads[i]->deleteLater();
	}
}

void ChunkLoadManager::LoadChunk(const core::v3s &chunkPos) {
	// only load chunk if it isn't already loading
	if(this->chunksLoading.find(chunkPos) != this->chunksLoading.end()) {
		this->chunksToIgnore.erase(chunkPos);	// unignore chunk
	} else {
		this->chunksToLoad.insert(chunkPos);
	}
}

void ChunkLoadManager::LoadChunkSafe(const core::v3s &chunkPos) {
	const QMutexLocker locker(this->chunksLoadingLock);
	LoadChunk(chunkPos);
}

void ChunkLoadManager::LoadChunks(v3sSet chunks) {
	this->chunksLoadingLock->lock();
	this->chunksToLoad.insert(chunks.begin(), chunks.end());	// this->chunksToLoad.end(),
	this->chunksLoadingLock->unlock();
}

bool ChunkLoadManager::WorkAvailable() {
	return !this->chunksToLoad.empty();
}

v3sPtr ChunkLoadManager::GetFirstChunkPos() {
	const QMutexLocker locker(this->chunksLoadingLock);

	if(this->chunksToLoad.empty()) {
		return v3sPtr();
	}
	v3sPtr chunkPos(new v3s(*this->chunksToLoad.begin()));
	//this->chunksToLoad.pop_front();
	this->chunksToLoad.erase(this->chunksToLoad.begin());
	this->chunksLoading.insert(*chunkPos);
	return chunkPos;
}

void ChunkLoadManager::IgnoreChunk(const v3s &chunkPos) {
	const QMutexLocker locker(this->chunksLoadingLock);
	this->chunksToIgnore.insert(chunkPos);
}

void ChunkLoadManager::UnIgnoreChunk(const v3s &chunkPos) {
	const QMutexLocker locker(this->chunksLoadingLock);
	this->chunksToIgnore.erase(chunkPos);
}

bool ChunkLoadManager::TryStopLoadChunk(const v3s &chunkPos) {
	const v3sSet::const_iterator chunkPosToLoad(this->chunksToLoad.find(chunkPos));
	if(chunkPosToLoad != this->chunksToLoad.end()) {
		this->chunksToLoad.erase(chunkPosToLoad);
		return true;
	}

	const v3sSet::const_iterator chunkPosLoading(this->chunksLoading.find(chunkPos));
	if(chunkPosLoading != this->chunksLoading.end()) {
		this->chunksToIgnore.insert(chunkPos);
		return true;
	}

	return false;
}

bool ChunkLoadManager::TryStopLoadChunkSafe(const v3s &chunkPos) {
	const QMutexLocker locker(this->chunksLoadingLock);
	return TryStopLoadChunk(chunkPos);
}

void ChunkLoadManager::DeliverChunk(const v3s &chunkPos, ChunkPtr& chunkToSet) {
	const QMutexLocker locker(this->chunksLoadingLock);

	this->chunksLoading.erase(chunkPos);

	const v3sSet::const_iterator chunkPosToIgnore(this->chunksToIgnore.find(chunkPos));
	if(chunkPosToIgnore != this->chunksToIgnore.end()) {
		this->chunksToIgnore.erase(chunkPosToIgnore);
		chunkToSet->StartDestroyJob(chunkToSet);
		return;
	}

	this->chunkManager->SetChunkAt(chunkPos, chunkToSet);
}






