/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "threadable.h"
#include "threadmanager.h"
#include <GLFW/glfw3.h>

s32 Threadable::threadableCount = 0;

Threadable::Threadable(ThreadManager *threadManager)
	: threadManager(threadManager),
	  shutDown(false),
	  deleteAfterShutdown(false)
{
	threadableCount++;
}

Threadable::~Threadable() {
	threadableCount--;
}

void Threadable::deleteLater() {
	this->deleteAfterShutdown = true;
	this->shutDown = true;
}

void Threadable::StartWork() {
	DoWork();
	this->threadManager->ThreadStopped(this);
}

void Threadable::StopWork() {
	this->shutDown = true;
}

void Threadable::StartWorkStatic(Threadable* threadable) {
	threadable->DoWork();
	ThreadManager* threadManager = threadable->threadManager;
	if(threadable->deleteAfterShutdown) {
		delete threadable;
	}
	threadManager->ThreadStopped(threadable);
}

void Threadable::StartWorkDetached(Threadable *threadable) {
	threadable->DoWork();
	if(threadable->deleteAfterShutdown) {
		delete threadable;
	}
}

void Threadable::DoWork() {

}
