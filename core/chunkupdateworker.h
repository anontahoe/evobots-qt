#ifndef CHUNKREBUILDTHREAD_H
#define CHUNKREBUILDTHREAD_H

#include "threadable.h"

using namespace irr;
using namespace scene;

class ChunkManager;
class TerrainGenerator;
class ChunkUpdateManager;

class ChunkUpdateWorker : public Threadable {
public:
	ChunkUpdateWorker(ChunkUpdateManager *chunkUpdateManager, ThreadManager *threadManager);
	virtual ~ChunkUpdateWorker();

public slots:
	virtual void		deleteLater();

protected slots:
	virtual void		DoWork();

private:
	void				RebuildChunk(const ChunkPtr &chunk);
	void				ChunkUpdateLoop();

	ChunkUpdateManager	*chunkUpdateManager;
};

#endif // CHUNKREBUILDTHREAD_H
