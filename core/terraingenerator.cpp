/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "terraingenerator.h"
#include "block.h"
#include "engine.h"
#include "chunkmanager.h"
#include "logger.h"
#include "typedefs.h"
#include "chunk.h"


using namespace irr;
using namespace core;

#define HILL_HEIGHT 20
#define HILL_WIDTH 40

TerrainGenerator::TerrainGenerator(IEngine *engine)
	: engine(engine),
	  chunkManager(nullptr)
{
	Logger::Log("Precalculating sine/cosine...");
	for(u32 i = 0; i < SIN_COMP_COUNT; i++)
	{
		this->sinPreComp[i] = sin(i * PI64 * 2 / SIN_COMP_COUNT);
		this->cosPreComp[i] = cos(i * PI64 * 2 / SIN_COMP_COUNT);
		//Logger::Log(std::to_string(i)+": "+std::to_string(sinPreComp[i]));

	}
}

void TerrainGenerator::Initialize() {
	this->chunkManager = this->engine->GetChunkManager();
}

ChunkPtr TerrainGenerator::CreateChunk(const vector3di &chunkPos) const {
	IGraphics* graphics = this->engine->GetGraphics();
	//graphics->LockSceneNode();
	ChunkPtr chunk(new IChunkSceneNode(0, graphics->GetSceneManager(), -1, chunkPos, this->chunkManager, this->engine->GetGraphics()), &IChunkSceneNode::myDeleter);
	chunk->Initialize();
	//chunk->setMaterialTexture(0, graphics->GetBlockTexture());
	//chunk->setVisible(false);
	//graphics->UnlockSceneNode();

	//GenerateTerrainFlat(chunk);
	//GenerateTerrainWaves(chunk);
	GenerateTerrain(chunk);

	return chunk;
}

void TerrainGenerator::GenerateTerrain(ChunkPtr chunk) const {
	vector3di pos = chunk->GetChunkPosition();

	double height = 0;

	for(s32 x = 0; x < CHUNK_SIZE; x++) {
		s32 xsinIndex = (double)((pos.X * CHUNK_SIZE + x) % (2 * HILL_WIDTH)) / HILL_WIDTH * SIN_COMP_COUNT / 2;
		while(xsinIndex < 0)							// keep in array boundaries
			xsinIndex += SIN_COMP_COUNT;
		double xsin = this->sinPreComp[xsinIndex];

		for(s32 z = 0; z < CHUNK_SIZE; z++)	{
			s32 zsinIndex = (double)((pos.Z * CHUNK_SIZE + z) % (2 * HILL_WIDTH)) / HILL_WIDTH * SIN_COMP_COUNT / 2;
			while(zsinIndex < 0)						// keep in array boundaries
				zsinIndex += SIN_COMP_COUNT;
			double zsin = this->sinPreComp[zsinIndex];

			height = (xsin + zsin) * HILL_HEIGHT / 2;
			height -= (double)pos.Y * (double)CHUNK_SIZE;

			for(s32 y = 0; y < height && y < CHUNK_SIZE; y++) {
				chunk->SetBlockType(v3s(x, y, z), Block::BT_Dirt + rand() % 3);
			}
			/*for(s32 y = height; y > 0 && y < CHUNK_SIZE; y++) {			// unnecessary, here for debugging purposes
				chunk->SetBlockType(v3i(x,y,z), Block::BT_Air);
			}*/
		}
	}
}

void TerrainGenerator::GenerateTerrainFlat(ChunkPtr chunk) const {
	vector3di pos = chunk->GetChunkPosition();

	for(u32 x = 0; x < CHUNK_SIZE; x++) {
		for(u32 y = 0; y < CHUNK_SIZE; y++) {
			for(u32 z = 0; z < CHUNK_SIZE; z++) {
				if(pos.Y < 0)
					chunk->SetBlockType(vector3di(x, y, z), Block::BT_Dirt);
				else if(pos.Y == 0 && y == 0) {
					chunk->SetBlockType(vector3di(x, y, z), Block::BT_Dirt);
				}
			}
		}
	}

	chunk->SetBlockType(v3s(3,1,3), Block::BT_Dirt);
	chunk->SetBlockType(v3s(3,2,3), Block::BT_Dirt);
	chunk->SetBlockType(v3s(3,3,3), Block::BT_Dirt);
	chunk->SetBlockType(v3s(4,3,3), Block::BT_Dirt);
	chunk->SetBlockType(v3s(4,3,4), Block::BT_Dirt);
}

void TerrainGenerator::GenerateTerrainWaves(ChunkPtr chunk) const {
	vector3di pos = chunk->GetChunkPosition();

	s32 offset = 0; //WORLD_SIZE / 2;
	//pos -= vector3di(offset);
	if(pos.Y - offset == -1) {
		for(u32 x = 0; x < CHUNK_SIZE; x++)
			for(u32 y = 0; y < CHUNK_SIZE; y++)
				for(u32 z = 0; z < CHUNK_SIZE; z++) {
					chunk->SetBlockType(vector3di(x,y,z), Block::BT_Dirt);
				}
		return;
	}
	if(pos.Y - offset < -1) {
		for(u32 x = 0; x < CHUNK_SIZE; x++)
			for(u32 y = 0; y < CHUNK_SIZE; y++)
				for(u32 z = 0; z < CHUNK_SIZE; z++) {
					chunk->SetBlockType(vector3di(x,y,z), Block::BT_Dirt);
				}
		return;
	}
	if(pos.Y - offset > 0) {
		for(u32 x = 0; x < CHUNK_SIZE; x++)
			for(u32 y = 0; y < CHUNK_SIZE; y++)
				for(u32 z = 0; z < CHUNK_SIZE; z++) {

				}
		return;
	}

	for(s32 x = 0; x < CHUNK_SIZE; x++) {
		s32 sinIndex = (double)((pos.X * CHUNK_SIZE + x) % (2 * HILL_WIDTH)) / HILL_WIDTH * SIN_COMP_COUNT / 2;
		while(sinIndex < 0)
			sinIndex += SIN_COMP_COUNT;
		double height = this->sinPreComp[sinIndex] * HILL_HEIGHT; // sin((pos.X * CHUNK_SIZE + x) * 3.14f / HILL_WIDTH) * HILL_HEIGHT;
		if(height == 0)
			continue;

		ChunkPtr below(nullptr);
		for(u32 z = 0; z < CHUNK_SIZE; z++) {
			double tempHeight = height;
			if(height > 0) {
				while(tempHeight > 0) {
					for(u32 y = 0; y < height; y++) {
						chunk->SetBlockType(vector3di(x,y,z), Block::BT_Dirt);
					}
					tempHeight -= CHUNK_SIZE;
				}
			}
			else if (height < 0) {
				while(tempHeight < 0) {
					pos.Y = pos.Y - 1;
					below = this->chunkManager->GetChunkAt(pos);
					if(below == nullptr)
						break;

					for(s32 y = (s32)CHUNK_SIZE - 1; y >= CHUNK_SIZE + tempHeight && y >= 0; y--) {
						below->SetBlockType(vector3di(x,y,z),Block::BT_Air);
					}
					tempHeight += CHUNK_SIZE;
				}
			}
		}
	}
}





