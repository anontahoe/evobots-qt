#ifndef CHUNKMANAGER_H
#define CHUNKMANAGER_H

#include "typedefs.h"
#include "threadable.h"

class IEngine;
class IChunkTableSceneNode;
class QReadWriteLock;
class TerrainGenerator;
class ChunkLoadManager;
class ChunkUpdateManager;

using namespace irr;
using namespace scene;

#define WORLD_SIZE 12
#define WORLD_RENDER_SIZE 12

class ChunkManager : public Threadable {
public:
	explicit				ChunkManager(IEngine *engine);
							~ChunkManager();

	void					Initialize();
	void					SetCameraPosition(const core::v3f &cameraPos);
	ChunkPtr				GetChunkAt(const core::v3s &pos);
	bool					IsChunkWithinRange(const core::v3s &pos1, const core::v3s &pos2, s32 range);
	void					SetChunkAt(const ChunkPtr &chunk);
	void					SetChunkAt(const core::v3s& chunkPos, const ChunkPtr &chunk);
	const IChunkTableSceneNode* GetChunkTable() const;

signals:

public slots:
	virtual void			deleteLater();

protected slots:
	virtual void			DoWork();

public:
	IEngine*				engine;
	ChunkLoadManager		*chunkLoadManager;
	ChunkUpdateManager		*chunkUpdateManager;
	IChunkTableSceneNode	*chunkTable;

private:
	void					CreateChunks();

	void					Update();
	void					ChunkManagerLoop();
	void					CameraChangedChunk();

	bool					isInArea(const core::v3s &pos, const s32 &area) const;
	void					ForgetChunk(const core::v3s &pos);

	s32						loadedToolate = 0;

	core::v3s				previousCamChunkPos;
	core::v3s				currentCamChunkPos;
	core::v3f				currentCamPos;
	QMutex					*currentCamPosLock;

	TerrainGenerator		*terra;
	//ChunkPtrList			chunksToSet;
	//core::v3iSet			chunksToLoad;
	//core::v3iSet			chunksCurrentlyLoading;	// queue to remember which blocks are already on their way so we don't ask multiple times
	//ChunkVisibilityQueue	chunkVisibilityQueue;
	//QMutex					*chunksToSetLock;
	//QReadWriteLock			*chunksToLoadLock;
	//QReadWriteLock			*chunksCurrentlyLoadingLock;
	//QReadWriteLock			*chunkVisibilityQueueLock;
};

#endif // CHUNKMANAGER_H
