#ifndef LOGGER_H
#define LOGGER_H

#include <cstdio>
#include <iostream>

#include <irrlicht/irrlicht.h>

using namespace irr;

class Logger
{
public:
	Logger();

	static void Log(const std::string& text);

	static void Log(s32 number);
	static void Log(u32 number);
	static void Log(float number);
	static void Log(double number);

	static void Log(core::vector3di vector);
};

#endif // LOGGER_H
