/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "typedefs.h"
#include "threadmanager.h"
#include "threadable.h"
#include <QThread>
#include <QCoreApplication>
#include <thread>



ThreadManager::ThreadManager()
	: workerCount(0),
	  workerActiveCount(0),
	  singleUseThreadsStd(ThreadableStatusMap())
{

}

ThreadManager::~ThreadManager() {
	for(std::pair<Threadable*, ThreadableStatusPair> threadStatus : this->singleUseThreadsStd) {
		JoinThread(threadStatus.first);
	}
	/*for(ThreadListStd::iterator itty = this->singleUseThreadsStd.begin(); itty != this->singleUseThreadsStd.end(); ) {
		std::thread* t = (*itty);
		if(t->joinable()) {
			try {
				t->join();
			} catch (u32 e) {
				u32 a = e;
			}
		}
		delete (*itty);
		itty = this->singleUseThreadsStd.erase(itty);
	}*/
	this->singleUseThreadsStd.clear();
}

void ThreadManager::AddDetatchedThread(Threadable *threadable) {
	std::thread(Threadable::StartWorkDetached, threadable).detach();
}

void ThreadManager::AddThread(Threadable *threadable) {
	this->workerCount++;
	this->workerActiveCount++;

	ThreadableStatusPair threadStatus{new std::thread(Threadable::StartWorkStatic, threadable), false};
	threadStatus.first->detach();
	this->singleUseThreadsStd[threadable] = threadStatus;
}

void ThreadManager::JoinThread(Threadable *threadable) {
	while(!this->singleUseThreadsStd[threadable].second) {
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
	}
}

void ThreadManager::ThreadStopped(Threadable *threadable) {
	delete this->singleUseThreadsStd[threadable].first;
	this->singleUseThreadsStd[threadable].first = nullptr;
	this->singleUseThreadsStd[threadable].second = true;
	WorkerFinished();
}

void ThreadManager::ThreadDestroyed() {

}

void ThreadManager::WorkerFinished() {
	this->workerActiveCount--;
}

void ThreadManager::WorkerDestroyed() {
	this->workerCount--;
}
