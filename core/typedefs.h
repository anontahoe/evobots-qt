#ifndef TYPEDEFS_H
#define TYPEDEFS_H

#include <irrlicht/irrlicht.h>
#include <QMetaType>
#include <QMutexLocker>
#include <set>
#include <map>
#include <queue>
#include <memory>

namespace irr {

namespace core {
	typedef dimension2di dim2i;
	typedef dimension2du dim2u;

	typedef vector3d<u32> vector3du;
	typedef vector2d<u32> vector2du;
	typedef vector3di v3s;
	typedef vector3du v3u;
	typedef vector3df v3f;
	typedef vector2df v2f;
	typedef vector2di v2i;
	typedef vector2du v2u;
	typedef std::shared_ptr<v3u>						v3uPtr;
	typedef std::shared_ptr<v3s>						v3sPtr;
	typedef std::shared_ptr<v3f>						v3fPtr;
	typedef std::list<v3s>								v3sList;
	typedef std::set<irr::core::v3s>					v3sSet;

	typedef aabbox3df									bboxf;
	typedef aabbox3di									bboxi;

}; // irr::core

namespace scene {
	typedef QVarLengthArray<u16>						IndicesArray;
	typedef QVarLengthArray<video::S3DVertex>			VerticesArray;
	class IChunkSceneNode;
	typedef std::shared_ptr<IChunkSceneNode>			ChunkPtr;
	typedef std::set<ChunkPtr>							ChunkPtrSet;
	typedef std::list<ChunkPtr>							ChunkPtrList;
	typedef std::pair<ChunkPtr, irr::u8>				ChunkNeighbourUpdatedPair;
	typedef std::shared_ptr<ChunkNeighbourUpdatedPair>	ChunkNeighbourUpdatedPairPtr;
	typedef std::list<ChunkNeighbourUpdatedPairPtr>		ChunkNeighbourUpdatedList;
	typedef std::map<ChunkPtr, irr::u8>					ChunkNeighbourUpdatedMap;
	typedef std::pair<core::v3s, ChunkPtr>				ChunkPosChunkPair;
	typedef std::map<core::v3s, ChunkPtr>				ChunkPosChunkMap;
}; // irr::scene

namespace video {
	typedef std::map<io::path, ITexture*>TextureMap;
	typedef std::map<video::ITexture*, u32>TextureIdMap;
}; // irr::video

}; // irr

namespace std {
	typedef basic_string<unsigned char> ustring;
}


typedef std::pair<irr::core::v3s, bool>				ChunkVisibilityPair;
typedef std::map<irr::core::v3s, bool>				ChunkVisibilityQueue;
typedef std::shared_ptr<QMutexLocker>				MutexLockerPtr;
typedef std::shared_ptr<irr::scene::ISceneNode>		SceneNodePtr;
typedef std::pair<std::string, std::string>			StringPair;
typedef std::map<StringPair, irr::u32>				ShaderProgramMap;

class Threadable;
namespace std { class thread; }
typedef std::pair<std::thread*,bool> ThreadableStatusPair;
typedef std::map<Threadable*, ThreadableStatusPair> ThreadableStatusMap;

#endif // TYPEDEFS_H
