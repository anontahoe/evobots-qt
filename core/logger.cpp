/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "logger.h"

Logger::Logger() {
}

void Logger::Log(const std::string& text) {
	std::printf("%s\n", text.c_str());
	std::cout.flush();
}

void Logger::Log(core::vector3di vector) {
	Log("{" + std::to_string(vector.X) + ", "
			 + std::to_string(vector.Y) + ","
			 + std::to_string(vector.Z) + "}");
}

void Logger::Log(s32 number) {
	Log(std::to_string(number));
}
void Logger::Log(u32 number) {
	Log(std::to_string(number));
}
void Logger::Log(float number) {
	Log(std::to_string(number));
}
void Logger::Log(double number) {
	Log(std::to_string(number));
}
