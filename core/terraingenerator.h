#ifndef TERRAINGENERATOR_H
#define TERRAINGENERATOR_H

#include <irrlicht/irrlicht.h>
#include "typedefs.h"

using namespace irr;
using namespace scene;

class IEngine;
class ChunkManager;

class TerrainGenerator
{
public:
	TerrainGenerator(IEngine *engine);

	void Initialize();

	ChunkPtr CreateChunk(const core::vector3di& chunkPos) const;

	void GenerateTerrain(ChunkPtr chunk) const;

	//void GenerateTerrainEmpty(Chunk* chunk);

	void GenerateTerrainFlat(ChunkPtr chunk) const;

	void GenerateTerrainWaves(ChunkPtr chunk) const;

private:
	IEngine* engine;
	ChunkManager* chunkManager;

#define SIN_COMP_COUNT 200
	double sinPreComp[SIN_COMP_COUNT];
	double cosPreComp[SIN_COMP_COUNT];
};

#endif // TERRAINGENERATOR_H
