/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "chunkconstructiondata.h"
#include "block.h"


v3s CD::adjacentDir[27];
v3s CD::adjacentFaceDir[CD::FACE_COUNT];
u8 CD::oppositeFaces[CD::FACE_COUNT];
v3s CD::vertexLocalPos[CD::VERT_COUNT];
u32 CD::vertexProviders[CD::VERT_COUNT];
vector3df CD::vertexNormals[CD::VERT_COUNT];
vector2df CD::vertexUV[CD::VERT_COUNT];
const u8 CD::invalidIndex((u8)-1);
const u8 CD::tilesPerTextureRow(16);

const CD chunkConstructionData;

ChunkConstructionData::ChunkConstructionData() {
	// initialization of some globals

	for(s32 z = 0; z < 2; z++)
		for(s32 y = 0; y < 2; y++)
			for(s32 x = 0; x < 2; x++) {
				vertexLocalPos[x+2*y+4*z] = v3s(x,y,z);
			}
	vertexLocalPos[VERT_BUL_SPECIAL] = vertexLocalPos[VERT_BUL];	// if those are in order I should put that in a loop
	vertexLocalPos[VERT_BUR_SPECIAL] = vertexLocalPos[VERT_BUR];
	vertexLocalPos[VERT_FUL_SPECIAL] = vertexLocalPos[VERT_FUL];
	vertexLocalPos[VERT_FUR_SPECIAL] = vertexLocalPos[VERT_FUR];
	vertexLocalPos[VERT_BDL_SPECIAL] = vertexLocalPos[VERT_BDL];
	vertexLocalPos[VERT_BDR_SPECIAL] = vertexLocalPos[VERT_BDR];
	vertexLocalPos[VERT_FDL_SPECIAL] = vertexLocalPos[VERT_FDL];
	vertexLocalPos[VERT_FDR_SPECIAL] = vertexLocalPos[VERT_FDR];


	vertexUV[VERT_BDL] = v2f(0,0);
	vertexUV[VERT_BDR] = v2f(1,0);
	vertexUV[VERT_BUL] = v2f(0,1);
	vertexUV[VERT_BUR] = v2f(1,1);
	vertexUV[VERT_FDL] = v2f(1,0);
	vertexUV[VERT_FDR] = v2f(0,0);
	vertexUV[VERT_FUL] = v2f(1,1);
	vertexUV[VERT_FUR] = v2f(0,1);
	vertexUV[VERT_BUL_SPECIAL] = vertexUV[VERT_BDL] + v2f(0,1);		// the block's top/bottom face textures are below the side textures in the atlas
	vertexUV[VERT_BUR_SPECIAL] = vertexUV[VERT_BDR] + v2f(0,1);
	vertexUV[VERT_FUL_SPECIAL] = vertexUV[VERT_BUL] + v2f(0,1);
	vertexUV[VERT_FUR_SPECIAL] = vertexUV[VERT_BUR] + v2f(0,1);
	vertexUV[VERT_BDL_SPECIAL] = vertexUV[VERT_BDL] + v2f(0,1);
	vertexUV[VERT_BDR_SPECIAL] = vertexUV[VERT_BDR] + v2f(0,1);
	vertexUV[VERT_FDL_SPECIAL] = vertexUV[VERT_BUL] + v2f(0,1);
	vertexUV[VERT_FDR_SPECIAL] = vertexUV[VERT_BUR] + v2f(0,1);
	for(int i = 0; i < VERT_COUNT; i++) {
		vertexUV[i] /= tilesPerTextureRow;// (float)Block::BlockTypeCount;	// all block types share one texture file, the block type's offset is added when the vertex is created
	}

	for(s32 z = 0; z < 3; z++)
		for(s32 y = 0; y < 3; y++)
			for(s32 x = 0; x < 3; x++) {
				adjacentDir[x+3*y+9*z] = v3s(x-1,y-1,z-1);
			}


	adjacentFaceDir[FACE_BACK]			= adjacentDir[DIR_BCC];
	adjacentFaceDir[FACE_FRONT]			= adjacentDir[DIR_FCC];
	//adjacentFaceDir[FACE_TOP]			= adjacentDir[DIR_CUC];
	adjacentFaceDir[FACE_TOP_SPECIAL]	= adjacentDir[DIR_CUC];
	//adjacentFaceDir[FACE_BOTTOM]		= adjacentDir[DIR_CDC];
	adjacentFaceDir[FACE_BOTTOM_SPECIAL]= adjacentDir[DIR_CDC];
	adjacentFaceDir[FACE_LEFT]			= adjacentDir[DIR_CCL];
	adjacentFaceDir[FACE_RIGHT]			= adjacentDir[DIR_CCR];

	oppositeFaces[FACE_BACK]			= FACE_FRONT;
	oppositeFaces[FACE_FRONT]			= FACE_BACK;
	//oppositeFaces[FACE_TOP]				= FACE_BOTTOM;
	oppositeFaces[FACE_TOP_SPECIAL]		= FACE_BOTTOM_SPECIAL;
	//oppositeFaces[FACE_BOTTOM]			= FACE_TOP;
	oppositeFaces[FACE_BOTTOM_SPECIAL]	= FACE_TOP_SPECIAL;
	oppositeFaces[FACE_LEFT]			= FACE_RIGHT;
	oppositeFaces[FACE_RIGHT]			= FACE_LEFT;

	vertexProviders[VERT_BDL] = 1 << DIR_BDL | 1 << DIR_BDC | 1 << DIR_BCL | 1 << DIR_BCC | 1 << DIR_CDL | 1 << DIR_CDC | 1 << DIR_CCL;
	vertexProviders[VERT_BDR] = 1 << DIR_BDC | 1 << DIR_BDR | 1 << DIR_BCC | 1 << DIR_BCR | 1 << DIR_CDC | 1 << DIR_CDR;
	vertexProviders[VERT_BUL] = 1 << DIR_BCL | 1 << DIR_BCC | 1 << DIR_BUL | 1 << DIR_BUC | 1 << DIR_CCL;
	vertexProviders[VERT_BUR] = 1 << DIR_BCC | 1 << DIR_BCR | 1 << DIR_BUC | 1 << DIR_BUR;
	vertexProviders[VERT_FDL] = 1 << DIR_CDL | 1 << DIR_CDC | 1 << DIR_CCL;
	vertexProviders[VERT_FDR] = 1 << DIR_CDC | 1 << DIR_CDR;
	vertexProviders[VERT_FUL] = 1 << DIR_CCL;
	vertexProviders[VERT_FUR] = 0;
	for(s32 i = VERT_BUL_SPECIAL; i < VERT_COUNT; i++) {
		vertexProviders[i] = 0;		// special faces mustn't share vertices
	}

	for(s32 i = 0; i < VERT_COUNT; i++) {
		vertexNormals[i] = vector3df(0, 1, 0);	// turns out we don't even need those in irrlicht. at least for now.
	}
}
