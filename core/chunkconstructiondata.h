/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#ifndef CHUNKCONSTRUCTIONDATA_H
#define CHUNKCONSTRUCTIONDATA_H

#include <typedefs.h>

using namespace irr;
using namespace core;

struct ChunkConstructionData {
	enum Face
	{
		FACE_BACK = 0,
		FACE_FRONT,
		//FACE_TOP,
		FACE_TOP_SPECIAL,
		//FACE_BOTTOM,
		FACE_BOTTOM_SPECIAL,
		FACE_LEFT,
		FACE_RIGHT,
		FACE_COUNT
	};

	enum Vertex
	{
		VERT_BDL = 0,	// Front Down Left (0,0,0)
		VERT_BDR,
		VERT_BUL,
		VERT_BUR,
		VERT_FDL,
		VERT_FDR,
		VERT_FUL,
		VERT_FUR,		// Back Up Right (1,1,1)
		VERT_BUL_SPECIAL, VERT_FUL_SPECIAL, VERT_FUR_SPECIAL, VERT_BUR_SPECIAL,
		VERT_BDL_SPECIAL, VERT_FDL_SPECIAL, VERT_FDR_SPECIAL, VERT_BDR_SPECIAL,
		VERT_COUNT
	};

	enum AdjacentDir
	{
		DIR_BDL = 0,	// Front Down Left (-1,-1,-1)
		DIR_BDC,
		DIR_BDR,
		DIR_BCL,
		DIR_BCC,
		DIR_BCR,
		DIR_BUL,
		DIR_BUC,
		DIR_BUR,
		DIR_CDL,
		DIR_CDC,
		DIR_CDR,
		DIR_CCL,
		DIR_MAXPROVIDERS,	// aka DIR_CCC but we don't use that
		DIR_CCR,
		DIR_CUL,
		DIR_CUC,
		DIR_CUR,
		DIR_FDL,
		DIR_FDC,
		DIR_FDR,
		DIR_FCL,
		DIR_FCC,
		DIR_FCR,
		DIR_FUL,
		DIR_FUC,
		DIR_FUR			// Back Up Right (1,1,1)
	};

	static v3s adjacentDir[];
	static v3s adjacentFaceDir[];		// maps facedirs to adjacentdir
	static u8 oppositeFaces[];
	static v3s vertexLocalPos[];
	static u32 vertexProviders[];
	static vector3df vertexNormals[];
	static vector2df vertexUV[];

	static const u8 invalidIndex;

	static const u8 tilesPerTextureRow;


	ChunkConstructionData();
};

typedef ChunkConstructionData CD;

extern const CD chunkConstructionData;

//QMutex* chunkDeletionMutex = new QMutex();
//QList<ChunkPtr> chunkDeletionList;

#endif // CHUNKCONSTRUCTIONDATA_H
