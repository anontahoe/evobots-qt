#ifndef ENGINE_H
#define ENGINE_H

#include "iengine.h"

class Engine : public IEngine
{
public:
	Engine();

	virtual void			ShutDown();

	virtual IGraphics*		GetGraphics() const;
	virtual InputManager*	GetInputManager() const;
	virtual ChunkManager*	GetChunkManager() const;
	virtual ThreadManager*	GetThreadManager() const;
	virtual UiUpdater*		GetUiUpdater() const;

protected:
	IGraphics*				graphicsModule;
	InputManager*			inputManagerModule;
	ChunkManager*			chunkManagerModule;
	ThreadManager*			threadManagerModule;
	UiUpdater*				uiUpdaterModule;
};

#endif // ENGINE_H
