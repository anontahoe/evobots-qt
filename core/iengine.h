#ifndef IENGINE_H
#define IENGINE_H

#include "igraphics.h"

class IGraphics;
class InputManager;
class ChunkManager;
class ThreadManager;
class UiUpdater;

class IEngine
{
public:
	virtual void			ShutDown() = 0;

	virtual IGraphics*		GetGraphics() const = 0;
	virtual InputManager*	GetInputManager() const = 0;
	virtual ChunkManager*	GetChunkManager() const = 0;
	virtual ThreadManager*	GetThreadManager() const = 0;
	virtual UiUpdater*		GetUiUpdater() const = 0;
};


#endif // IENGINE_H
