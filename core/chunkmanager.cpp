/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "chunkmanager.h"
#include "threadmanager.h"
#include "chunkloadmanager.h"
#include "chunkloadworker.h"
#include "chunkupdatemanager.h"
#include "chunkupdateworker.h"
#include "terraingenerator.h"
#include "chunktable.h"
#include "chunk.h"
#include "iengine.h"
#include <QWriteLocker>
#include <thread>
#include <chrono>
#include <utility>

using namespace irr;
using namespace core;
using namespace scene;

ChunkManager::ChunkManager(IEngine* engine) :
		Threadable(engine->GetThreadManager()),
		engine(engine),
		chunkLoadManager(nullptr),
		chunkUpdateManager(nullptr),
		chunkTable(nullptr),
		loadedToolate(0),
		previousCamChunkPos(v3s()),
		currentCamChunkPos(v3s()),
		currentCamPos(v3f()),
		currentCamPosLock(new QMutex()),
		terra(nullptr)/*,
		chunksToSet(ChunkPtrList()),
		chunksToLoad(v3iSet()),
		chunksCurrentlyLoading(v3iSet()),
		chunkVisibilityQueue(ChunkVisibilityQueue()),
		chunksToSetLock(new QMutex()),
		chunksToLoadLock(new QReadWriteLock()),
		chunksCurrentlyLoadingLock(new QReadWriteLock()),
		chunkVisibilityQueueLock(new QReadWriteLock())*/

{
}

ChunkManager::~ChunkManager() {
	delete this->currentCamPosLock;

	this->chunkTable->drop();		// will actually be destroyed when graphicsglew drops it
	this->chunkTable = nullptr;

	delete this->chunkLoadManager;
	this->chunkLoadManager = nullptr;

	delete this->chunkUpdateManager;
	this->chunkUpdateManager = nullptr;

	delete this->terra;
	this->terra = nullptr;
}

void ChunkManager::Initialize() {
	this->chunkTable					= new IChunkTableSceneNode(this->engine->GetGraphics()->GetRootSceneNode(), nullptr, -1, v3f(0), this->engine->GetGraphics());
	this->chunkTable->setAutomaticCulling(false);
	this->chunkTable->setMaterialFlag(video::EMF_LIGHTING, false);
	this->chunkTable->setMaterialFlag(video::EMF_FOG_ENABLE, true);

	this->terra							= new TerrainGenerator(this->engine);
	this->terra->Initialize();

	this->chunkLoadManager				= new ChunkLoadManager(this->engine, this, this->terra);
	this->chunkUpdateManager			= new ChunkUpdateManager(this->engine, this);

	CreateChunks();
}

const IChunkTableSceneNode* ChunkManager::GetChunkTable() const {
	return this->chunkTable;
}

void ChunkManager::CreateChunks() {
	this->previousCamChunkPos = this->currentCamChunkPos + WORLD_SIZE;
	CameraChangedChunk();
}

void ChunkManager::ForgetChunk(const v3s &pos){
	this->chunkTable->DropChunk(pos);
}

void ChunkManager::SetCameraPosition(const v3f &cameraPos) {
	this->currentCamPosLock->lock();
	this->currentCamPos = cameraPos;
	this->currentCamPosLock->unlock();
}

ChunkPtr ChunkManager::GetChunkAt(const v3s &pos) {
	return this->chunkTable->GetChunkAt(pos);
}

void ChunkManager::SetChunkAt(const v3s &chunkPos, const ChunkPtr &chunk) {
	//this->chunkUpdateManager->RebuildChunk(chunk);
	this->chunkTable->SetChunkAt(chunkPos, chunk);
}

bool ChunkManager::IsChunkWithinRange(const v3s &pos1, const v3s &pos2, s32 range) {
	range /= 2;
	vector3di diff = pos2 - pos1;
	if(abs(diff.X) <= range && abs(diff.Y) <= range && abs(diff.Z) <= range)
		return true;
	return false;
}

void ChunkManager::CameraChangedChunk() {
	const v3s dir(this->currentCamChunkPos - this->previousCamChunkPos);
	const v3s diffAbs(abs(dir.X), abs(dir.Y), abs(dir.Z));
	const v3s delOffset(dir.X < 0 ? WORLD_SIZE - diffAbs.X : 0,
						dir.Y < 0 ? WORLD_SIZE - diffAbs.Y : 0,
						dir.Z < 0 ? WORLD_SIZE - diffAbs.Z : 0);
	const v3s loadOffset(dir.X >= 0 ? WORLD_SIZE - diffAbs.X : 0,
						 dir.Y >= 0 ? WORLD_SIZE - diffAbs.Y : 0,
						 dir.Z >= 0 ? WORLD_SIZE - diffAbs.Z : 0);
	const s32 center = WORLD_SIZE / 2;
	const v3s oldWorldStart(this->previousCamChunkPos - center);
	const v3s nowWorldStart(this->currentCamChunkPos - center);

	this->chunkLoadManager->chunksLoadingLock->lock();

	v3s chunkDeletePos;
	for(s32 x = 0; x < diffAbs.X; x++) {
		for(s32 y = 0; y < WORLD_SIZE; y++) {
			for(s32 z = 0; z < WORLD_SIZE; z++) {
				chunkDeletePos = v3s(x + oldWorldStart.X + delOffset.X,
									 y + oldWorldStart.Y,
									 z + oldWorldStart.Z);
				if(!this->chunkLoadManager->TryStopLoadChunk(chunkDeletePos)) {
					this->chunkTable->DropChunk(chunkDeletePos);
				}
				this->chunkLoadManager->LoadChunk(v3s(x + nowWorldStart.X + loadOffset.X,
													  y + nowWorldStart.Y,
													  z + nowWorldStart.Z));
			}
		}

	}
	for(s32 x = diffAbs.X; x < WORLD_SIZE; x++) {
		for(s32 y = 0; y < diffAbs.Y; y++) {
			for(s32 z = 0; z < WORLD_SIZE; z++) {
				chunkDeletePos = v3s(x + oldWorldStart.X,
									 y + oldWorldStart.Y + delOffset.Y,
									 z + oldWorldStart.Z);
				if(!this->chunkLoadManager->TryStopLoadChunk(chunkDeletePos)) {
					this->chunkTable->DropChunk(chunkDeletePos);
				}
				this->chunkLoadManager->LoadChunk(v3s(x + nowWorldStart.X,
													  y + nowWorldStart.Y + loadOffset.Y,
													  z + nowWorldStart.Z));
			}
		}

	}
	for(s32 x = diffAbs.X; x < WORLD_SIZE; x++) {
		for(s32 y = diffAbs.Y; y < WORLD_SIZE; y++) {
			for(s32 z = 0; z < diffAbs.Z; z++) {
				chunkDeletePos = v3s(x + oldWorldStart.X,
									 y + oldWorldStart.Y,
									 z + oldWorldStart.Z + delOffset.Z);
				if(!this->chunkLoadManager->TryStopLoadChunk(chunkDeletePos)) {
					this->chunkTable->DropChunk(chunkDeletePos);
				}
				this->chunkLoadManager->LoadChunk(v3s(x + nowWorldStart.X,
													  y + nowWorldStart.Y,
													  z + nowWorldStart.Z + loadOffset.Z));
			}
		}

	}

	this->chunkLoadManager->chunksLoadingLock->unlock();

	this->previousCamChunkPos = this->currentCamChunkPos;
}

void ChunkManager::deleteLater() {
	this->chunkLoadManager->StopThreads();
	this->chunkUpdateManager->StopThreads();

	delete this->chunkLoadManager;
	this->chunkLoadManager = nullptr;

	delete this->chunkUpdateManager;
	this->chunkUpdateManager = nullptr;

	Threadable::deleteLater();
}

void ChunkManager::DoWork() {
	Threadable::DoWork();
	ChunkManagerLoop();
}

void ChunkManager::ChunkManagerLoop() {
	while(!this->shutDown) {
		Update();
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
	}
}

void ChunkManager::Update() {
	this->currentCamPosLock->lock();
	this->currentCamChunkPos = IChunkTableSceneNode::PosToChunkCoord(this->currentCamPos);
	this->currentCamPosLock->unlock();

	if(this->previousCamChunkPos != this->currentCamChunkPos) {
		CameraChangedChunk();
	}
}

bool ChunkManager::isInArea(const v3s &pos, const s32 &area) const {
	s32 offset = area / 2;
	if(pos.X >= -offset && pos.X <= offset && pos.Y >= -offset && pos.Y <= area && pos.Z >= -offset && pos.Z <= offset)
		return true;

	return false;
}



