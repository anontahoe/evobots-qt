/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "chunkupdatemanager.h"
#include "chunkupdateworker.h"
#include "threadmanager.h"
#include "chunkmanager.h"
#include "engine.h"
#include <utility>
#include <thread>

using namespace core;

ChunkUpdateManager::ChunkUpdateManager(IEngine *engine, ChunkManager *chunkManager)
	: chunksToRebuildCount(0),
	  chunksToDeleteCount(0),
	  chunksToInformCount(0),
	  chunksToRefreshCount(0),
	  engine(engine),
	  chunksToRebuild(ChunkPtrSet()),
	  chunksToRebuildLock(new QMutex()),
	  chunksToDelete(ChunkPtrSet()),
	  chunksToDeleteLock(new QMutex()),
	  chunksToInform(ChunkNeighbourUpdatedMap()),
	  chunksToInformLock(new QMutex()),
	  chunksToRefresh(ChunkNeighbourUpdatedMap()),
	  chunksToRefreshLock(new QMutex())
{
	for(u32 i = 0; i < CHUNKUPDATETHREADS; i++) {
		this->chunkUpdateWorkers[i] = new ChunkUpdateWorker(this, this->engine->GetThreadManager());
		this->engine->GetThreadManager()->AddThread(this->chunkUpdateWorkers[i]);
	}
}

ChunkUpdateManager::~ChunkUpdateManager() {
	for(u32 i = 0; i < CHUNKUPDATETHREADS; i++) {
		this->engine->GetThreadManager()->JoinThread(this->chunkUpdateWorkers[i]);
		this->chunkUpdateWorkers[i] = nullptr;
	}

	delete this->chunksToRefreshLock;
	this->chunksToRefreshLock = nullptr;

	delete this->chunksToInformLock;
	this->chunksToInformLock = nullptr;

	this->chunksToRebuild.clear();
	this->chunksToDelete.clear();

	delete this->chunksToDeleteLock;
	this->chunksToDeleteLock = nullptr;

	delete this->chunksToRebuildLock;
	this->chunksToRebuildLock = nullptr;
}

void ChunkUpdateManager::StopThreads() {
	for(u32 i = 0; i < CHUNKUPDATETHREADS; i++) {
		this->chunkUpdateWorkers[i]->deleteLater();
	}
}

// Rebuild chunk
bool ChunkUpdateManager::RebuildJobsAvailable() {
	return !this->chunksToRebuild.empty();
}

ChunkPtr ChunkUpdateManager::GetFirstRebuildJob() {
	this->chunksToRebuildLock->lock();
	if(this->chunksToRebuild.empty()) {
		this->chunksToRebuildLock->unlock();
		return nullptr;
	}
	ChunkPtr chunkToRebuild = *this->chunksToRebuild.begin();
	this->chunksToRebuild.erase(this->chunksToRebuild.begin());
	this->chunksToRebuildCount--;
	this->chunksToRebuildLock->unlock();
	return chunkToRebuild;
}

void ChunkUpdateManager::RebuildChunk(const ChunkPtr &chunkPtr) {
	this->chunksToRebuildLock->lock();
	this->chunksToRebuild.insert(chunkPtr);
	this->chunksToRebuildCount++;
	this->chunksToRebuildLock->unlock();
}

void ChunkUpdateManager::RebuildChunks(const ChunkPtrSet &chunks) {
	this->chunksToRebuildLock->lock();
	this->chunksToRebuild.insert(chunks.begin(), chunks.end());	// this->chunksToLoad.end(),
	this->chunksToRebuildCount += chunks.size();
	this->chunksToRebuildLock->unlock();
}

// Destroy chunk
bool ChunkUpdateManager::DeleteJobsAvailable() {
	return !this->chunksToDelete.empty();
}

ChunkPtr ChunkUpdateManager::GetFirstDeleteJob() {
	this->chunksToDeleteLock->lock();
	if(this->chunksToDelete.empty()) {
		this->chunksToDeleteLock->unlock();
		return nullptr;
	}
	ChunkPtr chunkPos = *this->chunksToDelete.begin();
	this->chunksToDelete.erase(this->chunksToDelete.begin());
	this->chunksToDeleteCount--;
	this->chunksToDeleteLock->unlock();
	return chunkPos;
}

void ChunkUpdateManager::DeleteChunk(ChunkPtr& chunkPtr) {
	this->chunksToDeleteLock->lock();
	this->chunksToDelete.insert(chunkPtr);
	this->chunksToDeleteCount++;
	chunkPtr.reset();
	this->chunksToDeleteLock->unlock();
}

void ChunkUpdateManager::DeleteChunks(const ChunkPtrSet &chunks) {
	this->chunksToDeleteLock->lock();
	this->chunksToDelete.insert(chunks.begin(), chunks.end());
	this->chunksToDeleteCount += chunks.size();
	this->chunksToDeleteLock->unlock();
}

// Chunk neighbour updated
bool ChunkUpdateManager::InformJobsAvailable() {
	return !this->chunksToInform.empty();
}

ChunkNeighbourUpdatedPair* ChunkUpdateManager::GetFirstInformJob() {
	this->chunksToInformLock->lock();
	if(this->chunksToInform.empty()) {
		this->chunksToInformLock->unlock();
		return nullptr;
	}
	ChunkNeighbourUpdatedPair* chunkNeighbourPair(new ChunkNeighbourUpdatedPair(*this->chunksToInform.begin()));
	this->chunksToInform.erase(this->chunksToInform.begin());
	this->chunksToInformCount--;
	this->chunksToInformLock->unlock();
	return chunkNeighbourPair;
}

void ChunkUpdateManager::InformChunkNeighbourCreated(const ChunkPtr &chunkPtr, const u8 &adjacentFaces) {
	this->chunksToInformLock->lock();
	// TODO: set instead of list
	ChunkNeighbourUpdatedMap::iterator chunkToInform(this->chunksToInform.find(chunkPtr));
	if(chunkToInform != this->chunksToInform.end()) {
		chunkToInform->second |= adjacentFaces;
	} else {
		this->chunksToInform[chunkPtr] = adjacentFaces;
		this->chunksToInformCount++;
	}
	this->chunksToInformLock->unlock();
}

ChunkNeighbourUpdatedPair *ChunkUpdateManager::GetFirstRefreshJob() {
	this->chunksToRefreshLock->lock();
	if(this->chunksToRefresh.empty()) {
		this->chunksToRefreshLock->unlock();
		return nullptr;
	}

	ChunkNeighbourUpdatedPair *chunkNeighbourPair(new ChunkNeighbourUpdatedPair(*this->chunksToRefresh.begin()));
	this->chunksToRefresh.erase(this->chunksToRefresh.begin()); //(this->chunksNeighboursUpdated.begin());
	this->chunksToRefreshCount--;
	this->chunksToRefreshLock->unlock();
	return chunkNeighbourPair;
}

void ChunkUpdateManager::RefreshChunkNeighbour(const ChunkPtr &chunkPtr, const u8 &adjacentFaces) {
	this->chunksToRefreshLock->lock();
	// TODO: set instead of list
	ChunkNeighbourUpdatedMap::iterator chunkToRefresh(this->chunksToRefresh.find(chunkPtr));
	if(chunkToRefresh != this->chunksToRefresh.end()) {
		chunkToRefresh->second |= adjacentFaces;
	} else {
		this->chunksToRefresh[chunkPtr] = adjacentFaces;
		this->chunksToRefreshCount++;
	}
	this->chunksToRefreshLock->unlock();
}
















