#ifndef THREADMANAGER_H
#define THREADMANAGER_H

#include "typedefs.h"
#include "irrlicht/irrlicht.h"
#include <QObject>
#include <QThread>

using namespace irr;

class QThread;
class Threadable;

namespace std { class thread; }

#define MAXTHREADS 8

class ThreadManager {
public:
				ThreadManager();
				~ThreadManager();

	void		AddDetatchedThread(Threadable* threadable);
	void		AddThread(Threadable *threadable);
	void		JoinThread(Threadable *threadable);
	void		ThreadStopped(Threadable* threadable);

	s32			workerCount;
	s32			workerActiveCount;
signals:

private slots:
	void		ThreadDestroyed();
	void		WorkerFinished();
	void		WorkerDestroyed();

private:
	ThreadableStatusMap singleUseThreadsStd;
};

#endif // THREADMANAGER_H
