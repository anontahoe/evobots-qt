/*
 * Copyright (C) 2015  anontahoe
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "engine.h"
//#include "igraphics.h"
//#include "inputmanager.h"
//#include "chunkmanager.h"
//#include "threadmanager.h"

Engine::Engine()
	: graphicsModule(nullptr),
	  inputManagerModule(nullptr),
	  chunkManagerModule(nullptr),
	  threadManagerModule(nullptr),
	  uiUpdaterModule(nullptr)
{
}

void Engine::ShutDown() {

}

IGraphics* Engine::GetGraphics() const {
	return this->graphicsModule;
}

InputManager* Engine::GetInputManager() const {
	return this->inputManagerModule;
}

ChunkManager* Engine::GetChunkManager() const {
	return this->chunkManagerModule;
}

ThreadManager* Engine::GetThreadManager() const {
	return this->threadManagerModule;
}

UiUpdater* Engine::GetUiUpdater() const {
	return this->uiUpdaterModule;
}
