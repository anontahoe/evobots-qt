#ifndef INPUTMANAGER_H
#define INPUTMANAGER_H

#include "typedefs.h"

class IEngine;
struct GLFWwindow;

class InputManager
{
public:
						InputManager(IEngine* engine, GLFWwindow* window);
						~InputManager();
	void				Initialize();

	void				CheckKeys(irr::u32 timerMs);

private:
	irr::core::v3f		GetRelativeRota(double mouseX, double mouseY);

	IEngine*			engine;
	GLFWwindow*			window;
	irr::core::dim2i	windowSize;
};

#endif // INPUTMANAGER_H
